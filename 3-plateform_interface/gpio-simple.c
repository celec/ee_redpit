#include "linux/kern_levels.h"
#include "linux/printk.h"
#include "linux/types.h"
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/mutex.h>

static struct platform_device *pdev;
struct timer_list exp_timer;
struct mutex mymutex;

static void print_timer(struct timer_list *t)
{
        printk(KERN_INFO "mutex freed by timer\n");
        mutex_unlock(&mymutex);
}

static ssize_t gpio_simple_show(struct device *pdev, struct device_attribute *attr, char *buf)
{ 
        printk(KERN_INFO "mutex taken to read smt\n");
        sprintf(buf, "SHOW FUNCTION!\n");
        mutex_lock(&mymutex);
        mod_timer(&exp_timer, jiffies + 5*HZ);
        return 0;
}
static DEVICE_ATTR(value, 0440, gpio_simple_show, NULL); // store

static int gpio_simple_probe(struct platform_device *pdev)
{
	printk(KERN_INFO "Hello from device\n");
	return device_create_file(&pdev->dev, &dev_attr_value);
}

static int gpio_simple_remove(struct platform_device *pdev)
{
	printk(KERN_INFO "Goodbye from device\n");
	return 0;
}

static struct platform_driver gpio_simple_driver = {
        .probe = gpio_simple_probe,
        .remove = gpio_simple_remove,
        .driver = {
                .name = "gpio-simple",
        },
};

static int __init gpio_simple_init(void)
{
	int ret;
	ret = platform_driver_register(&gpio_simple_driver);
	if (ret)
		return ret;
	pdev = platform_device_register_simple("gpio-simple", 0, NULL, 0);
	if (IS_ERR(pdev)) {
		platform_driver_unregister(&gpio_simple_driver);
		return PTR_ERR(pdev);
	}
        timer_setup(&exp_timer, print_timer, 0); // since 4.14
        exp_timer.expires = jiffies + 5*HZ;
        mutex_init(&mymutex);
	return 0;
}

static void __exit gpio_simple_exit(void)
{
	platform_device_unregister(pdev);
	platform_driver_unregister(&gpio_simple_driver);
        del_timer(&exp_timer) ;
}

module_init(gpio_simple_init)
module_exit(gpio_simple_exit)

MODULE_DESCRIPTION("GPIO simple driver");
MODULE_LICENSE("GPL");
