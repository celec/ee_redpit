 # Simple kernel plateform
 A module as driver, then a plateform as a peripheral

 For Linux: `ln -s Makefile.pc Makefile && make` <br>
 For RedPitaya: `ln -s Makefile.redpitaya Makefile && make flash`

Then, a `# insmod gpio-simple.ko` puts "Hello" in `dmesg`, while a `# rmmod gpio-simple` puts a "Goodbye".
If the peripheral is acknowledge by the driver, it puts "Hello from device" in `dmesg`, while if the driver is remove it will puts an additional "Goodby from device".
The device then take a mutex if value at `/sys/bus/plateform/gpio-simple.0/value` is read, then free this mutex after a 5s timer (check in `dmesg`).
