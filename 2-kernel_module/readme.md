 Bib: Linux Device Drivers, A. Rubini

 # Simple kernel module
 
 For Linux: `ln -s Makefile.pc Makefile && make` <br>
 For RedPitaya: `ln -s Makefile.redpitaya Makefile && make flash`

Then, a `# insmod hey.ko` puts "Hello" in `dmesg`, while a `# rmmod hey` puts a "Goodbye".
