// mknod /dev/ald c 100 0
#include "linux/kern_levels.h"
#include "linux/types.h"
#include <linux/module.h> // Needed by all modules
#include <linux/kernel.h> // Needed for KERN_INFO
#include <linux/init.h> // Needed for the macros
#include <linux/fs.h> // define fops
#include <linux/uaccess.h> // copy_to/from_user

#define CMAJOR 100
#define N 15

static int ald_open(struct inode *inod, struct file *fil);
static ssize_t ald_read(struct file *fil, char *buff, size_t len, loff_t *off);
static ssize_t ald_write(struct file *fil, const char *buff, size_t len,
			 loff_t *off);
static int ald_close(struct inode *inod, struct file *fil);

char buffer[N] = "Hello read\n\0";

int ald_start(void);
void ald_end(void);

static struct file_operations fops = {
	.read = ald_read,
	.open = ald_open,
	.write = ald_write,
	.release = ald_close,
};

int ald_start(void) // init_module(void)
{
	int t = register_chrdev(CMAJOR, "ald", &fops);
	if (t < 0) {
		printk(KERN_ALERT "registration failed\n");
		return t;
	}
	printk(KERN_ALERT "registration success\n");
	printk(KERN_INFO "Hello\n");
	return t;
}

void ald_end(void) // cleanup_module(void)
{
	printk(KERN_INFO "Goodbye\n");
	unregister_chrdev(CMAJOR, "ald");
}

static int ald_close(struct inode *inod, struct file *fil)
{
	printk(KERN_ALERT "bye\n");
	return 0;
}

static int ald_open(struct inode *inod, struct file *fil)
{
	printk(KERN_ALERT "open\n");
	return 0;
}

static ssize_t ald_read(struct file *fil, char *buff, size_t len, loff_t *off)
{
	int readPos = 0;
	printk(KERN_ALERT "read\n");
	while (len-- && (buffer[readPos] != 0))
		put_user(buffer[readPos++], buff++); // avoid kernel panic
	return readPos;
}

static ssize_t ald_write(struct file *fil, const char *buff, size_t len,
			 loff_t *off)
{
	int m = 0, mlen = (len > (N - 1)) ? (N - 1) : len;
	printk(KERN_ALERT "write ");
	for (m = 0; m < mlen; m++)
		get_user(buffer[m], buff + m); // avoid kernel panic
	buffer[mlen] = 0;
	printk(KERN_ALERT "%s \n", buffer);
	return len;
}

module_init(ald_start);
module_exit(ald_end);

MODULE_LICENSE("GPL");
