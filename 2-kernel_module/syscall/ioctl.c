#include <fcntl.h> // open
#include <unistd.h> // exit
#include <sys/ioctl.h> // ioctl 
#include <stdio.h>
#include <stdlib.h>

#define IOCTL_SET_MSG 0
#define IOCTL_GET_MSG 1
#define DEVICE_FILE_NAME "/dev/ald"

ioctl_set_msg(int file_desc, char *message)
{
        int ret_val;
        ret_val = ioctl(file_desc, IOCTL_SET_MSG, message);
        printf("set_msg message: %s\n", message);
}

ioctl_get_msg(int file_desc, char *message)
{
        int ret_val;
        ret_val = ioctl(file_desc, IOCTL_GET_MSG, message);
        printf("get_msg message: %s\n", message);
}

int main(int argc, char **argv)
{
        int file_desc, ret_val;
        char msg[30] = "Message passed by ictl\n\0";

        file_desc = open("/dev/ald", 0);
        printf("%d\n", file_desc);
        if (argc > 1)
                sprintf(msg, argv[1]);
        ioctl_set_msg(file_desc, msg);

        sprintf(msg, "Hello World");
        ioctl_get_msg(file_desc, msg);
        close(file_desc);
        return 0;
}
