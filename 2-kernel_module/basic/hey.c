#include <linux/module.h> // needed by all modules
#include <linux/kernel.h> // needed for printk priorities (KERN_INFO)
#include <linux/init.h> // needed for the macros

int hello_start(void);
void hello_end(void);

int hello_start(void)
{
	printk(KERN_INFO "Hello\n");
	return 0;
}

void hello_end(void)
{
	printk(KERN_INFO "Goodbye\n");
}

module_init(hello_start);
module_exit(hello_end);

MODULE_LICENSE("GPL");
