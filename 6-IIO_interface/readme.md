# Industrial Input/Output (IIO) interface
From Analog Devices: [https://wiki.analog.com/software/linux/docs/iio/iio](https://wiki.analog.com/software/linux/docs/iio/iio)

To load a module:
```Bash
insmod /lib/modules/$(XILVER)-xilinx/kernel/industrialio.ko
insmod /lib/modules/$(XILVER)-xilinx/kernel/buffer/industrialio-triggered-buffer.ko
insmod /lib/modules/$(XILVER)-xilinx/kernel/buffer/kfifo_buf.ko
```
Then the module itself:
```Bash
insmod /lib/modules/$(XILVER)-xilinx/kernel/adc/xilinx-xadc.ko # ADC or
insmod /lib/modules/$(XILVER)-xilinx/kernel/dac/ad5624r_spi.ko # DAC
```

To add configuration (channels to use...), add an overlay mocking the build DTS (keyword: `&adc` or `&dac` with same compatible). If the compatible module is already inserted, don't forget to reload it with a `rmmod/insmod`.

# Vivado bootgen
On host:
```Bash 
bootgen -image image.bif -arch zynq -process_bitstream bin -w on
```
Then on device (RedPitaya):
```
cp $(GATEWARE).bit.bin /lib/firmware/
echo "$(GATEWARE).bit.bin" > /sys/class/fpga_manager/fpga0/firmware
```
