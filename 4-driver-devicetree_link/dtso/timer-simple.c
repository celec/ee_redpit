#include "linux/export.h"
#include "linux/kern_levels.h"
#include "linux/printk.h"
#include "linux/types.h"
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/mutex.h>
#include <linux/of.h> // Open Firmware, convention for device-tree
#include <linux/of_device.h>

struct timer_list exp_timer;
struct mutex mymutex;
unsigned char delay = 5;

static void print_timer(struct timer_list *t)
{
        printk(KERN_INFO "mutex freed by timer\n");
        mutex_unlock(&mymutex);
}

static ssize_t timer_simple_show(struct device *pdev, struct device_attribute *attr, char *buf)
{ 
        printk(KERN_INFO "mutex taken to read smt\n");
        sprintf(buf, "SHOW FUNCTION!\n");
        mutex_lock(&mymutex);
        mod_timer(&exp_timer, jiffies + delay * HZ);
        return 0;
}
static DEVICE_ATTR(value, 0440, timer_simple_show, NULL); // store

static int timer_simple_probe(struct platform_device *pdev)
{
        struct device_node *np = pdev->dev.of_node;
        of_property_read_u8(np, "delay", &delay);
        timer_setup(&exp_timer, print_timer, 0); // since 4.14
        //exp_timer.expires = jiffies + delay * HZ;
        mutex_init(&mymutex);

	printk(KERN_INFO "Hello from driver\n");
	return device_create_file(&pdev->dev, &dev_attr_value);
}

static int timer_simple_remove(struct platform_device *pdev)
{
        del_timer(&exp_timer) ;

	printk(KERN_INFO "Goodbye from driver\n");
	device_remove_file(&pdev->dev, &dev_attr_value);
        return 0;
}

static const struct of_device_id timer_id_of[] = { // for dev ice tree
        { 
                .compatible = "simple-timer",
        },
        {}
};
MODULE_DEVICE_TABLE (of, timer_id_of);

static struct platform_driver timer_simple_driver = {
        .probe = timer_simple_probe,
        .remove = timer_simple_remove,
        .driver = {
                .name = "timer-simple",
                .owner = THIS_MODULE,
                .of_match_table = of_match_ptr(timer_id_of),
        },
};
module_platform_driver(timer_simple_driver);

MODULE_DESCRIPTION("Timer simple driver");
MODULE_LICENSE("GPL");
