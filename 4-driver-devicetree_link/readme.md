 # Simple kernel driver loaded by device-tree
 A module as driver, loaded by the kernel by device-tree: modified in the redpitaya boot partition.

 For RedPitaya: `ln -s Makefile.redpitaya Makefile && make flash`

If the peripheral is acknowledge by the driver, it puts "Hello from driver" in `dmesg`, while if the driver is remove it will puts an additional "Goodbye from driver".
The device then take a mutex if value at `/sys/firmware/devicetree/base/dummy_gpio/value` is read, then free this mutex after an `n` s (driver argument) timer (check in `dmesg`).

Then, same thing with an overlay method (no reboot needed).
