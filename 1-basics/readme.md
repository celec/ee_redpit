# Basics on RedPitaya

## Blink leds via bash
`blink.sh` on device

## Blink leds via http query
Launch server with `lighttpd -f /etc/lighttpd/lighttpd.conf` on device; create a basic `/var/www/index.html`; listen on `192.168.2.200`.
Add CGI support with `/etc/lighttpd/conf.d/cgi.conf` by adding `include "conf.d/cgi.conf"` at the end of `/etc/lighttpd/lighttpd.conf`.
Add permission to http query to access GPIO: `chown www-data.www-data /sys/class/leds/led?/brightness`.
`blink.cgi` in `/var/www/cgi/` in device; listen on `192.168.2.200/blink.cgi?led8=X&led9=X`, with `X = 0/1`.

## Print "Hello, World" on device 
Configure buildroot for RedPitaya:
```Bash 
git clone https://github.com/trabucayre/redpitaya.git 
cd redpitaya
source sourceme.ggm
cd $BR 
make redpitaya_defconfig
```
Then compile device images and binaries with `cd $BR; make -j8` (~1h30)

For cross-compilation, compiler is then `$BR/output/host/usr/bin/arm-linux-gcc`
If needed, flash the device-system SD-card with `dd if=$BR/output/images/sdcard.img of=dev/sdaX`
