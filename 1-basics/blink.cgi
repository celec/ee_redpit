#!/bin/sh

echo -e "Content-type: text/html\r\n\r\n"
echo "Hello CGI"

echo $QUERY_STRING "<br>"
led8=$(echo $QUERY_STRING | cut -d '&' -f1 | cut -d '=' -f2)
led9=$(echo $QUERY_STRING | cut -d '&' -f2 | cut -d '=' -f2)
echo $led8 "<br>"
echo $led9 "<br>"
echo $led8 > /sys/class/leds/led8/brightness
echo $led9 > /sys/class/leds/led9/brightness

if [ $led8 -eq 1 ]; then
        echo "LED8 ON"
else
        echo "LED8 OFF"
fi

if [ $led9 -eq 1 ]; then
        echo "LED9 ON"
else
        echo "LED9 OFF"
fi
