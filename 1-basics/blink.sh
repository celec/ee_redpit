#! /bin/sh

while :
do
        echo "LED8 ON; LED9 OFF"
        echo 1 > /sys/class/leds/led8/brightness
        echo 0 > /sys/class/leds/led9/brightness
        sleep 1
        echo "LED8 OFF; LED9 ON"
        echo 0 > /sys/class/leds/led8/brightness
        echo 1 > /sys/class/leds/led9/brightness
        sleep 1
done
