-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Nov 13 11:01:49 2023
-- Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
-- Command     : write_vhdl -force -mode synth_stub
--               /home/eqkter/X/uC/EE/7-OscimpDigital/acq/acq.gen/sources_1/bd/acq/ip/acq_shifterReal_0_0/acq_shifterReal_0_0_stub.vhdl
-- Design      : acq_shifterReal_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity acq_shifterReal_0_0 is
  Port ( 
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    data_en_i : in STD_LOGIC;
    data_eof_i : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    data_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    data_en_o : out STD_LOGIC;
    data_eof_o : out STD_LOGIC;
    data_rst_o : out STD_LOGIC;
    data_clk_o : out STD_LOGIC
  );

end acq_shifterReal_0_0;

architecture stub of acq_shifterReal_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_i[13:0],data_en_i,data_eof_i,data_clk_i,data_rst_i,data_o[15:0],data_en_o,data_eof_o,data_rst_o,data_clk_o";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "shifterReal,Vivado 2022.2";
begin
end;
