-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Nov 13 11:01:05 2023
-- Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
-- Command     : write_vhdl -force -mode synth_stub
--               /home/eqkter/X/uC/EE/7-OscimpDigital/acq/acq.gen/sources_1/bd/acq/ip/acq_dupplReal_1_to_2_0_0/acq_dupplReal_1_to_2_0_0_stub.vhdl
-- Design      : acq_dupplReal_1_to_2_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity acq_dupplReal_1_to_2_0_0 is
  Port ( 
    data_en_i : in STD_LOGIC;
    data_eof_i : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data1_en_o : out STD_LOGIC;
    data1_eof_o : out STD_LOGIC;
    data1_clk_o : out STD_LOGIC;
    data1_rst_o : out STD_LOGIC;
    data1_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    data2_en_o : out STD_LOGIC;
    data2_eof_o : out STD_LOGIC;
    data2_clk_o : out STD_LOGIC;
    data2_rst_o : out STD_LOGIC;
    data2_o : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end acq_dupplReal_1_to_2_0_0;

architecture stub of acq_dupplReal_1_to_2_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_en_i,data_eof_i,data_clk_i,data_rst_i,data_i[15:0],data1_en_o,data1_eof_o,data1_clk_o,data1_rst_o,data1_o[15:0],data2_en_o,data2_eof_o,data2_clk_o,data2_rst_o,data2_o[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "dupplReal_1_to_2,Vivado 2022.2";
begin
end;
