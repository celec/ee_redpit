// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Nov 13 11:01:04 2023
// Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_shifterReal_0_1_stub.v
// Design      : acq_shifterReal_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "shifterReal,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(data_i, data_en_i, data_eof_i, data_clk_i, 
  data_rst_i, data_o, data_en_o, data_eof_o, data_rst_o, data_clk_o)
/* synthesis syn_black_box black_box_pad_pin="data_i[18:0],data_en_i,data_eof_i,data_clk_i,data_rst_i,data_o[15:0],data_en_o,data_eof_o,data_rst_o,data_clk_o" */;
  input [18:0]data_i;
  input data_en_i;
  input data_eof_i;
  input data_clk_i;
  input data_rst_i;
  output [15:0]data_o;
  output data_en_o;
  output data_eof_o;
  output data_rst_o;
  output data_clk_o;
endmodule
