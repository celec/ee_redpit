// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Nov 13 11:01:05 2023
// Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_shifterReal_0_1_sim_netlist.v
// Design      : acq_shifterReal_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "acq_shifterReal_0_1,shifterReal,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "shifterReal,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (data_i,
    data_en_i,
    data_eof_i,
    data_clk_i,
    data_rst_i,
    data_o,
    data_en_o,
    data_eof_o,
    data_rst_o,
    data_clk_o);
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA" *) input [18:0]data_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_EN" *) input data_en_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_EOF" *) input data_eof_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_CLK" *) input data_clk_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_RST" *) input data_rst_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA" *) output [15:0]data_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_EN" *) output data_en_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_EOF" *) output data_eof_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_RST" *) output data_rst_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_CLK" *) output data_clk_o;

  wire data_clk_i;
  wire data_en_i;
  wire data_en_o;
  wire data_eof_i;
  wire data_eof_o;
  wire [18:0]data_i;
  wire [15:0]data_o;
  wire data_rst_i;

  assign data_clk_o = data_clk_i;
  assign data_rst_o = data_rst_i;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_shifterReal U0
       (.data_clk_i(data_clk_i),
        .data_en_i(data_en_i),
        .data_en_o(data_en_o),
        .data_eof_i(data_eof_i),
        .data_eof_o(data_eof_o),
        .data_i(data_i[18:3]),
        .data_o(data_o));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_shifterReal
   (data_o,
    data_en_o,
    data_eof_o,
    data_en_i,
    data_i,
    data_clk_i,
    data_eof_i);
  output [15:0]data_o;
  output data_en_o;
  output data_eof_o;
  input data_en_i;
  input [15:0]data_i;
  input data_clk_i;
  input data_eof_i;

  wire data_clk_i;
  wire data_en_i;
  wire data_en_o;
  wire data_eof_i;
  wire data_eof_o;
  wire data_eof_o_i_1_n_0;
  wire [15:0]data_i;
  wire [15:0]data_o;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_en_i),
        .Q(data_en_o),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    data_eof_o_i_1
       (.I0(data_en_i),
        .I1(data_eof_i),
        .O(data_eof_o_i_1_n_0));
  FDRE data_eof_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_eof_o_i_1_n_0),
        .Q(data_eof_o),
        .R(1'b0));
  FDRE \data_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[0]),
        .Q(data_o[0]),
        .R(1'b0));
  FDRE \data_s_reg[10] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[10]),
        .Q(data_o[10]),
        .R(1'b0));
  FDRE \data_s_reg[11] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[11]),
        .Q(data_o[11]),
        .R(1'b0));
  FDRE \data_s_reg[12] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[12]),
        .Q(data_o[12]),
        .R(1'b0));
  FDRE \data_s_reg[13] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[13]),
        .Q(data_o[13]),
        .R(1'b0));
  FDRE \data_s_reg[14] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[14]),
        .Q(data_o[14]),
        .R(1'b0));
  FDRE \data_s_reg[15] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[15]),
        .Q(data_o[15]),
        .R(1'b0));
  FDRE \data_s_reg[1] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[1]),
        .Q(data_o[1]),
        .R(1'b0));
  FDRE \data_s_reg[2] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[2]),
        .Q(data_o[2]),
        .R(1'b0));
  FDRE \data_s_reg[3] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[3]),
        .Q(data_o[3]),
        .R(1'b0));
  FDRE \data_s_reg[4] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[4]),
        .Q(data_o[4]),
        .R(1'b0));
  FDRE \data_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[5]),
        .Q(data_o[5]),
        .R(1'b0));
  FDRE \data_s_reg[6] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[6]),
        .Q(data_o[6]),
        .R(1'b0));
  FDRE \data_s_reg[7] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[7]),
        .Q(data_o[7]),
        .R(1'b0));
  FDRE \data_s_reg[8] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[8]),
        .Q(data_o[8]),
        .R(1'b0));
  FDRE \data_s_reg[9] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(data_i[9]),
        .Q(data_o[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
