// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Nov 13 11:01:52 2023
// Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_firReal_0_0_sim_netlist.v
// Design      : acq_firReal_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "acq_firReal_0_0,firReal,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "firReal,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (data_i,
    data_en_i,
    data_clk_i,
    data_rst_i,
    data_o,
    data_en_o,
    data_eof_o,
    data_clk_o,
    data_rst_o,
    tick_o,
    s00_axi_aclk,
    s00_axi_reset,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready);
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA" *) input [13:0]data_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_EN" *) input data_en_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_CLK" *) input data_clk_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_in DATA_RST" *) input data_rst_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA" *) output [18:0]data_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_EN" *) output data_en_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_EOF" *) output data_eof_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_CLK" *) output data_clk_o;
  (* x_interface_info = "xilinx.com:interface:real:1.0 data_out DATA_RST" *) output data_rst_o;
  output tick_o;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 s00_axi_signal_clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi_signal_clock, ASSOCIATED_BUSIF s00_axi, ASSOCIATED_RESET s00_axi_reset, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN acq_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 s00_axi_signal_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi_signal_reset, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input s00_axi_reset;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 125000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN acq_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RREADY" *) input s00_axi_rready;

  wire \<const0> ;
  wire data_clk_i;
  wire data_en_i;
  wire [13:0]data_i;
  wire [18:0]data_o;
  wire data_rst_i;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [28:0]\^s00_axi_rdata ;
  wire s00_axi_reset;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire s00_axi_wvalid;
  wire tick_o;

  assign data_clk_o = data_clk_i;
  assign data_en_o = tick_o;
  assign data_eof_o = \<const0> ;
  assign data_rst_o = data_rst_i;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rdata[31] = \<const0> ;
  assign s00_axi_rdata[30] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[29] = \<const0> ;
  assign s00_axi_rdata[28] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[27] = \<const0> ;
  assign s00_axi_rdata[26] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[25] = \<const0> ;
  assign s00_axi_rdata[24] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[23] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[22] = \<const0> ;
  assign s00_axi_rdata[21] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[20] = \<const0> ;
  assign s00_axi_rdata[19] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[18] = \<const0> ;
  assign s00_axi_rdata[17] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[16] = \<const0> ;
  assign s00_axi_rdata[15] = \<const0> ;
  assign s00_axi_rdata[14] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[13] = \<const0> ;
  assign s00_axi_rdata[12] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[11] = \<const0> ;
  assign s00_axi_rdata[10] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[9] = \<const0> ;
  assign s00_axi_rdata[8] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[7] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[6] = \<const0> ;
  assign s00_axi_rdata[5] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[4] = \<const0> ;
  assign s00_axi_rdata[3] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[2] = \<const0> ;
  assign s00_axi_rdata[1] = \^s00_axi_rdata [28];
  assign s00_axi_rdata[0] = \^s00_axi_rdata [0];
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal U0
       (.data_clk_i(data_clk_i),
        .data_en_i(data_en_i),
        .data_i(data_i),
        .data_o(data_o),
        .data_rst_i(data_rst_i),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata({\^s00_axi_rdata [28],\^s00_axi_rdata [0]}),
        .s00_axi_reset(s00_axi_reset),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata[15:0]),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wvalid(s00_axi_wvalid),
        .tick_o(tick_o));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_rvalid,
    s00_axi_rdata,
    data_o,
    tick_o,
    s00_axi_arready,
    s00_axi_bvalid,
    data_rst_i,
    data_clk_i,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_aclk,
    s00_axi_reset,
    s00_axi_wdata,
    data_en_i,
    data_i,
    s00_axi_awaddr,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_rvalid;
  output [1:0]s00_axi_rdata;
  output [18:0]data_o;
  output tick_o;
  output s00_axi_arready;
  output s00_axi_bvalid;
  input data_rst_i;
  input data_clk_i;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_aclk;
  input s00_axi_reset;
  input [15:0]s00_axi_wdata;
  input data_en_i;
  input [13:0]data_i;
  input [1:0]s00_axi_awaddr;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [1:0]addr_s;
  wire [4:0]coeff_addr_s;
  wire [4:0]coeff_addr_uns_s;
  wire coeff_en_s;
  wire [15:0]coeff_val_s;
  wire coeff_val_s_0;
  wire data_clk_i;
  wire data_en_i;
  wire [13:0]data_i;
  wire [18:0]data_o;
  wire data_rst_i;
  wire handle_comm_n_10;
  wire handle_comm_n_11;
  wire handle_comm_n_12;
  wire handle_comm_n_13;
  wire handle_comm_n_14;
  wire handle_comm_n_4;
  wire handle_comm_n_7;
  wire handle_comm_n_9;
  wire read_en_s;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [1:0]s00_axi_rdata;
  wire s00_axi_reset;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [15:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire s00_axi_wvalid;
  wire tick_o;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi firReal_axi_inst
       (.D(handle_comm_n_7),
        .E(coeff_val_s_0),
        .Q(coeff_addr_uns_s),
        .addr_s(addr_s),
        .\coeff_addr_s_reg[0]_0 (handle_comm_n_4),
        .\coeff_addr_s_reg[4]_0 (coeff_addr_s),
        .\coeff_addr_s_reg[4]_1 ({handle_comm_n_10,handle_comm_n_11,handle_comm_n_12,handle_comm_n_13,handle_comm_n_14}),
        .coeff_en_s(coeff_en_s),
        .coeff_en_s_reg_0(handle_comm_n_9),
        .\coeff_val_s_reg[15]_0 (coeff_val_s),
        .read_en_s(read_en_s),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_reset(s00_axi_reset),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top fir_top_inst
       (.coeff_en_s(coeff_en_s),
        .data_clk_i(data_clk_i),
        .data_en_i(data_en_i),
        .data_i(data_i),
        .data_o(data_o),
        .data_rst_i(data_rst_i),
        .mem_reg(coeff_addr_s),
        .mem_reg_0(coeff_val_s),
        .s00_axi_aclk(s00_axi_aclk),
        .tick_o(tick_o));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom handle_comm
       (.D(handle_comm_n_7),
        .E(coeff_val_s_0),
        .Q(coeff_addr_uns_s),
        .addr_s(addr_s),
        .axi_arready_reg_0(s00_axi_arready),
        .axi_awready_reg_0(s00_axi_awready),
        .axi_wready_reg_0(s00_axi_wready),
        .\coeff_addr_uns_s_reg[4] ({handle_comm_n_10,handle_comm_n_11,handle_comm_n_12,handle_comm_n_13,handle_comm_n_14}),
        .read_en_s(read_en_s),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_awvalid_0(handle_comm_n_4),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_reset(s00_axi_reset),
        .s00_axi_reset_0(handle_comm_n_9),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi
   (s00_axi_rvalid,
    coeff_en_s,
    Q,
    s00_axi_rdata,
    \coeff_val_s_reg[15]_0 ,
    \coeff_addr_s_reg[4]_0 ,
    s00_axi_reset,
    read_en_s,
    s00_axi_aclk,
    coeff_en_s_reg_0,
    addr_s,
    E,
    s00_axi_wdata,
    \coeff_addr_s_reg[0]_0 ,
    D,
    \coeff_addr_s_reg[4]_1 );
  output s00_axi_rvalid;
  output coeff_en_s;
  output [4:0]Q;
  output [1:0]s00_axi_rdata;
  output [15:0]\coeff_val_s_reg[15]_0 ;
  output [4:0]\coeff_addr_s_reg[4]_0 ;
  input s00_axi_reset;
  input read_en_s;
  input s00_axi_aclk;
  input coeff_en_s_reg_0;
  input [1:0]addr_s;
  input [0:0]E;
  input [15:0]s00_axi_wdata;
  input [0:0]\coeff_addr_s_reg[0]_0 ;
  input [0:0]D;
  input [4:0]\coeff_addr_s_reg[4]_1 ;

  wire [0:0]D;
  wire [0:0]E;
  wire [4:0]Q;
  wire [1:0]addr_s;
  wire [0:0]\coeff_addr_s_reg[0]_0 ;
  wire [4:0]\coeff_addr_s_reg[4]_0 ;
  wire [4:0]\coeff_addr_s_reg[4]_1 ;
  wire \coeff_addr_uns_s[1]_i_1_n_0 ;
  wire \coeff_addr_uns_s[2]_i_1_n_0 ;
  wire \coeff_addr_uns_s[3]_i_1_n_0 ;
  wire \coeff_addr_uns_s[4]_i_2_n_0 ;
  wire coeff_en_s;
  wire coeff_en_s_reg_0;
  wire [15:0]\coeff_val_s_reg[15]_0 ;
  wire read_en_s;
  wire \readdata_s[0]_i_1_n_0 ;
  wire \readdata_s[30]_i_1_n_0 ;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_rdata;
  wire s00_axi_reset;
  wire s00_axi_rvalid;
  wire [15:0]s00_axi_wdata;

  FDRE \coeff_addr_s_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_s_reg[4]_1 [0]),
        .Q(\coeff_addr_s_reg[4]_0 [0]),
        .R(s00_axi_reset));
  FDRE \coeff_addr_s_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_s_reg[4]_1 [1]),
        .Q(\coeff_addr_s_reg[4]_0 [1]),
        .R(s00_axi_reset));
  FDRE \coeff_addr_s_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_s_reg[4]_1 [2]),
        .Q(\coeff_addr_s_reg[4]_0 [2]),
        .R(s00_axi_reset));
  FDRE \coeff_addr_s_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_s_reg[4]_1 [3]),
        .Q(\coeff_addr_s_reg[4]_0 [3]),
        .R(s00_axi_reset));
  FDRE \coeff_addr_s_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_s_reg[4]_1 [4]),
        .Q(\coeff_addr_s_reg[4]_0 [4]),
        .R(s00_axi_reset));
  LUT3 #(
    .INIT(8'h28)) 
    \coeff_addr_uns_s[1]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\coeff_addr_uns_s[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h2888)) 
    \coeff_addr_uns_s[2]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\coeff_addr_uns_s[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h28888888)) 
    \coeff_addr_uns_s[3]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\coeff_addr_uns_s[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2888888888888888)) 
    \coeff_addr_uns_s[4]_i_2 
       (.I0(addr_s[0]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(\coeff_addr_uns_s[4]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \coeff_addr_uns_s_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(D),
        .Q(Q[0]),
        .R(s00_axi_reset));
  FDRE #(
    .INIT(1'b0)) 
    \coeff_addr_uns_s_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_uns_s[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(s00_axi_reset));
  FDRE #(
    .INIT(1'b0)) 
    \coeff_addr_uns_s_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_uns_s[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(s00_axi_reset));
  FDRE #(
    .INIT(1'b0)) 
    \coeff_addr_uns_s_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_uns_s[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(s00_axi_reset));
  FDRE #(
    .INIT(1'b0)) 
    \coeff_addr_uns_s_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\coeff_addr_s_reg[0]_0 ),
        .D(\coeff_addr_uns_s[4]_i_2_n_0 ),
        .Q(Q[4]),
        .R(s00_axi_reset));
  FDRE coeff_en_s_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(coeff_en_s_reg_0),
        .Q(coeff_en_s),
        .R(1'b0));
  FDRE \coeff_val_s_reg[0] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[0]),
        .Q(\coeff_val_s_reg[15]_0 [0]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[10] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[10]),
        .Q(\coeff_val_s_reg[15]_0 [10]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[11] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[11]),
        .Q(\coeff_val_s_reg[15]_0 [11]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[12] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[12]),
        .Q(\coeff_val_s_reg[15]_0 [12]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[13] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[13]),
        .Q(\coeff_val_s_reg[15]_0 [13]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[14] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[14]),
        .Q(\coeff_val_s_reg[15]_0 [14]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[15] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[15]),
        .Q(\coeff_val_s_reg[15]_0 [15]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[1] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[1]),
        .Q(\coeff_val_s_reg[15]_0 [1]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[2] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[2]),
        .Q(\coeff_val_s_reg[15]_0 [2]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[3] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[3]),
        .Q(\coeff_val_s_reg[15]_0 [3]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[4] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[4]),
        .Q(\coeff_val_s_reg[15]_0 [4]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[5] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[5]),
        .Q(\coeff_val_s_reg[15]_0 [5]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[6] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[6]),
        .Q(\coeff_val_s_reg[15]_0 [6]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[7] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[7]),
        .Q(\coeff_val_s_reg[15]_0 [7]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[8] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[8]),
        .Q(\coeff_val_s_reg[15]_0 [8]),
        .R(s00_axi_reset));
  FDRE \coeff_val_s_reg[9] 
       (.C(s00_axi_aclk),
        .CE(E),
        .D(s00_axi_wdata[9]),
        .Q(\coeff_val_s_reg[15]_0 [9]),
        .R(s00_axi_reset));
  FDRE read_ack_o_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(read_en_s),
        .Q(s00_axi_rvalid),
        .R(s00_axi_reset));
  LUT5 #(
    .INIT(32'h010F0100)) 
    \readdata_s[0]_i_1 
       (.I0(addr_s[0]),
        .I1(addr_s[1]),
        .I2(s00_axi_reset),
        .I3(read_en_s),
        .I4(s00_axi_rdata[0]),
        .O(\readdata_s[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00FC00AA)) 
    \readdata_s[30]_i_1 
       (.I0(s00_axi_rdata[1]),
        .I1(addr_s[0]),
        .I2(addr_s[1]),
        .I3(s00_axi_reset),
        .I4(read_en_s),
        .O(\readdata_s[30]_i_1_n_0 ));
  FDRE \readdata_s_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\readdata_s[0]_i_1_n_0 ),
        .Q(s00_axi_rdata[0]),
        .R(1'b0));
  FDRE \readdata_s_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\readdata_s[30]_i_1_n_0 ),
        .Q(s00_axi_rdata[1]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom
   (axi_awready_reg_0,
    axi_wready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    s00_axi_awvalid_0,
    addr_s,
    D,
    E,
    s00_axi_reset_0,
    \coeff_addr_uns_s_reg[4] ,
    read_en_s,
    s00_axi_reset,
    s00_axi_aclk,
    s00_axi_awvalid,
    s00_axi_wvalid,
    Q,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_araddr);
  output axi_awready_reg_0;
  output axi_wready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output [0:0]s00_axi_awvalid_0;
  output [1:0]addr_s;
  output [0:0]D;
  output [0:0]E;
  output s00_axi_reset_0;
  output [4:0]\coeff_addr_uns_s_reg[4] ;
  output read_en_s;
  input s00_axi_reset;
  input s00_axi_aclk;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [4:0]Q;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;
  input [1:0]s00_axi_awaddr;
  input [1:0]s00_axi_araddr;

  wire [0:0]D;
  wire [0:0]E;
  wire [4:0]Q;
  wire [1:0]addr_reg;
  wire [1:0]addr_s;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_rvalid_reg_n_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire [4:0]\coeff_addr_uns_s_reg[4] ;
  wire [1:0]read_addr_s;
  wire read_en_s;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire [0:0]s00_axi_awvalid_0;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire s00_axi_reset;
  wire s00_axi_reset_0;
  wire s00_axi_rready;
  wire s00_axi_wvalid;
  wire [1:0]write_addr_s;
  wire write_en_s;

  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \addr_reg[0]_i_1 
       (.I0(write_addr_s[0]),
        .I1(write_en_s),
        .I2(read_addr_s[0]),
        .I3(read_en_s),
        .I4(addr_reg[0]),
        .O(addr_s[0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \addr_reg[1]_i_1 
       (.I0(write_addr_s[1]),
        .I1(write_en_s),
        .I2(read_addr_s[1]),
        .I3(read_en_s),
        .I4(addr_reg[1]),
        .O(addr_s[1]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \addr_reg[1]_i_2 
       (.I0(axi_awready_reg_0),
        .I1(axi_wready_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .O(write_en_s));
  FDRE \addr_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(addr_s[0]),
        .Q(addr_reg[0]),
        .R(1'b0));
  FDRE \addr_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(addr_s[1]),
        .Q(addr_reg[1]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(read_addr_s[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(read_addr_s[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(read_addr_s[0]),
        .S(s00_axi_reset));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(read_addr_s[1]),
        .S(s00_axi_reset));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(s00_axi_reset));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .I4(write_addr_s[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .I4(write_addr_s[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(write_addr_s[0]),
        .R(s00_axi_reset));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(write_addr_s[1]),
        .R(s00_axi_reset));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(s00_axi_reset));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(s00_axi_reset));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(axi_arready_reg_0),
        .I1(s00_axi_arvalid),
        .I2(axi_rvalid_reg_n_0),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(axi_rvalid_reg_n_0),
        .R(s00_axi_reset));
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(s00_axi_reset));
  LUT2 #(
    .INIT(4'h8)) 
    \coeff_addr_s[0]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[0]),
        .O(\coeff_addr_uns_s_reg[4] [0]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \coeff_addr_s[1]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[1]),
        .O(\coeff_addr_uns_s_reg[4] [1]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \coeff_addr_s[2]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[2]),
        .O(\coeff_addr_uns_s_reg[4] [2]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \coeff_addr_s[3]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[3]),
        .O(\coeff_addr_uns_s_reg[4] [3]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \coeff_addr_s[4]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[4]),
        .O(\coeff_addr_uns_s_reg[4] [4]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \coeff_addr_uns_s[0]_i_1 
       (.I0(addr_s[0]),
        .I1(Q[0]),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'h00008000)) 
    \coeff_addr_uns_s[4]_i_1 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(addr_s[1]),
        .O(s00_axi_awvalid_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    coeff_en_s_i_1
       (.I0(addr_s[1]),
        .I1(addr_s[0]),
        .I2(write_en_s),
        .I3(s00_axi_reset),
        .O(s00_axi_reset_0));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \coeff_val_s[15]_i_1 
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_wready_reg_0),
        .I3(axi_awready_reg_0),
        .I4(addr_s[1]),
        .I5(addr_s[0]),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h08)) 
    read_ack_o_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .I2(axi_rvalid_reg_n_0),
        .O(read_en_s));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc
   (data_en_s_0,
    data_en_s,
    Q,
    E,
    res_s0,
    data_en_s_reg_0,
    res_s0_1,
    data_en_s_reg_1,
    res_s0_2,
    data_en_s_reg_2,
    res_s0_3,
    data_en_s_reg_3,
    res_s0_4,
    data_en_s_reg_4,
    res_s0_5,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    data_in_en_s,
    data_i,
    DOBDO,
    ready_next_s_1,
    end_s,
    ready_s,
    end_s_6,
    ready_s_7,
    end_s_8,
    ready_s_9,
    end_s_10,
    ready_s_11,
    end_s_12,
    ready_s_13,
    end_s_14,
    ready_s_15);
  output data_en_s_0;
  output [0:0]data_en_s;
  output [18:0]Q;
  output [0:0]E;
  output res_s0;
  output [0:0]data_en_s_reg_0;
  output res_s0_1;
  output [0:0]data_en_s_reg_1;
  output res_s0_2;
  output [0:0]data_en_s_reg_2;
  output res_s0_3;
  output [0:0]data_en_s_reg_3;
  output res_s0_4;
  output [0:0]data_en_s_reg_4;
  output res_s0_5;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input data_in_en_s;
  input [13:0]data_i;
  input [15:0]DOBDO;
  input [0:0]ready_next_s_1;
  input end_s;
  input ready_s;
  input end_s_6;
  input ready_s_7;
  input end_s_8;
  input ready_s_9;
  input end_s_10;
  input ready_s_11;
  input end_s_12;
  input ready_s_13;
  input end_s_14;
  input ready_s_15;

  wire [15:0]DOBDO;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_s;
  wire data_en_s_0;
  wire [0:0]data_en_s_reg_0;
  wire [0:0]data_en_s_reg_1;
  wire [0:0]data_en_s_reg_2;
  wire [0:0]data_en_s_reg_3;
  wire [0:0]data_en_s_reg_4;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire data_out_en_s;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire end_s_0;
  wire end_s_10;
  wire end_s_12;
  wire end_s_14;
  wire end_s_6;
  wire end_s_8;
  wire must_rst_s;
  wire must_rst_s_i_1__5_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_1;
  wire ready_s_11;
  wire ready_s_13;
  wire ready_s_15;
  wire ready_s_7;
  wire ready_s_9;
  wire ready_s_i_1_n_0;
  wire res_s0;
  wire res_s0_1;
  wire res_s0_2;
  wire res_s0_3;
  wire res_s0_4;
  wire res_s0_5;
  wire res_s0_6;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1
       (.I0(data_en_s_0),
        .I1(end_s_0),
        .O(data_out_en_s));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__0
       (.I0(data_en_s_0),
        .I1(end_s),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__1
       (.I0(data_en_s_0),
        .I1(end_s_6),
        .O(data_en_s_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__2
       (.I0(data_en_s_0),
        .I1(end_s_8),
        .O(data_en_s_reg_1));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__3
       (.I0(data_en_s_0),
        .I1(end_s_10),
        .O(data_en_s_reg_2));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__4
       (.I0(data_en_s_0),
        .I1(end_s_12),
        .O(data_en_s_reg_3));
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_i_1__5
       (.I0(data_en_s_0),
        .I1(end_s_14),
        .O(data_en_s_reg_4));
  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_out_en_s),
        .Q(data_en_s),
        .R(data_rst_i));
  FDRE data_en_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_in_en_s),
        .Q(data_en_s_0),
        .R(data_rst_i));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s_0),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_105),
        .Q(Q[0]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_95),
        .Q(Q[10]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_94),
        .Q(Q[11]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_93),
        .Q(Q[12]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_92),
        .Q(Q[13]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_91),
        .Q(Q[14]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_90),
        .Q(Q[15]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_89),
        .Q(Q[16]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_88),
        .Q(Q[17]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_87),
        .Q(Q[18]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_104),
        .Q(Q[1]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_103),
        .Q(Q[2]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_102),
        .Q(Q[3]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_101),
        .Q(Q[4]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_100),
        .Q(Q[5]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_99),
        .Q(Q[6]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_98),
        .Q(Q[7]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_97),
        .Q(Q[8]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(data_out_en_s),
        .D(res_s_reg_n_96),
        .Q(Q[9]),
        .R(data_rst_i));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__5
       (.I0(end_next_s_0),
        .I1(data_en_s_0),
        .I2(ready_s_1),
        .I3(must_rst_s),
        .O(must_rst_s_i_1__5_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__5_n_0),
        .Q(must_rst_s),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s_1),
        .O(ready_s_i_1_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1_n_0),
        .Q(ready_s_1),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO[15],DOBDO}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0_6),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1
       (.I0(data_en_s_0),
        .I1(ready_s_1),
        .O(res_s0_6));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__0
       (.I0(data_en_s_0),
        .I1(ready_s),
        .O(res_s0));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__1
       (.I0(data_en_s_0),
        .I1(ready_s_7),
        .O(res_s0_1));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__2
       (.I0(data_en_s_0),
        .I1(ready_s_9),
        .O(res_s0_2));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__3
       (.I0(data_en_s_0),
        .I1(ready_s_11),
        .O(res_s0_3));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__4
       (.I0(data_en_s_0),
        .I1(ready_s_13),
        .O(res_s0_4));
  LUT2 #(
    .INIT(4'h8)) 
    res_s_reg_i_1__5
       (.I0(data_en_s_0),
        .I1(ready_s_15),
        .O(res_s0_5));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0
   (end_s,
    data_en_s,
    ready_s,
    Q,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    ready_next_s_1,
    data_en_s_0);
  output end_s;
  output [0:0]data_en_s;
  output ready_s;
  output [18:0]Q;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input [0:0]ready_next_s_1;
  input data_en_s_0;

  wire [15:0]A;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_s;
  wire data_en_s_0;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire must_rst_s_i_1__4_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__0_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_s),
        .R(data_rst_i));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(Q[0]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(Q[10]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(Q[11]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(Q[12]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(Q[13]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(Q[14]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(Q[15]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(Q[16]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(Q[17]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(Q[18]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(Q[1]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(Q[2]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(Q[3]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(Q[4]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(Q[5]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(Q[6]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(Q[7]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(Q[8]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(Q[9]),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__4
       (.I0(end_next_s_0),
        .I1(data_en_s_0),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1__4_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__4_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__0
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__0_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__0_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1
   (end_s,
    data_en_s,
    ready_s,
    Q,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    ready_next_s_1,
    data_en_s_0);
  output end_s;
  output [0:0]data_en_s;
  output ready_s;
  output [18:0]Q;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input [0:0]ready_next_s_1;
  input data_en_s_0;

  wire [15:0]A;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_s;
  wire data_en_s_0;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire must_rst_s_i_1__3_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__1_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_s),
        .R(data_rst_i));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(Q[0]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(Q[10]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(Q[11]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(Q[12]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(Q[13]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(Q[14]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(Q[15]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(Q[16]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(Q[17]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(Q[18]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(Q[1]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(Q[2]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(Q[3]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(Q[4]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(Q[5]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(Q[6]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(Q[7]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(Q[8]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(Q[9]),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__3
       (.I0(end_next_s_0),
        .I1(data_en_s_0),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1__3_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__3_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__1
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__1_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__1_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2
   (end_s,
    data_en_s,
    ready_s,
    D,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    ready_next_s_1,
    data_en_s_0,
    \data_out_s_reg[18] ,
    \data_out_s_reg[18]_0 ,
    Q,
    \data_out_s_reg[18]_1 ,
    \data_out_s_reg[18]_2 ,
    \data_out_s_reg[18]_3 ,
    \data_out_s_reg[18]_4 ,
    \data_out_s_reg[17] ,
    \data_out_s_reg[16] ,
    \data_out_s_reg[15] ,
    \data_out_s_reg[14] ,
    \data_out_s_reg[13] ,
    \data_out_s_reg[12] ,
    \data_out_s_reg[11] ,
    \data_out_s_reg[10] ,
    \data_out_s_reg[9] ,
    \data_out_s_reg[8] ,
    \data_out_s_reg[7] ,
    \data_out_s_reg[6] ,
    \data_out_s_reg[5] ,
    \data_out_s_reg[4] ,
    \data_out_s_reg[3] ,
    \data_out_s_reg[2] ,
    \data_out_s_reg[1] ,
    \data_out_s_reg[0] );
  output end_s;
  output [0:0]data_en_s;
  output ready_s;
  output [18:0]D;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input [0:0]ready_next_s_1;
  input data_en_s_0;
  input \data_out_s_reg[18] ;
  input \data_out_s_reg[18]_0 ;
  input [18:0]Q;
  input \data_out_s_reg[18]_1 ;
  input [18:0]\data_out_s_reg[18]_2 ;
  input \data_out_s_reg[18]_3 ;
  input [18:0]\data_out_s_reg[18]_4 ;
  input \data_out_s_reg[17] ;
  input \data_out_s_reg[16] ;
  input \data_out_s_reg[15] ;
  input \data_out_s_reg[14] ;
  input \data_out_s_reg[13] ;
  input \data_out_s_reg[12] ;
  input \data_out_s_reg[11] ;
  input \data_out_s_reg[10] ;
  input \data_out_s_reg[9] ;
  input \data_out_s_reg[8] ;
  input \data_out_s_reg[7] ;
  input \data_out_s_reg[6] ;
  input \data_out_s_reg[5] ;
  input \data_out_s_reg[4] ;
  input \data_out_s_reg[3] ;
  input \data_out_s_reg[2] ;
  input \data_out_s_reg[1] ;
  input \data_out_s_reg[0] ;

  wire [15:0]A;
  wire [18:0]D;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_s;
  wire data_en_s_0;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire \data_out_s[0]_i_2_n_0 ;
  wire \data_out_s[10]_i_2_n_0 ;
  wire \data_out_s[11]_i_2_n_0 ;
  wire \data_out_s[12]_i_2_n_0 ;
  wire \data_out_s[13]_i_2_n_0 ;
  wire \data_out_s[14]_i_2_n_0 ;
  wire \data_out_s[15]_i_2_n_0 ;
  wire \data_out_s[16]_i_2_n_0 ;
  wire \data_out_s[17]_i_2_n_0 ;
  wire \data_out_s[18]_i_4_n_0 ;
  wire \data_out_s[1]_i_2_n_0 ;
  wire \data_out_s[2]_i_2_n_0 ;
  wire \data_out_s[3]_i_2_n_0 ;
  wire \data_out_s[4]_i_2_n_0 ;
  wire \data_out_s[5]_i_2_n_0 ;
  wire \data_out_s[6]_i_2_n_0 ;
  wire \data_out_s[7]_i_2_n_0 ;
  wire \data_out_s[8]_i_2_n_0 ;
  wire \data_out_s[9]_i_2_n_0 ;
  wire \data_out_s_reg[0] ;
  wire \data_out_s_reg[10] ;
  wire \data_out_s_reg[11] ;
  wire \data_out_s_reg[12] ;
  wire \data_out_s_reg[13] ;
  wire \data_out_s_reg[14] ;
  wire \data_out_s_reg[15] ;
  wire \data_out_s_reg[16] ;
  wire \data_out_s_reg[17] ;
  wire \data_out_s_reg[18] ;
  wire \data_out_s_reg[18]_0 ;
  wire \data_out_s_reg[18]_1 ;
  wire [18:0]\data_out_s_reg[18]_2 ;
  wire \data_out_s_reg[18]_3 ;
  wire [18:0]\data_out_s_reg[18]_4 ;
  wire \data_out_s_reg[1] ;
  wire \data_out_s_reg[2] ;
  wire \data_out_s_reg[3] ;
  wire \data_out_s_reg[4] ;
  wire \data_out_s_reg[5] ;
  wire \data_out_s_reg[6] ;
  wire \data_out_s_reg[7] ;
  wire \data_out_s_reg[8] ;
  wire \data_out_s_reg[9] ;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire \final_res_s_reg_n_0_[0] ;
  wire \final_res_s_reg_n_0_[10] ;
  wire \final_res_s_reg_n_0_[11] ;
  wire \final_res_s_reg_n_0_[12] ;
  wire \final_res_s_reg_n_0_[13] ;
  wire \final_res_s_reg_n_0_[14] ;
  wire \final_res_s_reg_n_0_[15] ;
  wire \final_res_s_reg_n_0_[16] ;
  wire \final_res_s_reg_n_0_[17] ;
  wire \final_res_s_reg_n_0_[18] ;
  wire \final_res_s_reg_n_0_[1] ;
  wire \final_res_s_reg_n_0_[2] ;
  wire \final_res_s_reg_n_0_[3] ;
  wire \final_res_s_reg_n_0_[4] ;
  wire \final_res_s_reg_n_0_[5] ;
  wire \final_res_s_reg_n_0_[6] ;
  wire \final_res_s_reg_n_0_[7] ;
  wire \final_res_s_reg_n_0_[8] ;
  wire \final_res_s_reg_n_0_[9] ;
  wire must_rst_s_i_1__2_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__2_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_s),
        .R(data_rst_i));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[0]_i_2 
       (.I0(\final_res_s_reg_n_0_[0] ),
        .I1(Q[0]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [0]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [0]),
        .O(\data_out_s[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[10]_i_2 
       (.I0(\final_res_s_reg_n_0_[10] ),
        .I1(Q[10]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [10]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [10]),
        .O(\data_out_s[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[11]_i_2 
       (.I0(\final_res_s_reg_n_0_[11] ),
        .I1(Q[11]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [11]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [11]),
        .O(\data_out_s[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[12]_i_2 
       (.I0(\final_res_s_reg_n_0_[12] ),
        .I1(Q[12]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [12]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [12]),
        .O(\data_out_s[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[13]_i_2 
       (.I0(\final_res_s_reg_n_0_[13] ),
        .I1(Q[13]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [13]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [13]),
        .O(\data_out_s[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[14]_i_2 
       (.I0(\final_res_s_reg_n_0_[14] ),
        .I1(Q[14]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [14]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [14]),
        .O(\data_out_s[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[15]_i_2 
       (.I0(\final_res_s_reg_n_0_[15] ),
        .I1(Q[15]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [15]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [15]),
        .O(\data_out_s[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[16]_i_2 
       (.I0(\final_res_s_reg_n_0_[16] ),
        .I1(Q[16]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [16]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [16]),
        .O(\data_out_s[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[17]_i_2 
       (.I0(\final_res_s_reg_n_0_[17] ),
        .I1(Q[17]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [17]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [17]),
        .O(\data_out_s[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[18]_i_4 
       (.I0(\final_res_s_reg_n_0_[18] ),
        .I1(Q[18]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [18]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [18]),
        .O(\data_out_s[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[1]_i_2 
       (.I0(\final_res_s_reg_n_0_[1] ),
        .I1(Q[1]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [1]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [1]),
        .O(\data_out_s[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[2]_i_2 
       (.I0(\final_res_s_reg_n_0_[2] ),
        .I1(Q[2]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [2]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [2]),
        .O(\data_out_s[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[3]_i_2 
       (.I0(\final_res_s_reg_n_0_[3] ),
        .I1(Q[3]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [3]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [3]),
        .O(\data_out_s[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[4]_i_2 
       (.I0(\final_res_s_reg_n_0_[4] ),
        .I1(Q[4]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [4]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [4]),
        .O(\data_out_s[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[5]_i_2 
       (.I0(\final_res_s_reg_n_0_[5] ),
        .I1(Q[5]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [5]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [5]),
        .O(\data_out_s[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[6]_i_2 
       (.I0(\final_res_s_reg_n_0_[6] ),
        .I1(Q[6]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [6]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [6]),
        .O(\data_out_s[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[7]_i_2 
       (.I0(\final_res_s_reg_n_0_[7] ),
        .I1(Q[7]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [7]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [7]),
        .O(\data_out_s[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[8]_i_2 
       (.I0(\final_res_s_reg_n_0_[8] ),
        .I1(Q[8]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [8]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [8]),
        .O(\data_out_s[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \data_out_s[9]_i_2 
       (.I0(\final_res_s_reg_n_0_[9] ),
        .I1(Q[9]),
        .I2(\data_out_s_reg[18]_1 ),
        .I3(\data_out_s_reg[18]_2 [9]),
        .I4(\data_out_s_reg[18]_3 ),
        .I5(\data_out_s_reg[18]_4 [9]),
        .O(\data_out_s[9]_i_2_n_0 ));
  MUXF7 \data_out_s_reg[0]_i_1 
       (.I0(\data_out_s[0]_i_2_n_0 ),
        .I1(\data_out_s_reg[0] ),
        .O(D[0]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[10]_i_1 
       (.I0(\data_out_s[10]_i_2_n_0 ),
        .I1(\data_out_s_reg[10] ),
        .O(D[10]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[11]_i_1 
       (.I0(\data_out_s[11]_i_2_n_0 ),
        .I1(\data_out_s_reg[11] ),
        .O(D[11]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[12]_i_1 
       (.I0(\data_out_s[12]_i_2_n_0 ),
        .I1(\data_out_s_reg[12] ),
        .O(D[12]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[13]_i_1 
       (.I0(\data_out_s[13]_i_2_n_0 ),
        .I1(\data_out_s_reg[13] ),
        .O(D[13]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[14]_i_1 
       (.I0(\data_out_s[14]_i_2_n_0 ),
        .I1(\data_out_s_reg[14] ),
        .O(D[14]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[15]_i_1 
       (.I0(\data_out_s[15]_i_2_n_0 ),
        .I1(\data_out_s_reg[15] ),
        .O(D[15]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[16]_i_1 
       (.I0(\data_out_s[16]_i_2_n_0 ),
        .I1(\data_out_s_reg[16] ),
        .O(D[16]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[17]_i_1 
       (.I0(\data_out_s[17]_i_2_n_0 ),
        .I1(\data_out_s_reg[17] ),
        .O(D[17]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[18]_i_2 
       (.I0(\data_out_s[18]_i_4_n_0 ),
        .I1(\data_out_s_reg[18]_0 ),
        .O(D[18]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[1]_i_1 
       (.I0(\data_out_s[1]_i_2_n_0 ),
        .I1(\data_out_s_reg[1] ),
        .O(D[1]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[2]_i_1 
       (.I0(\data_out_s[2]_i_2_n_0 ),
        .I1(\data_out_s_reg[2] ),
        .O(D[2]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[3]_i_1 
       (.I0(\data_out_s[3]_i_2_n_0 ),
        .I1(\data_out_s_reg[3] ),
        .O(D[3]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[4]_i_1 
       (.I0(\data_out_s[4]_i_2_n_0 ),
        .I1(\data_out_s_reg[4] ),
        .O(D[4]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[5]_i_1 
       (.I0(\data_out_s[5]_i_2_n_0 ),
        .I1(\data_out_s_reg[5] ),
        .O(D[5]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[6]_i_1 
       (.I0(\data_out_s[6]_i_2_n_0 ),
        .I1(\data_out_s_reg[6] ),
        .O(D[6]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[7]_i_1 
       (.I0(\data_out_s[7]_i_2_n_0 ),
        .I1(\data_out_s_reg[7] ),
        .O(D[7]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[8]_i_1 
       (.I0(\data_out_s[8]_i_2_n_0 ),
        .I1(\data_out_s_reg[8] ),
        .O(D[8]),
        .S(\data_out_s_reg[18] ));
  MUXF7 \data_out_s_reg[9]_i_1 
       (.I0(\data_out_s[9]_i_2_n_0 ),
        .I1(\data_out_s_reg[9] ),
        .O(D[9]),
        .S(\data_out_s_reg[18] ));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(\final_res_s_reg_n_0_[0] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(\final_res_s_reg_n_0_[10] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(\final_res_s_reg_n_0_[11] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(\final_res_s_reg_n_0_[12] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(\final_res_s_reg_n_0_[13] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(\final_res_s_reg_n_0_[14] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(\final_res_s_reg_n_0_[15] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(\final_res_s_reg_n_0_[16] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(\final_res_s_reg_n_0_[17] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(\final_res_s_reg_n_0_[18] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(\final_res_s_reg_n_0_[1] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(\final_res_s_reg_n_0_[2] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(\final_res_s_reg_n_0_[3] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(\final_res_s_reg_n_0_[4] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(\final_res_s_reg_n_0_[5] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(\final_res_s_reg_n_0_[6] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(\final_res_s_reg_n_0_[7] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(\final_res_s_reg_n_0_[8] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(\final_res_s_reg_n_0_[9] ),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__2
       (.I0(end_next_s_0),
        .I1(data_en_s_0),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1__2_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__2_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__2
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__2_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__2_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3
   (end_s,
    ready_s,
    data_en_o_reg_0,
    data_en_o_reg_1,
    data_en_o_reg_2,
    data_en_o_reg_3,
    Q,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    \cpt_store_s_reg[2] ,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    \cpt_store_s_reg[0] ,
    \cpt_store_s_reg[0]_0 ,
    \cpt_store_s_reg[0]_1 ,
    ready_next_s_1,
    data_en_s);
  output end_s;
  output ready_s;
  output data_en_o_reg_0;
  output data_en_o_reg_1;
  output data_en_o_reg_2;
  output [0:0]data_en_o_reg_3;
  output [18:0]Q;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [5:0]\cpt_store_s_reg[2] ;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input \cpt_store_s_reg[0] ;
  input \cpt_store_s_reg[0]_0 ;
  input \cpt_store_s_reg[0]_1 ;
  input [0:0]ready_next_s_1;
  input data_en_s;

  wire [15:0]A;
  wire [0:0]E;
  wire [18:0]Q;
  wire \cpt_store_s_reg[0] ;
  wire \cpt_store_s_reg[0]_0 ;
  wire \cpt_store_s_reg[0]_1 ;
  wire [5:0]\cpt_store_s_reg[2] ;
  wire data_clk_i;
  wire data_en_o_reg_0;
  wire data_en_o_reg_1;
  wire data_en_o_reg_2;
  wire [0:0]data_en_o_reg_3;
  wire data_en_s;
  wire [4:4]data_en_s__0;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire \data_out_s[18]_i_3_n_0 ;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire must_rst_s_i_1__1_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__3_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0101000E01010E0E)) 
    \cpt_store_s[0]_i_1 
       (.I0(\data_out_s[18]_i_3_n_0 ),
        .I1(\cpt_store_s_reg[2] [4]),
        .I2(data_rst_i),
        .I3(\cpt_store_s_reg[0] ),
        .I4(\cpt_store_s_reg[0]_0 ),
        .I5(\cpt_store_s_reg[0]_1 ),
        .O(data_en_o_reg_0));
  LUT6 #(
    .INIT(64'h010E0100010E0F00)) 
    \cpt_store_s[1]_i_1 
       (.I0(\data_out_s[18]_i_3_n_0 ),
        .I1(\cpt_store_s_reg[2] [4]),
        .I2(data_rst_i),
        .I3(\cpt_store_s_reg[0] ),
        .I4(\cpt_store_s_reg[0]_0 ),
        .I5(\cpt_store_s_reg[0]_1 ),
        .O(data_en_o_reg_1));
  LUT6 #(
    .INIT(64'h010F010F0E000000)) 
    \cpt_store_s[2]_i_1 
       (.I0(\data_out_s[18]_i_3_n_0 ),
        .I1(\cpt_store_s_reg[2] [4]),
        .I2(data_rst_i),
        .I3(\cpt_store_s_reg[0] ),
        .I4(\cpt_store_s_reg[0]_0 ),
        .I5(\cpt_store_s_reg[0]_1 ),
        .O(data_en_o_reg_2));
  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_s__0),
        .R(data_rst_i));
  LUT2 #(
    .INIT(4'hE)) 
    \data_out_s[18]_i_1 
       (.I0(\data_out_s[18]_i_3_n_0 ),
        .I1(\cpt_store_s_reg[2] [4]),
        .O(data_en_o_reg_3));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \data_out_s[18]_i_3 
       (.I0(data_en_s__0),
        .I1(\cpt_store_s_reg[2] [2]),
        .I2(\cpt_store_s_reg[2] [0]),
        .I3(\cpt_store_s_reg[2] [1]),
        .I4(\cpt_store_s_reg[2] [3]),
        .I5(\cpt_store_s_reg[2] [5]),
        .O(\data_out_s[18]_i_3_n_0 ));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(Q[0]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(Q[10]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(Q[11]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(Q[12]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(Q[13]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(Q[14]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(Q[15]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(Q[16]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(Q[17]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(Q[18]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(Q[1]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(Q[2]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(Q[3]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(Q[4]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(Q[5]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(Q[6]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(Q[7]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(Q[8]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(Q[9]),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__1
       (.I0(end_next_s_0),
        .I1(data_en_s),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1__1_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__1_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__3
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__3_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__3_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4
   (end_s,
    data_en_o_reg_0,
    ready_s,
    Q,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    ready_next_s_1,
    data_en_s);
  output end_s;
  output [0:0]data_en_o_reg_0;
  output ready_s;
  output [18:0]Q;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input [0:0]ready_next_s_1;
  input data_en_s;

  wire [15:0]A;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_o_reg_0;
  wire data_en_s;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire must_rst_s_i_1__0_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__4_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_o_reg_0),
        .R(data_rst_i));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(Q[0]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(Q[10]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(Q[11]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(Q[12]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(Q[13]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(Q[14]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(Q[15]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(Q[16]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(Q[17]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(Q[18]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(Q[1]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(Q[2]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(Q[3]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(Q[4]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(Q[5]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(Q[6]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(Q[7]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(Q[8]),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(Q[9]),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1__0
       (.I0(end_next_s_0),
        .I1(data_en_s),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1__0_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1__0_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__4
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__4_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__4_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "firReal_proc" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5
   (end_s,
    data_en_o_reg_0,
    ready_s,
    \final_res_s_reg[18]_0 ,
    \final_res_s_reg[17]_0 ,
    \final_res_s_reg[16]_0 ,
    \final_res_s_reg[15]_0 ,
    \final_res_s_reg[14]_0 ,
    \final_res_s_reg[13]_0 ,
    \final_res_s_reg[12]_0 ,
    \final_res_s_reg[11]_0 ,
    \final_res_s_reg[10]_0 ,
    \final_res_s_reg[9]_0 ,
    \final_res_s_reg[8]_0 ,
    \final_res_s_reg[7]_0 ,
    \final_res_s_reg[6]_0 ,
    \final_res_s_reg[5]_0 ,
    \final_res_s_reg[4]_0 ,
    \final_res_s_reg[3]_0 ,
    \final_res_s_reg[2]_0 ,
    \final_res_s_reg[1]_0 ,
    \final_res_s_reg[0]_0 ,
    data_rst_i,
    end_next_s_0,
    data_clk_i,
    E,
    data_in_en_s,
    res_s0,
    data_i,
    A,
    ready_next_s_1,
    data_en_s,
    \data_out_s_reg[18] ,
    Q,
    \data_out_s_reg[18]_0 ,
    \data_out_s_reg[18]_1 );
  output end_s;
  output [0:0]data_en_o_reg_0;
  output ready_s;
  output \final_res_s_reg[18]_0 ;
  output \final_res_s_reg[17]_0 ;
  output \final_res_s_reg[16]_0 ;
  output \final_res_s_reg[15]_0 ;
  output \final_res_s_reg[14]_0 ;
  output \final_res_s_reg[13]_0 ;
  output \final_res_s_reg[12]_0 ;
  output \final_res_s_reg[11]_0 ;
  output \final_res_s_reg[10]_0 ;
  output \final_res_s_reg[9]_0 ;
  output \final_res_s_reg[8]_0 ;
  output \final_res_s_reg[7]_0 ;
  output \final_res_s_reg[6]_0 ;
  output \final_res_s_reg[5]_0 ;
  output \final_res_s_reg[4]_0 ;
  output \final_res_s_reg[3]_0 ;
  output \final_res_s_reg[2]_0 ;
  output \final_res_s_reg[1]_0 ;
  output \final_res_s_reg[0]_0 ;
  input data_rst_i;
  input [0:0]end_next_s_0;
  input data_clk_i;
  input [0:0]E;
  input data_in_en_s;
  input res_s0;
  input [13:0]data_i;
  input [15:0]A;
  input [0:0]ready_next_s_1;
  input data_en_s;
  input \data_out_s_reg[18] ;
  input [18:0]Q;
  input \data_out_s_reg[18]_0 ;
  input [18:0]\data_out_s_reg[18]_1 ;

  wire [15:0]A;
  wire [0:0]E;
  wire [18:0]Q;
  wire data_clk_i;
  wire [0:0]data_en_o_reg_0;
  wire data_en_s;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire \data_out_s_reg[18] ;
  wire \data_out_s_reg[18]_0 ;
  wire [18:0]\data_out_s_reg[18]_1 ;
  wire data_rst_i;
  wire [0:0]end_next_s_0;
  wire end_s;
  wire \final_res_s_reg[0]_0 ;
  wire \final_res_s_reg[10]_0 ;
  wire \final_res_s_reg[11]_0 ;
  wire \final_res_s_reg[12]_0 ;
  wire \final_res_s_reg[13]_0 ;
  wire \final_res_s_reg[14]_0 ;
  wire \final_res_s_reg[15]_0 ;
  wire \final_res_s_reg[16]_0 ;
  wire \final_res_s_reg[17]_0 ;
  wire \final_res_s_reg[18]_0 ;
  wire \final_res_s_reg[1]_0 ;
  wire \final_res_s_reg[2]_0 ;
  wire \final_res_s_reg[3]_0 ;
  wire \final_res_s_reg[4]_0 ;
  wire \final_res_s_reg[5]_0 ;
  wire \final_res_s_reg[6]_0 ;
  wire \final_res_s_reg[7]_0 ;
  wire \final_res_s_reg[8]_0 ;
  wire \final_res_s_reg[9]_0 ;
  wire \final_res_s_reg_n_0_[0] ;
  wire \final_res_s_reg_n_0_[10] ;
  wire \final_res_s_reg_n_0_[11] ;
  wire \final_res_s_reg_n_0_[12] ;
  wire \final_res_s_reg_n_0_[13] ;
  wire \final_res_s_reg_n_0_[14] ;
  wire \final_res_s_reg_n_0_[15] ;
  wire \final_res_s_reg_n_0_[16] ;
  wire \final_res_s_reg_n_0_[17] ;
  wire \final_res_s_reg_n_0_[18] ;
  wire \final_res_s_reg_n_0_[1] ;
  wire \final_res_s_reg_n_0_[2] ;
  wire \final_res_s_reg_n_0_[3] ;
  wire \final_res_s_reg_n_0_[4] ;
  wire \final_res_s_reg_n_0_[5] ;
  wire \final_res_s_reg_n_0_[6] ;
  wire \final_res_s_reg_n_0_[7] ;
  wire \final_res_s_reg_n_0_[8] ;
  wire \final_res_s_reg_n_0_[9] ;
  wire must_rst_s_i_1_n_0;
  wire must_rst_s_reg_n_0;
  wire [0:0]ready_next_s_1;
  wire ready_s;
  wire ready_s_i_1__5_n_0;
  wire res_s0;
  wire res_s_reg_n_100;
  wire res_s_reg_n_101;
  wire res_s_reg_n_102;
  wire res_s_reg_n_103;
  wire res_s_reg_n_104;
  wire res_s_reg_n_105;
  wire res_s_reg_n_71;
  wire res_s_reg_n_72;
  wire res_s_reg_n_73;
  wire res_s_reg_n_74;
  wire res_s_reg_n_75;
  wire res_s_reg_n_76;
  wire res_s_reg_n_77;
  wire res_s_reg_n_78;
  wire res_s_reg_n_79;
  wire res_s_reg_n_80;
  wire res_s_reg_n_81;
  wire res_s_reg_n_82;
  wire res_s_reg_n_83;
  wire res_s_reg_n_84;
  wire res_s_reg_n_85;
  wire res_s_reg_n_86;
  wire res_s_reg_n_87;
  wire res_s_reg_n_88;
  wire res_s_reg_n_89;
  wire res_s_reg_n_90;
  wire res_s_reg_n_91;
  wire res_s_reg_n_92;
  wire res_s_reg_n_93;
  wire res_s_reg_n_94;
  wire res_s_reg_n_95;
  wire res_s_reg_n_96;
  wire res_s_reg_n_97;
  wire res_s_reg_n_98;
  wire res_s_reg_n_99;
  wire NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_res_s_reg_OVERFLOW_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_res_s_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_res_s_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_res_s_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_res_s_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_res_s_reg_CARRYOUT_UNCONNECTED;
  wire [47:35]NLW_res_s_reg_P_UNCONNECTED;
  wire [47:0]NLW_res_s_reg_PCOUT_UNCONNECTED;

  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(E),
        .Q(data_en_o_reg_0),
        .R(data_rst_i));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[0]_i_3 
       (.I0(\final_res_s_reg_n_0_[0] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[0]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [0]),
        .O(\final_res_s_reg[0]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[10]_i_3 
       (.I0(\final_res_s_reg_n_0_[10] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[10]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [10]),
        .O(\final_res_s_reg[10]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[11]_i_3 
       (.I0(\final_res_s_reg_n_0_[11] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[11]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [11]),
        .O(\final_res_s_reg[11]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[12]_i_3 
       (.I0(\final_res_s_reg_n_0_[12] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[12]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [12]),
        .O(\final_res_s_reg[12]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[13]_i_3 
       (.I0(\final_res_s_reg_n_0_[13] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[13]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [13]),
        .O(\final_res_s_reg[13]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[14]_i_3 
       (.I0(\final_res_s_reg_n_0_[14] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[14]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [14]),
        .O(\final_res_s_reg[14]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[15]_i_3 
       (.I0(\final_res_s_reg_n_0_[15] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[15]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [15]),
        .O(\final_res_s_reg[15]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[16]_i_3 
       (.I0(\final_res_s_reg_n_0_[16] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[16]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [16]),
        .O(\final_res_s_reg[16]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[17]_i_3 
       (.I0(\final_res_s_reg_n_0_[17] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[17]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [17]),
        .O(\final_res_s_reg[17]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[18]_i_5 
       (.I0(\final_res_s_reg_n_0_[18] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[18]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [18]),
        .O(\final_res_s_reg[18]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[1]_i_3 
       (.I0(\final_res_s_reg_n_0_[1] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[1]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [1]),
        .O(\final_res_s_reg[1]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[2]_i_3 
       (.I0(\final_res_s_reg_n_0_[2] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[2]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [2]),
        .O(\final_res_s_reg[2]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[3]_i_3 
       (.I0(\final_res_s_reg_n_0_[3] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[3]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [3]),
        .O(\final_res_s_reg[3]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[4]_i_3 
       (.I0(\final_res_s_reg_n_0_[4] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[4]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [4]),
        .O(\final_res_s_reg[4]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[5]_i_3 
       (.I0(\final_res_s_reg_n_0_[5] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[5]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [5]),
        .O(\final_res_s_reg[5]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[6]_i_3 
       (.I0(\final_res_s_reg_n_0_[6] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[6]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [6]),
        .O(\final_res_s_reg[6]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[7]_i_3 
       (.I0(\final_res_s_reg_n_0_[7] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[7]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [7]),
        .O(\final_res_s_reg[7]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[8]_i_3 
       (.I0(\final_res_s_reg_n_0_[8] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[8]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [8]),
        .O(\final_res_s_reg[8]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \data_out_s[9]_i_3 
       (.I0(\final_res_s_reg_n_0_[9] ),
        .I1(\data_out_s_reg[18] ),
        .I2(Q[9]),
        .I3(\data_out_s_reg[18]_0 ),
        .I4(\data_out_s_reg[18]_1 [9]),
        .O(\final_res_s_reg[9]_0 ));
  FDRE end_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_next_s_0),
        .Q(end_s),
        .R(data_rst_i));
  FDRE \final_res_s_reg[0] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_105),
        .Q(\final_res_s_reg_n_0_[0] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[10] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_95),
        .Q(\final_res_s_reg_n_0_[10] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[11] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_94),
        .Q(\final_res_s_reg_n_0_[11] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[12] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_93),
        .Q(\final_res_s_reg_n_0_[12] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[13] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_92),
        .Q(\final_res_s_reg_n_0_[13] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[14] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_91),
        .Q(\final_res_s_reg_n_0_[14] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[15] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_90),
        .Q(\final_res_s_reg_n_0_[15] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[16] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_89),
        .Q(\final_res_s_reg_n_0_[16] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[17] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_88),
        .Q(\final_res_s_reg_n_0_[17] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[18] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_87),
        .Q(\final_res_s_reg_n_0_[18] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[1] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_104),
        .Q(\final_res_s_reg_n_0_[1] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[2] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_103),
        .Q(\final_res_s_reg_n_0_[2] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[3] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_102),
        .Q(\final_res_s_reg_n_0_[3] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[4] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_101),
        .Q(\final_res_s_reg_n_0_[4] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[5] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_100),
        .Q(\final_res_s_reg_n_0_[5] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[6] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_99),
        .Q(\final_res_s_reg_n_0_[6] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[7] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_98),
        .Q(\final_res_s_reg_n_0_[7] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[8] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_97),
        .Q(\final_res_s_reg_n_0_[8] ),
        .R(data_rst_i));
  FDRE \final_res_s_reg[9] 
       (.C(data_clk_i),
        .CE(E),
        .D(res_s_reg_n_96),
        .Q(\final_res_s_reg_n_0_[9] ),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'hBF80)) 
    must_rst_s_i_1
       (.I0(end_next_s_0),
        .I1(data_en_s),
        .I2(ready_s),
        .I3(must_rst_s_reg_n_0),
        .O(must_rst_s_i_1_n_0));
  FDRE must_rst_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(must_rst_s_i_1_n_0),
        .Q(must_rst_s_reg_n_0),
        .R(data_rst_i));
  LUT4 #(
    .INIT(16'h8F88)) 
    ready_s_i_1__5
       (.I0(ready_next_s_1),
        .I1(data_in_en_s),
        .I2(end_next_s_0),
        .I3(ready_s),
        .O(ready_s_i_1__5_n_0));
  FDRE ready_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(ready_s_i_1__5_n_0),
        .Q(ready_s),
        .R(data_rst_i));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    res_s_reg
       (.A({A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A[15],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_res_s_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({data_i[13],data_i[13],data_i[13],data_i[13],data_i}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_res_s_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_res_s_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(data_in_en_s),
        .CEA2(data_in_en_s),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(data_in_en_s),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(res_s0),
        .CLK(data_clk_i),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,must_rst_s_reg_n_0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_res_s_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_res_s_reg_P_UNCONNECTED[47:35],res_s_reg_n_71,res_s_reg_n_72,res_s_reg_n_73,res_s_reg_n_74,res_s_reg_n_75,res_s_reg_n_76,res_s_reg_n_77,res_s_reg_n_78,res_s_reg_n_79,res_s_reg_n_80,res_s_reg_n_81,res_s_reg_n_82,res_s_reg_n_83,res_s_reg_n_84,res_s_reg_n_85,res_s_reg_n_86,res_s_reg_n_87,res_s_reg_n_88,res_s_reg_n_89,res_s_reg_n_90,res_s_reg_n_91,res_s_reg_n_92,res_s_reg_n_93,res_s_reg_n_94,res_s_reg_n_95,res_s_reg_n_96,res_s_reg_n_97,res_s_reg_n_98,res_s_reg_n_99,res_s_reg_n_100,res_s_reg_n_101,res_s_reg_n_102,res_s_reg_n_103,res_s_reg_n_104,res_s_reg_n_105}),
        .PATTERNBDETECT(NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_res_s_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_res_s_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(data_rst_i),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(data_rst_i),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(data_rst_i),
        .UNDERFLOW(NLW_res_s_reg_UNDERFLOW_UNCONNECTED));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram
   (DOBDO,
    s00_axi_aclk,
    data_clk_i,
    coeff_en_s,
    mem_reg_0,
    Q,
    mem_reg_1);
  output [15:0]DOBDO;
  input s00_axi_aclk;
  input data_clk_i;
  input coeff_en_s;
  input [4:0]mem_reg_0;
  input [4:0]Q;
  input [15:0]mem_reg_1;

  wire [15:0]DOBDO;
  wire [4:0]Q;
  wire coeff_en_s;
  wire data_clk_i;
  wire [4:0]mem_reg_0;
  wire [15:0]mem_reg_1;
  wire s00_axi_aclk;
  wire [15:0]NLW_mem_reg_DOADO_UNCONNECTED;
  wire [1:0]NLW_mem_reg_DOPADOP_UNCONNECTED;
  wire [1:0]NLW_mem_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d16" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-4 {cell *THIS*} {string 5}} {SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "512" *) 
  (* RTL_RAM_NAME = "U0/fir_top_inst/ram_coeff/mem_reg" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "1023" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "15" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    mem_reg
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,mem_reg_0,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b0,1'b0,1'b0,1'b0,1'b0,Q,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(data_clk_i),
        .DIADI(mem_reg_1),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO(NLW_mem_reg_DOADO_UNCONNECTED[15:0]),
        .DOBDO(DOBDO),
        .DOPADOP(NLW_mem_reg_DOPADOP_UNCONNECTED[1:0]),
        .DOPBDOP(NLW_mem_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(coeff_en_s),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b1,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top
   (tick_o,
    data_o,
    data_rst_i,
    data_clk_i,
    s00_axi_aclk,
    coeff_en_s,
    mem_reg,
    mem_reg_0,
    data_en_i,
    data_i);
  output tick_o;
  output [18:0]data_o;
  input data_rst_i;
  input data_clk_i;
  input s00_axi_aclk;
  input coeff_en_s;
  input [4:0]mem_reg;
  input [15:0]mem_reg_0;
  input data_en_i;
  input [13:0]data_i;

  wire coeff_en_s;
  wire [15:0]coeff_s;
  wire \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire [15:0]\coeff_tab_s_reg[13] ;
  wire \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire [15:0]\coeff_tab_s_reg[18] ;
  wire \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire [15:0]\coeff_tab_s_reg[23] ;
  wire \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire [15:0]\coeff_tab_s_reg[28] ;
  wire \coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire [15:0]\coeff_tab_s_reg[3] ;
  wire \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire [15:0]\coeff_tab_s_reg[8] ;
  wire coeff_tab_s_reg_gate__0_n_0;
  wire coeff_tab_s_reg_gate__10_n_0;
  wire coeff_tab_s_reg_gate__11_n_0;
  wire coeff_tab_s_reg_gate__12_n_0;
  wire coeff_tab_s_reg_gate__13_n_0;
  wire coeff_tab_s_reg_gate__14_n_0;
  wire coeff_tab_s_reg_gate__15_n_0;
  wire coeff_tab_s_reg_gate__16_n_0;
  wire coeff_tab_s_reg_gate__17_n_0;
  wire coeff_tab_s_reg_gate__18_n_0;
  wire coeff_tab_s_reg_gate__19_n_0;
  wire coeff_tab_s_reg_gate__1_n_0;
  wire coeff_tab_s_reg_gate__20_n_0;
  wire coeff_tab_s_reg_gate__21_n_0;
  wire coeff_tab_s_reg_gate__22_n_0;
  wire coeff_tab_s_reg_gate__23_n_0;
  wire coeff_tab_s_reg_gate__24_n_0;
  wire coeff_tab_s_reg_gate__25_n_0;
  wire coeff_tab_s_reg_gate__26_n_0;
  wire coeff_tab_s_reg_gate__27_n_0;
  wire coeff_tab_s_reg_gate__28_n_0;
  wire coeff_tab_s_reg_gate__29_n_0;
  wire coeff_tab_s_reg_gate__2_n_0;
  wire coeff_tab_s_reg_gate__30_n_0;
  wire coeff_tab_s_reg_gate__31_n_0;
  wire coeff_tab_s_reg_gate__32_n_0;
  wire coeff_tab_s_reg_gate__33_n_0;
  wire coeff_tab_s_reg_gate__34_n_0;
  wire coeff_tab_s_reg_gate__35_n_0;
  wire coeff_tab_s_reg_gate__36_n_0;
  wire coeff_tab_s_reg_gate__37_n_0;
  wire coeff_tab_s_reg_gate__38_n_0;
  wire coeff_tab_s_reg_gate__39_n_0;
  wire coeff_tab_s_reg_gate__3_n_0;
  wire coeff_tab_s_reg_gate__40_n_0;
  wire coeff_tab_s_reg_gate__41_n_0;
  wire coeff_tab_s_reg_gate__42_n_0;
  wire coeff_tab_s_reg_gate__43_n_0;
  wire coeff_tab_s_reg_gate__44_n_0;
  wire coeff_tab_s_reg_gate__45_n_0;
  wire coeff_tab_s_reg_gate__46_n_0;
  wire coeff_tab_s_reg_gate__47_n_0;
  wire coeff_tab_s_reg_gate__48_n_0;
  wire coeff_tab_s_reg_gate__49_n_0;
  wire coeff_tab_s_reg_gate__4_n_0;
  wire coeff_tab_s_reg_gate__50_n_0;
  wire coeff_tab_s_reg_gate__51_n_0;
  wire coeff_tab_s_reg_gate__52_n_0;
  wire coeff_tab_s_reg_gate__53_n_0;
  wire coeff_tab_s_reg_gate__54_n_0;
  wire coeff_tab_s_reg_gate__55_n_0;
  wire coeff_tab_s_reg_gate__56_n_0;
  wire coeff_tab_s_reg_gate__57_n_0;
  wire coeff_tab_s_reg_gate__58_n_0;
  wire coeff_tab_s_reg_gate__59_n_0;
  wire coeff_tab_s_reg_gate__5_n_0;
  wire coeff_tab_s_reg_gate__60_n_0;
  wire coeff_tab_s_reg_gate__61_n_0;
  wire coeff_tab_s_reg_gate__62_n_0;
  wire coeff_tab_s_reg_gate__63_n_0;
  wire coeff_tab_s_reg_gate__64_n_0;
  wire coeff_tab_s_reg_gate__65_n_0;
  wire coeff_tab_s_reg_gate__66_n_0;
  wire coeff_tab_s_reg_gate__67_n_0;
  wire coeff_tab_s_reg_gate__68_n_0;
  wire coeff_tab_s_reg_gate__69_n_0;
  wire coeff_tab_s_reg_gate__6_n_0;
  wire coeff_tab_s_reg_gate__70_n_0;
  wire coeff_tab_s_reg_gate__71_n_0;
  wire coeff_tab_s_reg_gate__72_n_0;
  wire coeff_tab_s_reg_gate__73_n_0;
  wire coeff_tab_s_reg_gate__74_n_0;
  wire coeff_tab_s_reg_gate__75_n_0;
  wire coeff_tab_s_reg_gate__76_n_0;
  wire coeff_tab_s_reg_gate__77_n_0;
  wire coeff_tab_s_reg_gate__78_n_0;
  wire coeff_tab_s_reg_gate__79_n_0;
  wire coeff_tab_s_reg_gate__7_n_0;
  wire coeff_tab_s_reg_gate__80_n_0;
  wire coeff_tab_s_reg_gate__81_n_0;
  wire coeff_tab_s_reg_gate__82_n_0;
  wire coeff_tab_s_reg_gate__83_n_0;
  wire coeff_tab_s_reg_gate__84_n_0;
  wire coeff_tab_s_reg_gate__85_n_0;
  wire coeff_tab_s_reg_gate__86_n_0;
  wire coeff_tab_s_reg_gate__87_n_0;
  wire coeff_tab_s_reg_gate__88_n_0;
  wire coeff_tab_s_reg_gate__89_n_0;
  wire coeff_tab_s_reg_gate__8_n_0;
  wire coeff_tab_s_reg_gate__90_n_0;
  wire coeff_tab_s_reg_gate__91_n_0;
  wire coeff_tab_s_reg_gate__92_n_0;
  wire coeff_tab_s_reg_gate__93_n_0;
  wire coeff_tab_s_reg_gate__94_n_0;
  wire coeff_tab_s_reg_gate__9_n_0;
  wire coeff_tab_s_reg_gate_n_0;
  wire \cpt_s_reg_n_0_[0] ;
  wire \cpt_s_reg_n_0_[1] ;
  wire \cpt_s_reg_n_0_[2] ;
  wire \cpt_s_reg_n_0_[3] ;
  wire \cpt_s_reg_n_0_[4] ;
  wire \cpt_s_reg_n_0_[5] ;
  wire \cpt_store_s_reg_n_0_[0] ;
  wire \cpt_store_s_reg_n_0_[1] ;
  wire \cpt_store_s_reg_n_0_[2] ;
  wire data_clk_i;
  wire data_en_i;
  wire data_en_next;
  wire [6:0]data_en_s;
  wire data_en_s_10;
  wire [13:0]data_i;
  wire data_in_en_s;
  wire [18:0]data_o;
  wire data_out_en_s;
  wire data_out_en_s_1;
  wire data_out_en_s_3;
  wire data_out_en_s_5;
  wire data_out_en_s_7;
  wire data_out_en_s_9;
  wire data_rst_i;
  wire [18:0]\data_s[6]_0 ;
  wire end_macc_s__0;
  wire [0:0]end_next_s;
  wire [31:1]end_next_s_0;
  wire end_s;
  wire end_s_12;
  wire end_s_14;
  wire end_s_16;
  wire end_s_18;
  wire end_s_20;
  wire \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire end_s_reg_gate__0_n_0;
  wire end_s_reg_gate__1_n_0;
  wire end_s_reg_gate__2_n_0;
  wire end_s_reg_gate__3_n_0;
  wire end_s_reg_gate__4_n_0;
  wire end_s_reg_gate_n_0;
  wire end_s_reg_r_0_n_0;
  wire end_s_reg_r_1_n_0;
  wire end_s_reg_r_2_n_0;
  wire end_s_reg_r_n_0;
  wire [18:0]final_res_s;
  wire \gen_macc[1].logic_inst_n_10 ;
  wire \gen_macc[1].logic_inst_n_11 ;
  wire \gen_macc[1].logic_inst_n_12 ;
  wire \gen_macc[1].logic_inst_n_13 ;
  wire \gen_macc[1].logic_inst_n_14 ;
  wire \gen_macc[1].logic_inst_n_15 ;
  wire \gen_macc[1].logic_inst_n_16 ;
  wire \gen_macc[1].logic_inst_n_17 ;
  wire \gen_macc[1].logic_inst_n_18 ;
  wire \gen_macc[1].logic_inst_n_19 ;
  wire \gen_macc[1].logic_inst_n_20 ;
  wire \gen_macc[1].logic_inst_n_21 ;
  wire \gen_macc[1].logic_inst_n_3 ;
  wire \gen_macc[1].logic_inst_n_4 ;
  wire \gen_macc[1].logic_inst_n_5 ;
  wire \gen_macc[1].logic_inst_n_6 ;
  wire \gen_macc[1].logic_inst_n_7 ;
  wire \gen_macc[1].logic_inst_n_8 ;
  wire \gen_macc[1].logic_inst_n_9 ;
  wire \gen_macc[2].logic_inst_n_10 ;
  wire \gen_macc[2].logic_inst_n_11 ;
  wire \gen_macc[2].logic_inst_n_12 ;
  wire \gen_macc[2].logic_inst_n_13 ;
  wire \gen_macc[2].logic_inst_n_14 ;
  wire \gen_macc[2].logic_inst_n_15 ;
  wire \gen_macc[2].logic_inst_n_16 ;
  wire \gen_macc[2].logic_inst_n_17 ;
  wire \gen_macc[2].logic_inst_n_18 ;
  wire \gen_macc[2].logic_inst_n_19 ;
  wire \gen_macc[2].logic_inst_n_20 ;
  wire \gen_macc[2].logic_inst_n_21 ;
  wire \gen_macc[2].logic_inst_n_3 ;
  wire \gen_macc[2].logic_inst_n_4 ;
  wire \gen_macc[2].logic_inst_n_5 ;
  wire \gen_macc[2].logic_inst_n_6 ;
  wire \gen_macc[2].logic_inst_n_7 ;
  wire \gen_macc[2].logic_inst_n_8 ;
  wire \gen_macc[2].logic_inst_n_9 ;
  wire \gen_macc[4].logic_inst_n_10 ;
  wire \gen_macc[4].logic_inst_n_11 ;
  wire \gen_macc[4].logic_inst_n_12 ;
  wire \gen_macc[4].logic_inst_n_13 ;
  wire \gen_macc[4].logic_inst_n_14 ;
  wire \gen_macc[4].logic_inst_n_15 ;
  wire \gen_macc[4].logic_inst_n_16 ;
  wire \gen_macc[4].logic_inst_n_17 ;
  wire \gen_macc[4].logic_inst_n_18 ;
  wire \gen_macc[4].logic_inst_n_19 ;
  wire \gen_macc[4].logic_inst_n_2 ;
  wire \gen_macc[4].logic_inst_n_20 ;
  wire \gen_macc[4].logic_inst_n_21 ;
  wire \gen_macc[4].logic_inst_n_22 ;
  wire \gen_macc[4].logic_inst_n_23 ;
  wire \gen_macc[4].logic_inst_n_24 ;
  wire \gen_macc[4].logic_inst_n_3 ;
  wire \gen_macc[4].logic_inst_n_4 ;
  wire \gen_macc[4].logic_inst_n_6 ;
  wire \gen_macc[4].logic_inst_n_7 ;
  wire \gen_macc[4].logic_inst_n_8 ;
  wire \gen_macc[4].logic_inst_n_9 ;
  wire \gen_macc[5].logic_inst_n_10 ;
  wire \gen_macc[5].logic_inst_n_11 ;
  wire \gen_macc[5].logic_inst_n_12 ;
  wire \gen_macc[5].logic_inst_n_13 ;
  wire \gen_macc[5].logic_inst_n_14 ;
  wire \gen_macc[5].logic_inst_n_15 ;
  wire \gen_macc[5].logic_inst_n_16 ;
  wire \gen_macc[5].logic_inst_n_17 ;
  wire \gen_macc[5].logic_inst_n_18 ;
  wire \gen_macc[5].logic_inst_n_19 ;
  wire \gen_macc[5].logic_inst_n_20 ;
  wire \gen_macc[5].logic_inst_n_21 ;
  wire \gen_macc[5].logic_inst_n_3 ;
  wire \gen_macc[5].logic_inst_n_4 ;
  wire \gen_macc[5].logic_inst_n_5 ;
  wire \gen_macc[5].logic_inst_n_6 ;
  wire \gen_macc[5].logic_inst_n_7 ;
  wire \gen_macc[5].logic_inst_n_8 ;
  wire \gen_macc[5].logic_inst_n_9 ;
  wire \gen_macc[6].logic_inst_n_10 ;
  wire \gen_macc[6].logic_inst_n_11 ;
  wire \gen_macc[6].logic_inst_n_12 ;
  wire \gen_macc[6].logic_inst_n_13 ;
  wire \gen_macc[6].logic_inst_n_14 ;
  wire \gen_macc[6].logic_inst_n_15 ;
  wire \gen_macc[6].logic_inst_n_16 ;
  wire \gen_macc[6].logic_inst_n_17 ;
  wire \gen_macc[6].logic_inst_n_18 ;
  wire \gen_macc[6].logic_inst_n_19 ;
  wire \gen_macc[6].logic_inst_n_20 ;
  wire \gen_macc[6].logic_inst_n_21 ;
  wire \gen_macc[6].logic_inst_n_3 ;
  wire \gen_macc[6].logic_inst_n_4 ;
  wire \gen_macc[6].logic_inst_n_5 ;
  wire \gen_macc[6].logic_inst_n_6 ;
  wire \gen_macc[6].logic_inst_n_7 ;
  wire \gen_macc[6].logic_inst_n_8 ;
  wire \gen_macc[6].logic_inst_n_9 ;
  wire [4:0]mem_reg;
  wire [15:0]mem_reg_0;
  wire [5:0]mux_cpt_s;
  wire [0:0]ready_next_s;
  wire [31:1]ready_next_s_1;
  wire ready_s;
  wire ready_s_11;
  wire ready_s_13;
  wire ready_s_15;
  wire ready_s_17;
  wire ready_s_19;
  wire \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ;
  wire \ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ;
  wire \ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ;
  wire ready_s_reg_gate__0_n_0;
  wire ready_s_reg_gate__1_n_0;
  wire ready_s_reg_gate__2_n_0;
  wire ready_s_reg_gate__3_n_0;
  wire ready_s_reg_gate__4_n_0;
  wire ready_s_reg_gate__5_n_0;
  wire ready_s_reg_gate_n_0;
  wire res_s0;
  wire res_s0_0;
  wire res_s0_2;
  wire res_s0_4;
  wire res_s0_6;
  wire res_s0_8;
  wire s00_axi_aclk;
  wire tick_o;

  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [0]),
        .Q(\coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [10]),
        .Q(\coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [11]),
        .Q(\coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [12]),
        .Q(\coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [13]),
        .Q(\coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [14]),
        .Q(\coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [15]),
        .Q(\coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [1]),
        .Q(\coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [2]),
        .Q(\coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [3]),
        .Q(\coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [4]),
        .Q(\coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [5]),
        .Q(\coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [6]),
        .Q(\coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [7]),
        .Q(\coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [8]),
        .Q(\coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[11] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[8] [9]),
        .Q(\coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[13][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__46_n_0),
        .Q(\coeff_tab_s_reg[13] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__36_n_0),
        .Q(\coeff_tab_s_reg[13] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__35_n_0),
        .Q(\coeff_tab_s_reg[13] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__34_n_0),
        .Q(\coeff_tab_s_reg[13] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__33_n_0),
        .Q(\coeff_tab_s_reg[13] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__32_n_0),
        .Q(\coeff_tab_s_reg[13] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__31_n_0),
        .Q(\coeff_tab_s_reg[13] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__45_n_0),
        .Q(\coeff_tab_s_reg[13] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__44_n_0),
        .Q(\coeff_tab_s_reg[13] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__43_n_0),
        .Q(\coeff_tab_s_reg[13] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__42_n_0),
        .Q(\coeff_tab_s_reg[13] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__41_n_0),
        .Q(\coeff_tab_s_reg[13] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__40_n_0),
        .Q(\coeff_tab_s_reg[13] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__39_n_0),
        .Q(\coeff_tab_s_reg[13] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__38_n_0),
        .Q(\coeff_tab_s_reg[13] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[13][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__37_n_0),
        .Q(\coeff_tab_s_reg[13] [9]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [0]),
        .Q(\coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [10]),
        .Q(\coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [11]),
        .Q(\coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [12]),
        .Q(\coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [13]),
        .Q(\coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [14]),
        .Q(\coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [15]),
        .Q(\coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [1]),
        .Q(\coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [2]),
        .Q(\coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [3]),
        .Q(\coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [4]),
        .Q(\coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [5]),
        .Q(\coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [6]),
        .Q(\coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [7]),
        .Q(\coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [8]),
        .Q(\coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[16] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[13] [9]),
        .Q(\coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[18][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__62_n_0),
        .Q(\coeff_tab_s_reg[18] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__52_n_0),
        .Q(\coeff_tab_s_reg[18] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__51_n_0),
        .Q(\coeff_tab_s_reg[18] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__50_n_0),
        .Q(\coeff_tab_s_reg[18] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__49_n_0),
        .Q(\coeff_tab_s_reg[18] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__48_n_0),
        .Q(\coeff_tab_s_reg[18] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__47_n_0),
        .Q(\coeff_tab_s_reg[18] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__61_n_0),
        .Q(\coeff_tab_s_reg[18] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__60_n_0),
        .Q(\coeff_tab_s_reg[18] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__59_n_0),
        .Q(\coeff_tab_s_reg[18] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__58_n_0),
        .Q(\coeff_tab_s_reg[18] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__57_n_0),
        .Q(\coeff_tab_s_reg[18] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__56_n_0),
        .Q(\coeff_tab_s_reg[18] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__55_n_0),
        .Q(\coeff_tab_s_reg[18] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__54_n_0),
        .Q(\coeff_tab_s_reg[18] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[18][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__53_n_0),
        .Q(\coeff_tab_s_reg[18] [9]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[0]),
        .Q(\coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[10]),
        .Q(\coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[11]),
        .Q(\coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[12]),
        .Q(\coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[13]),
        .Q(\coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[14]),
        .Q(\coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[15]),
        .Q(\coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[1]),
        .Q(\coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[2]),
        .Q(\coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[3]),
        .Q(\coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[4]),
        .Q(\coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[5]),
        .Q(\coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[6]),
        .Q(\coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[7]),
        .Q(\coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[8]),
        .Q(\coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[1] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(coeff_s[9]),
        .Q(\coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [0]),
        .Q(\coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [10]),
        .Q(\coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [11]),
        .Q(\coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [12]),
        .Q(\coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [13]),
        .Q(\coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [14]),
        .Q(\coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [15]),
        .Q(\coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [1]),
        .Q(\coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [2]),
        .Q(\coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [3]),
        .Q(\coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [4]),
        .Q(\coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [5]),
        .Q(\coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [6]),
        .Q(\coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [7]),
        .Q(\coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [8]),
        .Q(\coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[21] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[18] [9]),
        .Q(\coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[23][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__78_n_0),
        .Q(\coeff_tab_s_reg[23] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__68_n_0),
        .Q(\coeff_tab_s_reg[23] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__67_n_0),
        .Q(\coeff_tab_s_reg[23] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__66_n_0),
        .Q(\coeff_tab_s_reg[23] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__65_n_0),
        .Q(\coeff_tab_s_reg[23] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__64_n_0),
        .Q(\coeff_tab_s_reg[23] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__63_n_0),
        .Q(\coeff_tab_s_reg[23] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__77_n_0),
        .Q(\coeff_tab_s_reg[23] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__76_n_0),
        .Q(\coeff_tab_s_reg[23] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__75_n_0),
        .Q(\coeff_tab_s_reg[23] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__74_n_0),
        .Q(\coeff_tab_s_reg[23] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__73_n_0),
        .Q(\coeff_tab_s_reg[23] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__72_n_0),
        .Q(\coeff_tab_s_reg[23] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__71_n_0),
        .Q(\coeff_tab_s_reg[23] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__70_n_0),
        .Q(\coeff_tab_s_reg[23] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[23][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__69_n_0),
        .Q(\coeff_tab_s_reg[23] [9]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [0]),
        .Q(\coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [10]),
        .Q(\coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [11]),
        .Q(\coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [12]),
        .Q(\coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [13]),
        .Q(\coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [14]),
        .Q(\coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [15]),
        .Q(\coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [1]),
        .Q(\coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [2]),
        .Q(\coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [3]),
        .Q(\coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [4]),
        .Q(\coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [5]),
        .Q(\coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [6]),
        .Q(\coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [7]),
        .Q(\coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [8]),
        .Q(\coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[26] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[23] [9]),
        .Q(\coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[28][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__94_n_0),
        .Q(\coeff_tab_s_reg[28] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__84_n_0),
        .Q(\coeff_tab_s_reg[28] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__83_n_0),
        .Q(\coeff_tab_s_reg[28] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__82_n_0),
        .Q(\coeff_tab_s_reg[28] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__81_n_0),
        .Q(\coeff_tab_s_reg[28] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__80_n_0),
        .Q(\coeff_tab_s_reg[28] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__79_n_0),
        .Q(\coeff_tab_s_reg[28] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__93_n_0),
        .Q(\coeff_tab_s_reg[28] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__92_n_0),
        .Q(\coeff_tab_s_reg[28] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__91_n_0),
        .Q(\coeff_tab_s_reg[28] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__90_n_0),
        .Q(\coeff_tab_s_reg[28] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__89_n_0),
        .Q(\coeff_tab_s_reg[28] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__88_n_0),
        .Q(\coeff_tab_s_reg[28] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__87_n_0),
        .Q(\coeff_tab_s_reg[28] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__86_n_0),
        .Q(\coeff_tab_s_reg[28] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[28][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__85_n_0),
        .Q(\coeff_tab_s_reg[28] [9]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[3][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__14_n_0),
        .Q(\coeff_tab_s_reg[3] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__4_n_0),
        .Q(\coeff_tab_s_reg[3] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__3_n_0),
        .Q(\coeff_tab_s_reg[3] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__2_n_0),
        .Q(\coeff_tab_s_reg[3] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__1_n_0),
        .Q(\coeff_tab_s_reg[3] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__0_n_0),
        .Q(\coeff_tab_s_reg[3] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate_n_0),
        .Q(\coeff_tab_s_reg[3] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__13_n_0),
        .Q(\coeff_tab_s_reg[3] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__12_n_0),
        .Q(\coeff_tab_s_reg[3] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__11_n_0),
        .Q(\coeff_tab_s_reg[3] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__10_n_0),
        .Q(\coeff_tab_s_reg[3] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__9_n_0),
        .Q(\coeff_tab_s_reg[3] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__8_n_0),
        .Q(\coeff_tab_s_reg[3] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__7_n_0),
        .Q(\coeff_tab_s_reg[3] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__6_n_0),
        .Q(\coeff_tab_s_reg[3] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[3][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__5_n_0),
        .Q(\coeff_tab_s_reg[3] [9]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [0]),
        .Q(\coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [10]),
        .Q(\coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [11]),
        .Q(\coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [12]),
        .Q(\coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [13]),
        .Q(\coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [14]),
        .Q(\coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [15]),
        .Q(\coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [1]),
        .Q(\coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [2]),
        .Q(\coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [3]),
        .Q(\coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [4]),
        .Q(\coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [5]),
        .Q(\coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [6]),
        .Q(\coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [7]),
        .Q(\coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [8]),
        .Q(\coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  (* srl_bus_name = "\U0/fir_top_inst/coeff_tab_s_reg[6] " *) 
  (* srl_name = "\U0/fir_top_inst/coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(\coeff_tab_s_reg[3] [9]),
        .Q(\coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \coeff_tab_s_reg[8][0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__30_n_0),
        .Q(\coeff_tab_s_reg[8] [0]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__20_n_0),
        .Q(\coeff_tab_s_reg[8] [10]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][11] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__19_n_0),
        .Q(\coeff_tab_s_reg[8] [11]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][12] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__18_n_0),
        .Q(\coeff_tab_s_reg[8] [12]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][13] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__17_n_0),
        .Q(\coeff_tab_s_reg[8] [13]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][14] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__16_n_0),
        .Q(\coeff_tab_s_reg[8] [14]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__15_n_0),
        .Q(\coeff_tab_s_reg[8] [15]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][1] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__29_n_0),
        .Q(\coeff_tab_s_reg[8] [1]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][2] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__28_n_0),
        .Q(\coeff_tab_s_reg[8] [2]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][3] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__27_n_0),
        .Q(\coeff_tab_s_reg[8] [3]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][4] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__26_n_0),
        .Q(\coeff_tab_s_reg[8] [4]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__25_n_0),
        .Q(\coeff_tab_s_reg[8] [5]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][6] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__24_n_0),
        .Q(\coeff_tab_s_reg[8] [6]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][7] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__23_n_0),
        .Q(\coeff_tab_s_reg[8] [7]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][8] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__22_n_0),
        .Q(\coeff_tab_s_reg[8] [8]),
        .R(data_rst_i));
  FDRE \coeff_tab_s_reg[8][9] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(coeff_tab_s_reg_gate__21_n_0),
        .Q(\coeff_tab_s_reg[8] [9]),
        .R(data_rst_i));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate
       (.I0(\coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__0
       (.I0(\coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__1
       (.I0(\coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__10
       (.I0(\coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__11
       (.I0(\coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__12
       (.I0(\coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__13
       (.I0(\coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__14
       (.I0(\coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__15
       (.I0(\coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__16
       (.I0(\coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__17
       (.I0(\coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__17_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__18
       (.I0(\coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__18_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__19
       (.I0(\coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__19_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__2
       (.I0(\coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__20
       (.I0(\coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__20_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__21
       (.I0(\coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__21_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__22
       (.I0(\coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__22_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__23
       (.I0(\coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__23_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__24
       (.I0(\coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__24_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__25
       (.I0(\coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__25_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__26
       (.I0(\coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__26_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__27
       (.I0(\coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__27_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__28
       (.I0(\coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__28_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__29
       (.I0(\coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__29_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__3
       (.I0(\coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__30
       (.I0(\coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__30_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__31
       (.I0(\coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__31_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__32
       (.I0(\coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__32_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__33
       (.I0(\coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__33_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__34
       (.I0(\coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__34_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__35
       (.I0(\coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__35_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__36
       (.I0(\coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__36_n_0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__37
       (.I0(\coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__37_n_0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__38
       (.I0(\coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__38_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__39
       (.I0(\coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__39_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__4
       (.I0(\coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__40
       (.I0(\coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__40_n_0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__41
       (.I0(\coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__41_n_0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__42
       (.I0(\coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__42_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__43
       (.I0(\coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__43_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__44
       (.I0(\coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__44_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__45
       (.I0(\coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__45_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__46
       (.I0(\coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__46_n_0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__47
       (.I0(\coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__47_n_0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__48
       (.I0(\coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__48_n_0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__49
       (.I0(\coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__49_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__5
       (.I0(\coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__50
       (.I0(\coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__50_n_0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__51
       (.I0(\coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__51_n_0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__52
       (.I0(\coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__52_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__53
       (.I0(\coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__53_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__54
       (.I0(\coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__54_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__55
       (.I0(\coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__55_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__56
       (.I0(\coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__56_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__57
       (.I0(\coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__57_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__58
       (.I0(\coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__58_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__59
       (.I0(\coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__59_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__6
       (.I0(\coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__60
       (.I0(\coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__60_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__61
       (.I0(\coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__61_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__62
       (.I0(\coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__62_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__63
       (.I0(\coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__63_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__64
       (.I0(\coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__64_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__65
       (.I0(\coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__65_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__66
       (.I0(\coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__66_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__67
       (.I0(\coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__67_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__68
       (.I0(\coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__68_n_0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__69
       (.I0(\coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__69_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__7
       (.I0(\coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__70
       (.I0(\coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__70_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__71
       (.I0(\coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__71_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__72
       (.I0(\coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__72_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__73
       (.I0(\coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__73_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__74
       (.I0(\coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__74_n_0));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__75
       (.I0(\coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__75_n_0));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__76
       (.I0(\coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__76_n_0));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__77
       (.I0(\coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__77_n_0));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__78
       (.I0(\coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__78_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__79
       (.I0(\coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__79_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__8
       (.I0(\coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__80
       (.I0(\coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__80_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__81
       (.I0(\coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__81_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__82
       (.I0(\coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__82_n_0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__83
       (.I0(\coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__83_n_0));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__84
       (.I0(\coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__84_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__85
       (.I0(\coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__85_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__86
       (.I0(\coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__86_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__87
       (.I0(\coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__87_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__88
       (.I0(\coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__88_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__89
       (.I0(\coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__89_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__9
       (.I0(\coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(coeff_tab_s_reg_gate__9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__90
       (.I0(\coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__90_n_0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__91
       (.I0(\coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__91_n_0));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__92
       (.I0(\coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__92_n_0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__93
       (.I0(\coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__93_n_0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    coeff_tab_s_reg_gate__94
       (.I0(\coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(coeff_tab_s_reg_gate__94_n_0));
  LUT6 #(
    .INIT(64'h0F0E0F0F0F0F0F0F)) 
    \cpt_s[0]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[4] ),
        .I2(\cpt_s_reg_n_0_[0] ),
        .I3(\cpt_s_reg_n_0_[3] ),
        .I4(\cpt_s_reg_n_0_[5] ),
        .I5(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[0]));
  LUT6 #(
    .INIT(64'h0F0E0F0FF0F0F0F0)) 
    \cpt_s[1]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[4] ),
        .I2(\cpt_s_reg_n_0_[0] ),
        .I3(\cpt_s_reg_n_0_[3] ),
        .I4(\cpt_s_reg_n_0_[5] ),
        .I5(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[1]));
  LUT3 #(
    .INIT(8'h6A)) 
    \cpt_s[2]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[0] ),
        .I2(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h78F0)) 
    \cpt_s[3]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[0] ),
        .I2(\cpt_s_reg_n_0_[3] ),
        .I3(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[3]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h6CCCCCCC)) 
    \cpt_s[4]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[4] ),
        .I2(\cpt_s_reg_n_0_[0] ),
        .I3(\cpt_s_reg_n_0_[3] ),
        .I4(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[4]));
  LUT6 #(
    .INIT(64'h7FFE8000FFFF0000)) 
    \cpt_s[5]_i_1 
       (.I0(\cpt_s_reg_n_0_[2] ),
        .I1(\cpt_s_reg_n_0_[4] ),
        .I2(\cpt_s_reg_n_0_[0] ),
        .I3(\cpt_s_reg_n_0_[3] ),
        .I4(\cpt_s_reg_n_0_[5] ),
        .I5(\cpt_s_reg_n_0_[1] ),
        .O(mux_cpt_s[5]));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[0]),
        .Q(\cpt_s_reg_n_0_[0] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[1] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[1]),
        .Q(\cpt_s_reg_n_0_[1] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[2] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[2]),
        .Q(\cpt_s_reg_n_0_[2] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[3] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[3]),
        .Q(\cpt_s_reg_n_0_[3] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[4] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[4]),
        .Q(\cpt_s_reg_n_0_[4] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_en_i),
        .D(mux_cpt_s[5]),
        .Q(\cpt_s_reg_n_0_[5] ),
        .R(data_rst_i));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_store_s_reg[0] 
       (.C(data_clk_i),
        .CE(1'b1),
        .D(\gen_macc[4].logic_inst_n_2 ),
        .Q(\cpt_store_s_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_store_s_reg[1] 
       (.C(data_clk_i),
        .CE(1'b1),
        .D(\gen_macc[4].logic_inst_n_3 ),
        .Q(\cpt_store_s_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cpt_store_s_reg[2] 
       (.C(data_clk_i),
        .CE(1'b1),
        .D(\gen_macc[4].logic_inst_n_4 ),
        .Q(\cpt_store_s_reg_n_0_[2] ),
        .R(1'b0));
  FDRE data_en_o_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_en_next),
        .Q(tick_o),
        .R(data_rst_i));
  FDRE data_in_en_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(data_en_i),
        .Q(data_in_en_s),
        .R(data_rst_i));
  FDRE \data_out_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [0]),
        .Q(data_o[0]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[10] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [10]),
        .Q(data_o[10]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[11] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [11]),
        .Q(data_o[11]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[12] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [12]),
        .Q(data_o[12]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[13] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [13]),
        .Q(data_o[13]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[14] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [14]),
        .Q(data_o[14]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[15] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [15]),
        .Q(data_o[15]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[16] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [16]),
        .Q(data_o[16]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[17] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [17]),
        .Q(data_o[17]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[18] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [18]),
        .Q(data_o[18]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[1] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [1]),
        .Q(data_o[1]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[2] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [2]),
        .Q(data_o[2]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[3] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [3]),
        .Q(data_o[3]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[4] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [4]),
        .Q(data_o[4]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [5]),
        .Q(data_o[5]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[6] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [6]),
        .Q(data_o[6]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[7] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [7]),
        .Q(data_o[7]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[8] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [8]),
        .Q(data_o[8]),
        .R(data_rst_i));
  FDRE \data_out_s_reg[9] 
       (.C(data_clk_i),
        .CE(data_en_next),
        .D(\data_s[6]_0 [9]),
        .Q(data_o[9]),
        .R(data_rst_i));
  FDRE end_delay_macc_s_reg
       (.C(data_clk_i),
        .CE(1'b1),
        .D(end_macc_s__0),
        .Q(end_next_s),
        .R(data_rst_i));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    end_macc_s
       (.I0(\cpt_s_reg_n_0_[5] ),
        .I1(\cpt_s_reg_n_0_[3] ),
        .I2(\cpt_s_reg_n_0_[4] ),
        .I3(\cpt_s_reg_n_0_[2] ),
        .I4(\cpt_s_reg_n_0_[0] ),
        .I5(\cpt_s_reg_n_0_[1] ),
        .O(end_macc_s__0));
  FDRE \end_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_next_s),
        .Q(end_next_s_0[1]),
        .R(data_rst_i));
  FDRE \end_s_reg[10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate__3_n_0),
        .Q(end_next_s_0[11]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[11]),
        .Q(\end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \end_s_reg[15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate__2_n_0),
        .Q(end_next_s_0[16]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[16]),
        .Q(\end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \end_s_reg[20] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate__1_n_0),
        .Q(end_next_s_0[21]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[21]),
        .Q(\end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \end_s_reg[25] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate__0_n_0),
        .Q(end_next_s_0[26]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[26]),
        .Q(\end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \end_s_reg[30] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate_n_0),
        .Q(end_next_s_0[31]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[1]),
        .Q(\end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \end_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_gate__4_n_0),
        .Q(end_next_s_0[6]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/end_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(end_next_s_0[6]),
        .Q(\end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate
       (.I0(\end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate__0
       (.I0(\end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate__1
       (.I0(\end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate__2
       (.I0(\end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate__3
       (.I0(\end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h8)) 
    end_s_reg_gate__4
       (.I0(\end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(end_s_reg_gate__4_n_0));
  FDRE end_s_reg_r
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(1'b1),
        .Q(end_s_reg_r_n_0),
        .R(data_rst_i));
  FDRE end_s_reg_r_0
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_r_n_0),
        .Q(end_s_reg_r_0_n_0),
        .R(data_rst_i));
  FDRE end_s_reg_r_1
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_r_0_n_0),
        .Q(end_s_reg_r_1_n_0),
        .R(data_rst_i));
  FDRE end_s_reg_r_2
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(end_s_reg_r_1_n_0),
        .Q(end_s_reg_r_2_n_0),
        .R(data_rst_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc \gen_macc[0].logic_inst 
       (.DOBDO(coeff_s),
        .E(data_out_en_s_9),
        .Q(final_res_s),
        .data_clk_i(data_clk_i),
        .data_en_s(data_en_s[0]),
        .data_en_s_0(data_en_s_10),
        .data_en_s_reg_0(data_out_en_s_7),
        .data_en_s_reg_1(data_out_en_s_5),
        .data_en_s_reg_2(data_out_en_s_3),
        .data_en_s_reg_3(data_out_en_s_1),
        .data_en_s_reg_4(data_out_en_s),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[1]),
        .end_s(end_s),
        .end_s_10(end_s_16),
        .end_s_12(end_s_18),
        .end_s_14(end_s_20),
        .end_s_6(end_s_12),
        .end_s_8(end_s_14),
        .ready_next_s_1(ready_next_s_1[1]),
        .ready_s(ready_s),
        .ready_s_11(ready_s_15),
        .ready_s_13(ready_s_17),
        .ready_s_15(ready_s_19),
        .ready_s_7(ready_s_11),
        .ready_s_9(ready_s_13),
        .res_s0(res_s0_8),
        .res_s0_1(res_s0_6),
        .res_s0_2(res_s0_4),
        .res_s0_3(res_s0_2),
        .res_s0_4(res_s0_0),
        .res_s0_5(res_s0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0 \gen_macc[1].logic_inst 
       (.A(\coeff_tab_s_reg[3] ),
        .E(data_out_en_s_9),
        .Q({\gen_macc[1].logic_inst_n_3 ,\gen_macc[1].logic_inst_n_4 ,\gen_macc[1].logic_inst_n_5 ,\gen_macc[1].logic_inst_n_6 ,\gen_macc[1].logic_inst_n_7 ,\gen_macc[1].logic_inst_n_8 ,\gen_macc[1].logic_inst_n_9 ,\gen_macc[1].logic_inst_n_10 ,\gen_macc[1].logic_inst_n_11 ,\gen_macc[1].logic_inst_n_12 ,\gen_macc[1].logic_inst_n_13 ,\gen_macc[1].logic_inst_n_14 ,\gen_macc[1].logic_inst_n_15 ,\gen_macc[1].logic_inst_n_16 ,\gen_macc[1].logic_inst_n_17 ,\gen_macc[1].logic_inst_n_18 ,\gen_macc[1].logic_inst_n_19 ,\gen_macc[1].logic_inst_n_20 ,\gen_macc[1].logic_inst_n_21 }),
        .data_clk_i(data_clk_i),
        .data_en_s(data_en_s[1]),
        .data_en_s_0(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[6]),
        .end_s(end_s),
        .ready_next_s_1(ready_next_s_1[6]),
        .ready_s(ready_s),
        .res_s0(res_s0_8));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1 \gen_macc[2].logic_inst 
       (.A(\coeff_tab_s_reg[8] ),
        .E(data_out_en_s_7),
        .Q({\gen_macc[2].logic_inst_n_3 ,\gen_macc[2].logic_inst_n_4 ,\gen_macc[2].logic_inst_n_5 ,\gen_macc[2].logic_inst_n_6 ,\gen_macc[2].logic_inst_n_7 ,\gen_macc[2].logic_inst_n_8 ,\gen_macc[2].logic_inst_n_9 ,\gen_macc[2].logic_inst_n_10 ,\gen_macc[2].logic_inst_n_11 ,\gen_macc[2].logic_inst_n_12 ,\gen_macc[2].logic_inst_n_13 ,\gen_macc[2].logic_inst_n_14 ,\gen_macc[2].logic_inst_n_15 ,\gen_macc[2].logic_inst_n_16 ,\gen_macc[2].logic_inst_n_17 ,\gen_macc[2].logic_inst_n_18 ,\gen_macc[2].logic_inst_n_19 ,\gen_macc[2].logic_inst_n_20 ,\gen_macc[2].logic_inst_n_21 }),
        .data_clk_i(data_clk_i),
        .data_en_s(data_en_s[2]),
        .data_en_s_0(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[11]),
        .end_s(end_s_12),
        .ready_next_s_1(ready_next_s_1[11]),
        .ready_s(ready_s_11),
        .res_s0(res_s0_6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2 \gen_macc[3].logic_inst 
       (.A(\coeff_tab_s_reg[13] ),
        .D(\data_s[6]_0 ),
        .E(data_out_en_s_5),
        .Q({\gen_macc[2].logic_inst_n_3 ,\gen_macc[2].logic_inst_n_4 ,\gen_macc[2].logic_inst_n_5 ,\gen_macc[2].logic_inst_n_6 ,\gen_macc[2].logic_inst_n_7 ,\gen_macc[2].logic_inst_n_8 ,\gen_macc[2].logic_inst_n_9 ,\gen_macc[2].logic_inst_n_10 ,\gen_macc[2].logic_inst_n_11 ,\gen_macc[2].logic_inst_n_12 ,\gen_macc[2].logic_inst_n_13 ,\gen_macc[2].logic_inst_n_14 ,\gen_macc[2].logic_inst_n_15 ,\gen_macc[2].logic_inst_n_16 ,\gen_macc[2].logic_inst_n_17 ,\gen_macc[2].logic_inst_n_18 ,\gen_macc[2].logic_inst_n_19 ,\gen_macc[2].logic_inst_n_20 ,\gen_macc[2].logic_inst_n_21 }),
        .data_clk_i(data_clk_i),
        .data_en_s(data_en_s[3]),
        .data_en_s_0(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .\data_out_s_reg[0] (\gen_macc[6].logic_inst_n_21 ),
        .\data_out_s_reg[10] (\gen_macc[6].logic_inst_n_11 ),
        .\data_out_s_reg[11] (\gen_macc[6].logic_inst_n_10 ),
        .\data_out_s_reg[12] (\gen_macc[6].logic_inst_n_9 ),
        .\data_out_s_reg[13] (\gen_macc[6].logic_inst_n_8 ),
        .\data_out_s_reg[14] (\gen_macc[6].logic_inst_n_7 ),
        .\data_out_s_reg[15] (\gen_macc[6].logic_inst_n_6 ),
        .\data_out_s_reg[16] (\gen_macc[6].logic_inst_n_5 ),
        .\data_out_s_reg[17] (\gen_macc[6].logic_inst_n_4 ),
        .\data_out_s_reg[18] (\cpt_store_s_reg_n_0_[2] ),
        .\data_out_s_reg[18]_0 (\gen_macc[6].logic_inst_n_3 ),
        .\data_out_s_reg[18]_1 (\cpt_store_s_reg_n_0_[1] ),
        .\data_out_s_reg[18]_2 ({\gen_macc[1].logic_inst_n_3 ,\gen_macc[1].logic_inst_n_4 ,\gen_macc[1].logic_inst_n_5 ,\gen_macc[1].logic_inst_n_6 ,\gen_macc[1].logic_inst_n_7 ,\gen_macc[1].logic_inst_n_8 ,\gen_macc[1].logic_inst_n_9 ,\gen_macc[1].logic_inst_n_10 ,\gen_macc[1].logic_inst_n_11 ,\gen_macc[1].logic_inst_n_12 ,\gen_macc[1].logic_inst_n_13 ,\gen_macc[1].logic_inst_n_14 ,\gen_macc[1].logic_inst_n_15 ,\gen_macc[1].logic_inst_n_16 ,\gen_macc[1].logic_inst_n_17 ,\gen_macc[1].logic_inst_n_18 ,\gen_macc[1].logic_inst_n_19 ,\gen_macc[1].logic_inst_n_20 ,\gen_macc[1].logic_inst_n_21 }),
        .\data_out_s_reg[18]_3 (\cpt_store_s_reg_n_0_[0] ),
        .\data_out_s_reg[18]_4 (final_res_s),
        .\data_out_s_reg[1] (\gen_macc[6].logic_inst_n_20 ),
        .\data_out_s_reg[2] (\gen_macc[6].logic_inst_n_19 ),
        .\data_out_s_reg[3] (\gen_macc[6].logic_inst_n_18 ),
        .\data_out_s_reg[4] (\gen_macc[6].logic_inst_n_17 ),
        .\data_out_s_reg[5] (\gen_macc[6].logic_inst_n_16 ),
        .\data_out_s_reg[6] (\gen_macc[6].logic_inst_n_15 ),
        .\data_out_s_reg[7] (\gen_macc[6].logic_inst_n_14 ),
        .\data_out_s_reg[8] (\gen_macc[6].logic_inst_n_13 ),
        .\data_out_s_reg[9] (\gen_macc[6].logic_inst_n_12 ),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[16]),
        .end_s(end_s_14),
        .ready_next_s_1(ready_next_s_1[16]),
        .ready_s(ready_s_13),
        .res_s0(res_s0_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3 \gen_macc[4].logic_inst 
       (.A(\coeff_tab_s_reg[18] ),
        .E(data_out_en_s_3),
        .Q({\gen_macc[4].logic_inst_n_6 ,\gen_macc[4].logic_inst_n_7 ,\gen_macc[4].logic_inst_n_8 ,\gen_macc[4].logic_inst_n_9 ,\gen_macc[4].logic_inst_n_10 ,\gen_macc[4].logic_inst_n_11 ,\gen_macc[4].logic_inst_n_12 ,\gen_macc[4].logic_inst_n_13 ,\gen_macc[4].logic_inst_n_14 ,\gen_macc[4].logic_inst_n_15 ,\gen_macc[4].logic_inst_n_16 ,\gen_macc[4].logic_inst_n_17 ,\gen_macc[4].logic_inst_n_18 ,\gen_macc[4].logic_inst_n_19 ,\gen_macc[4].logic_inst_n_20 ,\gen_macc[4].logic_inst_n_21 ,\gen_macc[4].logic_inst_n_22 ,\gen_macc[4].logic_inst_n_23 ,\gen_macc[4].logic_inst_n_24 }),
        .\cpt_store_s_reg[0] (\cpt_store_s_reg_n_0_[1] ),
        .\cpt_store_s_reg[0]_0 (\cpt_store_s_reg_n_0_[0] ),
        .\cpt_store_s_reg[0]_1 (\cpt_store_s_reg_n_0_[2] ),
        .\cpt_store_s_reg[2] ({data_en_s[6:5],data_en_s[3:0]}),
        .data_clk_i(data_clk_i),
        .data_en_o_reg_0(\gen_macc[4].logic_inst_n_2 ),
        .data_en_o_reg_1(\gen_macc[4].logic_inst_n_3 ),
        .data_en_o_reg_2(\gen_macc[4].logic_inst_n_4 ),
        .data_en_o_reg_3(data_en_next),
        .data_en_s(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[21]),
        .end_s(end_s_16),
        .ready_next_s_1(ready_next_s_1[21]),
        .ready_s(ready_s_15),
        .res_s0(res_s0_2));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4 \gen_macc[5].logic_inst 
       (.A(\coeff_tab_s_reg[23] ),
        .E(data_out_en_s_1),
        .Q({\gen_macc[5].logic_inst_n_3 ,\gen_macc[5].logic_inst_n_4 ,\gen_macc[5].logic_inst_n_5 ,\gen_macc[5].logic_inst_n_6 ,\gen_macc[5].logic_inst_n_7 ,\gen_macc[5].logic_inst_n_8 ,\gen_macc[5].logic_inst_n_9 ,\gen_macc[5].logic_inst_n_10 ,\gen_macc[5].logic_inst_n_11 ,\gen_macc[5].logic_inst_n_12 ,\gen_macc[5].logic_inst_n_13 ,\gen_macc[5].logic_inst_n_14 ,\gen_macc[5].logic_inst_n_15 ,\gen_macc[5].logic_inst_n_16 ,\gen_macc[5].logic_inst_n_17 ,\gen_macc[5].logic_inst_n_18 ,\gen_macc[5].logic_inst_n_19 ,\gen_macc[5].logic_inst_n_20 ,\gen_macc[5].logic_inst_n_21 }),
        .data_clk_i(data_clk_i),
        .data_en_o_reg_0(data_en_s[5]),
        .data_en_s(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[26]),
        .end_s(end_s_18),
        .ready_next_s_1(ready_next_s_1[26]),
        .ready_s(ready_s_17),
        .res_s0(res_s0_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5 \gen_macc[6].logic_inst 
       (.A(\coeff_tab_s_reg[28] ),
        .E(data_out_en_s),
        .Q({\gen_macc[5].logic_inst_n_3 ,\gen_macc[5].logic_inst_n_4 ,\gen_macc[5].logic_inst_n_5 ,\gen_macc[5].logic_inst_n_6 ,\gen_macc[5].logic_inst_n_7 ,\gen_macc[5].logic_inst_n_8 ,\gen_macc[5].logic_inst_n_9 ,\gen_macc[5].logic_inst_n_10 ,\gen_macc[5].logic_inst_n_11 ,\gen_macc[5].logic_inst_n_12 ,\gen_macc[5].logic_inst_n_13 ,\gen_macc[5].logic_inst_n_14 ,\gen_macc[5].logic_inst_n_15 ,\gen_macc[5].logic_inst_n_16 ,\gen_macc[5].logic_inst_n_17 ,\gen_macc[5].logic_inst_n_18 ,\gen_macc[5].logic_inst_n_19 ,\gen_macc[5].logic_inst_n_20 ,\gen_macc[5].logic_inst_n_21 }),
        .data_clk_i(data_clk_i),
        .data_en_o_reg_0(data_en_s[6]),
        .data_en_s(data_en_s_10),
        .data_i(data_i),
        .data_in_en_s(data_in_en_s),
        .\data_out_s_reg[18] (\cpt_store_s_reg_n_0_[1] ),
        .\data_out_s_reg[18]_0 (\cpt_store_s_reg_n_0_[0] ),
        .\data_out_s_reg[18]_1 ({\gen_macc[4].logic_inst_n_6 ,\gen_macc[4].logic_inst_n_7 ,\gen_macc[4].logic_inst_n_8 ,\gen_macc[4].logic_inst_n_9 ,\gen_macc[4].logic_inst_n_10 ,\gen_macc[4].logic_inst_n_11 ,\gen_macc[4].logic_inst_n_12 ,\gen_macc[4].logic_inst_n_13 ,\gen_macc[4].logic_inst_n_14 ,\gen_macc[4].logic_inst_n_15 ,\gen_macc[4].logic_inst_n_16 ,\gen_macc[4].logic_inst_n_17 ,\gen_macc[4].logic_inst_n_18 ,\gen_macc[4].logic_inst_n_19 ,\gen_macc[4].logic_inst_n_20 ,\gen_macc[4].logic_inst_n_21 ,\gen_macc[4].logic_inst_n_22 ,\gen_macc[4].logic_inst_n_23 ,\gen_macc[4].logic_inst_n_24 }),
        .data_rst_i(data_rst_i),
        .end_next_s_0(end_next_s_0[31]),
        .end_s(end_s_20),
        .\final_res_s_reg[0]_0 (\gen_macc[6].logic_inst_n_21 ),
        .\final_res_s_reg[10]_0 (\gen_macc[6].logic_inst_n_11 ),
        .\final_res_s_reg[11]_0 (\gen_macc[6].logic_inst_n_10 ),
        .\final_res_s_reg[12]_0 (\gen_macc[6].logic_inst_n_9 ),
        .\final_res_s_reg[13]_0 (\gen_macc[6].logic_inst_n_8 ),
        .\final_res_s_reg[14]_0 (\gen_macc[6].logic_inst_n_7 ),
        .\final_res_s_reg[15]_0 (\gen_macc[6].logic_inst_n_6 ),
        .\final_res_s_reg[16]_0 (\gen_macc[6].logic_inst_n_5 ),
        .\final_res_s_reg[17]_0 (\gen_macc[6].logic_inst_n_4 ),
        .\final_res_s_reg[18]_0 (\gen_macc[6].logic_inst_n_3 ),
        .\final_res_s_reg[1]_0 (\gen_macc[6].logic_inst_n_20 ),
        .\final_res_s_reg[2]_0 (\gen_macc[6].logic_inst_n_19 ),
        .\final_res_s_reg[3]_0 (\gen_macc[6].logic_inst_n_18 ),
        .\final_res_s_reg[4]_0 (\gen_macc[6].logic_inst_n_17 ),
        .\final_res_s_reg[5]_0 (\gen_macc[6].logic_inst_n_16 ),
        .\final_res_s_reg[6]_0 (\gen_macc[6].logic_inst_n_15 ),
        .\final_res_s_reg[7]_0 (\gen_macc[6].logic_inst_n_14 ),
        .\final_res_s_reg[8]_0 (\gen_macc[6].logic_inst_n_13 ),
        .\final_res_s_reg[9]_0 (\gen_macc[6].logic_inst_n_12 ),
        .ready_next_s_1(ready_next_s_1[31]),
        .ready_s(ready_s_19),
        .res_s0(res_s0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram ram_coeff
       (.DOBDO(coeff_s),
        .Q({\cpt_s_reg_n_0_[4] ,\cpt_s_reg_n_0_[3] ,\cpt_s_reg_n_0_[2] ,\cpt_s_reg_n_0_[1] ,\cpt_s_reg_n_0_[0] }),
        .coeff_en_s(coeff_en_s),
        .data_clk_i(data_clk_i),
        .mem_reg_0(mem_reg),
        .mem_reg_1(mem_reg_0),
        .s00_axi_aclk(s00_axi_aclk));
  FDSE \ready_s_reg[0] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_next_s),
        .Q(ready_next_s_1[1]),
        .S(data_rst_i));
  FDRE \ready_s_reg[10] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__4_n_0),
        .Q(ready_next_s_1[11]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[11]),
        .Q(\ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[15] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__3_n_0),
        .Q(ready_next_s_1[16]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[16]),
        .Q(\ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[20] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__2_n_0),
        .Q(ready_next_s_1[21]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[21]),
        .Q(\ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[25] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__1_n_0),
        .Q(ready_next_s_1[26]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[26]),
        .Q(\ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[30] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__0_n_0),
        .Q(ready_next_s_1[31]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0 " *) 
  SRL16E \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[31]),
        .Q(\ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ));
  FDRE \ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0 ),
        .Q(\ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[34] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate_n_0),
        .Q(ready_next_s),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[1]),
        .Q(\ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  FDRE \ready_s_reg[5] 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(ready_s_reg_gate__5_n_0),
        .Q(ready_next_s_1[6]),
        .R(data_rst_i));
  (* srl_bus_name = "\U0/fir_top_inst/ready_s_reg " *) 
  (* srl_name = "\U0/fir_top_inst/ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 " *) 
  SRL16E \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 
       (.A0(1'b0),
        .A1(1'b1),
        .A2(1'b0),
        .A3(1'b0),
        .CE(data_in_en_s),
        .CLK(data_clk_i),
        .D(ready_next_s_1[6]),
        .Q(\ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ));
  FDRE \ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2 
       (.C(data_clk_i),
        .CE(data_in_en_s),
        .D(\ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .Q(\ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate
       (.I0(\ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0 ),
        .I1(end_s_reg_r_1_n_0),
        .O(ready_s_reg_gate_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__0
       (.I0(\ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__1
       (.I0(\ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__2
       (.I0(\ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__3
       (.I0(\ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__4
       (.I0(\ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    ready_s_reg_gate__5
       (.I0(\ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0 ),
        .I1(end_s_reg_r_2_n_0),
        .O(ready_s_reg_gate__5_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
