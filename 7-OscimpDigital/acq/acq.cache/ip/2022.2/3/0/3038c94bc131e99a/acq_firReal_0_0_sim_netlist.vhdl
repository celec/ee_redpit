-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Nov 13 11:01:52 2023
-- Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_firReal_0_0_sim_netlist.vhdl
-- Design      : acq_firReal_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi is
  port (
    s00_axi_rvalid : out STD_LOGIC;
    coeff_en_s : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \coeff_val_s_reg[15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \coeff_addr_s_reg[4]_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_reset : in STD_LOGIC;
    read_en_s : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    coeff_en_s_reg_0 : in STD_LOGIC;
    addr_s : in STD_LOGIC_VECTOR ( 1 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \coeff_addr_s_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \coeff_addr_s_reg[4]_1\ : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi is
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \coeff_addr_uns_s[1]_i_1_n_0\ : STD_LOGIC;
  signal \coeff_addr_uns_s[2]_i_1_n_0\ : STD_LOGIC;
  signal \coeff_addr_uns_s[3]_i_1_n_0\ : STD_LOGIC;
  signal \coeff_addr_uns_s[4]_i_2_n_0\ : STD_LOGIC;
  signal \readdata_s[0]_i_1_n_0\ : STD_LOGIC;
  signal \readdata_s[30]_i_1_n_0\ : STD_LOGIC;
  signal \^s00_axi_rdata\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \coeff_addr_uns_s[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \coeff_addr_uns_s[3]_i_1\ : label is "soft_lutpair0";
begin
  Q(4 downto 0) <= \^q\(4 downto 0);
  s00_axi_rdata(1 downto 0) <= \^s00_axi_rdata\(1 downto 0);
\coeff_addr_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_s_reg[4]_1\(0),
      Q => \coeff_addr_s_reg[4]_0\(0),
      R => s00_axi_reset
    );
\coeff_addr_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_s_reg[4]_1\(1),
      Q => \coeff_addr_s_reg[4]_0\(1),
      R => s00_axi_reset
    );
\coeff_addr_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_s_reg[4]_1\(2),
      Q => \coeff_addr_s_reg[4]_0\(2),
      R => s00_axi_reset
    );
\coeff_addr_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_s_reg[4]_1\(3),
      Q => \coeff_addr_s_reg[4]_0\(3),
      R => s00_axi_reset
    );
\coeff_addr_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_s_reg[4]_1\(4),
      Q => \coeff_addr_s_reg[4]_0\(4),
      R => s00_axi_reset
    );
\coeff_addr_uns_s[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => addr_s(0),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \coeff_addr_uns_s[1]_i_1_n_0\
    );
\coeff_addr_uns_s[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2888"
    )
        port map (
      I0 => addr_s(0),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => \coeff_addr_uns_s[2]_i_1_n_0\
    );
\coeff_addr_uns_s[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"28888888"
    )
        port map (
      I0 => addr_s(0),
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      O => \coeff_addr_uns_s[3]_i_1_n_0\
    );
\coeff_addr_uns_s[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2888888888888888"
    )
        port map (
      I0 => addr_s(0),
      I1 => \^q\(4),
      I2 => \^q\(3),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \^q\(2),
      O => \coeff_addr_uns_s[4]_i_2_n_0\
    );
\coeff_addr_uns_s_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => D(0),
      Q => \^q\(0),
      R => s00_axi_reset
    );
\coeff_addr_uns_s_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_uns_s[1]_i_1_n_0\,
      Q => \^q\(1),
      R => s00_axi_reset
    );
\coeff_addr_uns_s_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_uns_s[2]_i_1_n_0\,
      Q => \^q\(2),
      R => s00_axi_reset
    );
\coeff_addr_uns_s_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_uns_s[3]_i_1_n_0\,
      Q => \^q\(3),
      R => s00_axi_reset
    );
\coeff_addr_uns_s_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => \coeff_addr_s_reg[0]_0\(0),
      D => \coeff_addr_uns_s[4]_i_2_n_0\,
      Q => \^q\(4),
      R => s00_axi_reset
    );
coeff_en_s_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => coeff_en_s_reg_0,
      Q => coeff_en_s,
      R => '0'
    );
\coeff_val_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(0),
      Q => \coeff_val_s_reg[15]_0\(0),
      R => s00_axi_reset
    );
\coeff_val_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(10),
      Q => \coeff_val_s_reg[15]_0\(10),
      R => s00_axi_reset
    );
\coeff_val_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(11),
      Q => \coeff_val_s_reg[15]_0\(11),
      R => s00_axi_reset
    );
\coeff_val_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(12),
      Q => \coeff_val_s_reg[15]_0\(12),
      R => s00_axi_reset
    );
\coeff_val_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(13),
      Q => \coeff_val_s_reg[15]_0\(13),
      R => s00_axi_reset
    );
\coeff_val_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(14),
      Q => \coeff_val_s_reg[15]_0\(14),
      R => s00_axi_reset
    );
\coeff_val_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(15),
      Q => \coeff_val_s_reg[15]_0\(15),
      R => s00_axi_reset
    );
\coeff_val_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(1),
      Q => \coeff_val_s_reg[15]_0\(1),
      R => s00_axi_reset
    );
\coeff_val_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(2),
      Q => \coeff_val_s_reg[15]_0\(2),
      R => s00_axi_reset
    );
\coeff_val_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(3),
      Q => \coeff_val_s_reg[15]_0\(3),
      R => s00_axi_reset
    );
\coeff_val_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(4),
      Q => \coeff_val_s_reg[15]_0\(4),
      R => s00_axi_reset
    );
\coeff_val_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(5),
      Q => \coeff_val_s_reg[15]_0\(5),
      R => s00_axi_reset
    );
\coeff_val_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(6),
      Q => \coeff_val_s_reg[15]_0\(6),
      R => s00_axi_reset
    );
\coeff_val_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(7),
      Q => \coeff_val_s_reg[15]_0\(7),
      R => s00_axi_reset
    );
\coeff_val_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(8),
      Q => \coeff_val_s_reg[15]_0\(8),
      R => s00_axi_reset
    );
\coeff_val_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => E(0),
      D => s00_axi_wdata(9),
      Q => \coeff_val_s_reg[15]_0\(9),
      R => s00_axi_reset
    );
read_ack_o_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => read_en_s,
      Q => s00_axi_rvalid,
      R => s00_axi_reset
    );
\readdata_s[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"010F0100"
    )
        port map (
      I0 => addr_s(0),
      I1 => addr_s(1),
      I2 => s00_axi_reset,
      I3 => read_en_s,
      I4 => \^s00_axi_rdata\(0),
      O => \readdata_s[0]_i_1_n_0\
    );
\readdata_s[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FC00AA"
    )
        port map (
      I0 => \^s00_axi_rdata\(1),
      I1 => addr_s(0),
      I2 => addr_s(1),
      I3 => s00_axi_reset,
      I4 => read_en_s,
      O => \readdata_s[30]_i_1_n_0\
    );
\readdata_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \readdata_s[0]_i_1_n_0\,
      Q => \^s00_axi_rdata\(0),
      R => '0'
    );
\readdata_s_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \readdata_s[30]_i_1_n_0\,
      Q => \^s00_axi_rdata\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom is
  port (
    axi_awready_reg_0 : out STD_LOGIC;
    axi_wready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_awvalid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    addr_s : out STD_LOGIC_VECTOR ( 1 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_reset_0 : out STD_LOGIC;
    \coeff_addr_uns_s_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    read_en_s : out STD_LOGIC;
    s00_axi_reset : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom is
  signal addr_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^addr_s\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_reg_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal read_addr_s : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^read_en_s\ : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal write_addr_s : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal write_en_s : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addr_reg[1]_i_2\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \axi_awaddr[3]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of axi_awready_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \coeff_addr_s[1]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \coeff_addr_s[2]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \coeff_addr_s[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \coeff_addr_s[4]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \coeff_addr_uns_s[0]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \coeff_addr_uns_s[4]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of coeff_en_s_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of read_ack_o_i_1 : label is "soft_lutpair62";
begin
  addr_s(1 downto 0) <= \^addr_s\(1 downto 0);
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  read_en_s <= \^read_en_s\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
\addr_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => write_addr_s(0),
      I1 => write_en_s,
      I2 => read_addr_s(0),
      I3 => \^read_en_s\,
      I4 => addr_reg(0),
      O => \^addr_s\(0)
    );
\addr_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => write_addr_s(1),
      I1 => write_en_s,
      I2 => read_addr_s(1),
      I3 => \^read_en_s\,
      I4 => addr_reg(1),
      O => \^addr_s\(1)
    );
\addr_reg[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^axi_awready_reg_0\,
      I1 => \^axi_wready_reg_0\,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      O => write_en_s
    );
\addr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \^addr_s\(0),
      Q => addr_reg(0),
      R => '0'
    );
\addr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \^addr_s\(1),
      Q => addr_reg(1),
      R => '0'
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^axi_arready_reg_0\,
      I3 => read_addr_s(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^axi_arready_reg_0\,
      I3 => read_addr_s(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => read_addr_s(0),
      S => s00_axi_reset
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => read_addr_s(1),
      S => s00_axi_reset
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => s00_axi_reset
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_awready_reg_0\,
      I4 => write_addr_s(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_awready_reg_0\,
      I4 => write_addr_s(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => write_addr_s(0),
      R => s00_axi_reset
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => write_addr_s(1),
      R => s00_axi_reset
    );
axi_awready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => s00_axi_reset
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => s00_axi_reset
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^axi_arready_reg_0\,
      I1 => s00_axi_arvalid,
      I2 => axi_rvalid_reg_n_0,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => axi_rvalid_reg_n_0,
      R => s00_axi_reset
    );
axi_wready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => s00_axi_awvalid,
      I2 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => s00_axi_reset
    );
\coeff_addr_s[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(0),
      O => \coeff_addr_uns_s_reg[4]\(0)
    );
\coeff_addr_s[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(1),
      O => \coeff_addr_uns_s_reg[4]\(1)
    );
\coeff_addr_s[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(2),
      O => \coeff_addr_uns_s_reg[4]\(2)
    );
\coeff_addr_s[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(3),
      O => \coeff_addr_uns_s_reg[4]\(3)
    );
\coeff_addr_s[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(4),
      O => \coeff_addr_uns_s_reg[4]\(4)
    );
\coeff_addr_uns_s[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^addr_s\(0),
      I1 => Q(0),
      O => D(0)
    );
\coeff_addr_uns_s[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => \^addr_s\(1),
      O => s00_axi_awvalid_0(0)
    );
coeff_en_s_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \^addr_s\(1),
      I1 => \^addr_s\(0),
      I2 => write_en_s,
      I3 => s00_axi_reset,
      O => s00_axi_reset_0
    );
\coeff_val_s[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^axi_wready_reg_0\,
      I3 => \^axi_awready_reg_0\,
      I4 => \^addr_s\(1),
      I5 => \^addr_s\(0),
      O => E(0)
    );
read_ack_o_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      I2 => axi_rvalid_reg_n_0,
      O => \^read_en_s\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc is
  port (
    data_en_s_0 : out STD_LOGIC;
    data_en_s : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 18 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0 : out STD_LOGIC;
    data_en_s_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0_1 : out STD_LOGIC;
    data_en_s_reg_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0_2 : out STD_LOGIC;
    data_en_s_reg_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0_3 : out STD_LOGIC;
    data_en_s_reg_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0_4 : out STD_LOGIC;
    data_en_s_reg_4 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_s0_5 : out STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    data_in_en_s : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    DOBDO : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    end_s : in STD_LOGIC;
    ready_s : in STD_LOGIC;
    end_s_6 : in STD_LOGIC;
    ready_s_7 : in STD_LOGIC;
    end_s_8 : in STD_LOGIC;
    ready_s_9 : in STD_LOGIC;
    end_s_10 : in STD_LOGIC;
    ready_s_11 : in STD_LOGIC;
    end_s_12 : in STD_LOGIC;
    ready_s_13 : in STD_LOGIC;
    end_s_14 : in STD_LOGIC;
    ready_s_15 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc is
  signal \^data_en_s_0\ : STD_LOGIC;
  signal data_out_en_s : STD_LOGIC;
  signal end_s_0 : STD_LOGIC;
  signal must_rst_s : STD_LOGIC;
  signal \must_rst_s_i_1__5_n_0\ : STD_LOGIC;
  signal ready_s_1 : STD_LOGIC;
  signal ready_s_i_1_n_0 : STD_LOGIC;
  signal res_s0_6 : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \data_en_o_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \data_en_o_i_1__1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_en_o_i_1__2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_en_o_i_1__3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \data_en_o_i_1__4\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \must_rst_s_i_1__5\ : label is "soft_lutpair1";
begin
  data_en_s_0 <= \^data_en_s_0\;
data_en_o_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_0,
      O => data_out_en_s
    );
\data_en_o_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s,
      O => E(0)
    );
\data_en_o_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_6,
      O => data_en_s_reg_0(0)
    );
\data_en_o_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_8,
      O => data_en_s_reg_1(0)
    );
\data_en_o_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_10,
      O => data_en_s_reg_2(0)
    );
\data_en_o_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_12,
      O => data_en_s_reg_3(0)
    );
\data_en_o_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => end_s_14,
      O => data_en_s_reg_4(0)
    );
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => data_out_en_s,
      Q => data_en_s(0),
      R => data_rst_i
    );
data_en_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => data_in_en_s,
      Q => \^data_en_s_0\,
      R => data_rst_i
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s_0,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_105,
      Q => Q(0),
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_95,
      Q => Q(10),
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_94,
      Q => Q(11),
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_93,
      Q => Q(12),
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_92,
      Q => Q(13),
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_91,
      Q => Q(14),
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_90,
      Q => Q(15),
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_89,
      Q => Q(16),
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_88,
      Q => Q(17),
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_87,
      Q => Q(18),
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_104,
      Q => Q(1),
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_103,
      Q => Q(2),
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_102,
      Q => Q(3),
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_101,
      Q => Q(4),
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_100,
      Q => Q(5),
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_99,
      Q => Q(6),
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_98,
      Q => Q(7),
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_97,
      Q => Q(8),
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_out_en_s,
      D => res_s_reg_n_96,
      Q => Q(9),
      R => data_rst_i
    );
\must_rst_s_i_1__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => \^data_en_s_0\,
      I2 => ready_s_1,
      I3 => must_rst_s,
      O => \must_rst_s_i_1__5_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__5_n_0\,
      Q => must_rst_s,
      R => data_rst_i
    );
ready_s_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => ready_s_1,
      O => ready_s_i_1_n_0
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => ready_s_i_1_n_0,
      Q => ready_s_1,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => DOBDO(15),
      A(28) => DOBDO(15),
      A(27) => DOBDO(15),
      A(26) => DOBDO(15),
      A(25) => DOBDO(15),
      A(24) => DOBDO(15),
      A(23) => DOBDO(15),
      A(22) => DOBDO(15),
      A(21) => DOBDO(15),
      A(20) => DOBDO(15),
      A(19) => DOBDO(15),
      A(18) => DOBDO(15),
      A(17) => DOBDO(15),
      A(16) => DOBDO(15),
      A(15 downto 0) => DOBDO(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0_6,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
res_s_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_1,
      O => res_s0_6
    );
\res_s_reg_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s,
      O => res_s0
    );
\res_s_reg_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_7,
      O => res_s0_1
    );
\res_s_reg_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_9,
      O => res_s0_2
    );
\res_s_reg_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_11,
      O => res_s0_3
    );
\res_s_reg_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_13,
      O => res_s0_4
    );
\res_s_reg_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^data_en_s_0\,
      I1 => ready_s_15,
      O => res_s0_5
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0 is
  port (
    end_s : out STD_LOGIC;
    data_en_s : out STD_LOGIC_VECTOR ( 0 to 0 );
    ready_s : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0 is
  signal \must_rst_s_i_1__4_n_0\ : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__0_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => data_en_s(0),
      R => data_rst_i
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => Q(0),
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => Q(10),
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => Q(11),
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => Q(12),
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => Q(13),
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => Q(14),
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => Q(15),
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => Q(16),
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => Q(17),
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => Q(18),
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => Q(1),
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => Q(2),
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => Q(3),
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => Q(4),
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => Q(5),
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => Q(6),
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => Q(7),
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => Q(8),
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => Q(9),
      R => data_rst_i
    );
\must_rst_s_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s_0,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => \must_rst_s_i_1__4_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__4_n_0\,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__0_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__0_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1 is
  port (
    end_s : out STD_LOGIC;
    data_en_s : out STD_LOGIC_VECTOR ( 0 to 0 );
    ready_s : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1 is
  signal \must_rst_s_i_1__3_n_0\ : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__1_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => data_en_s(0),
      R => data_rst_i
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => Q(0),
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => Q(10),
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => Q(11),
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => Q(12),
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => Q(13),
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => Q(14),
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => Q(15),
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => Q(16),
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => Q(17),
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => Q(18),
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => Q(1),
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => Q(2),
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => Q(3),
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => Q(4),
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => Q(5),
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => Q(6),
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => Q(7),
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => Q(8),
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => Q(9),
      R => data_rst_i
    );
\must_rst_s_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s_0,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => \must_rst_s_i_1__3_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__3_n_0\,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__1_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__1_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2 is
  port (
    end_s : out STD_LOGIC;
    data_en_s : out STD_LOGIC_VECTOR ( 0 to 0 );
    ready_s : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s_0 : in STD_LOGIC;
    \data_out_s_reg[18]\ : in STD_LOGIC;
    \data_out_s_reg[18]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 18 downto 0 );
    \data_out_s_reg[18]_1\ : in STD_LOGIC;
    \data_out_s_reg[18]_2\ : in STD_LOGIC_VECTOR ( 18 downto 0 );
    \data_out_s_reg[18]_3\ : in STD_LOGIC;
    \data_out_s_reg[18]_4\ : in STD_LOGIC_VECTOR ( 18 downto 0 );
    \data_out_s_reg[17]\ : in STD_LOGIC;
    \data_out_s_reg[16]\ : in STD_LOGIC;
    \data_out_s_reg[15]\ : in STD_LOGIC;
    \data_out_s_reg[14]\ : in STD_LOGIC;
    \data_out_s_reg[13]\ : in STD_LOGIC;
    \data_out_s_reg[12]\ : in STD_LOGIC;
    \data_out_s_reg[11]\ : in STD_LOGIC;
    \data_out_s_reg[10]\ : in STD_LOGIC;
    \data_out_s_reg[9]\ : in STD_LOGIC;
    \data_out_s_reg[8]\ : in STD_LOGIC;
    \data_out_s_reg[7]\ : in STD_LOGIC;
    \data_out_s_reg[6]\ : in STD_LOGIC;
    \data_out_s_reg[5]\ : in STD_LOGIC;
    \data_out_s_reg[4]\ : in STD_LOGIC;
    \data_out_s_reg[3]\ : in STD_LOGIC;
    \data_out_s_reg[2]\ : in STD_LOGIC;
    \data_out_s_reg[1]\ : in STD_LOGIC;
    \data_out_s_reg[0]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2 is
  signal \data_out_s[0]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[10]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[11]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[12]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[13]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[14]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[15]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[16]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[17]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[18]_i_4_n_0\ : STD_LOGIC;
  signal \data_out_s[1]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[2]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[3]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[4]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[5]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[6]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[7]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[8]_i_2_n_0\ : STD_LOGIC;
  signal \data_out_s[9]_i_2_n_0\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[0]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[10]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[11]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[12]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[13]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[14]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[15]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[16]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[17]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[18]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[1]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[2]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[3]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[4]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[5]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[6]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[7]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[8]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[9]\ : STD_LOGIC;
  signal \must_rst_s_i_1__2_n_0\ : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__2_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => data_en_s(0),
      R => data_rst_i
    );
\data_out_s[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[0]\,
      I1 => Q(0),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(0),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(0),
      O => \data_out_s[0]_i_2_n_0\
    );
\data_out_s[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[10]\,
      I1 => Q(10),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(10),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(10),
      O => \data_out_s[10]_i_2_n_0\
    );
\data_out_s[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[11]\,
      I1 => Q(11),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(11),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(11),
      O => \data_out_s[11]_i_2_n_0\
    );
\data_out_s[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[12]\,
      I1 => Q(12),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(12),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(12),
      O => \data_out_s[12]_i_2_n_0\
    );
\data_out_s[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[13]\,
      I1 => Q(13),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(13),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(13),
      O => \data_out_s[13]_i_2_n_0\
    );
\data_out_s[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[14]\,
      I1 => Q(14),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(14),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(14),
      O => \data_out_s[14]_i_2_n_0\
    );
\data_out_s[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[15]\,
      I1 => Q(15),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(15),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(15),
      O => \data_out_s[15]_i_2_n_0\
    );
\data_out_s[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[16]\,
      I1 => Q(16),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(16),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(16),
      O => \data_out_s[16]_i_2_n_0\
    );
\data_out_s[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[17]\,
      I1 => Q(17),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(17),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(17),
      O => \data_out_s[17]_i_2_n_0\
    );
\data_out_s[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[18]\,
      I1 => Q(18),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(18),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(18),
      O => \data_out_s[18]_i_4_n_0\
    );
\data_out_s[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[1]\,
      I1 => Q(1),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(1),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(1),
      O => \data_out_s[1]_i_2_n_0\
    );
\data_out_s[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[2]\,
      I1 => Q(2),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(2),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(2),
      O => \data_out_s[2]_i_2_n_0\
    );
\data_out_s[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[3]\,
      I1 => Q(3),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(3),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(3),
      O => \data_out_s[3]_i_2_n_0\
    );
\data_out_s[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[4]\,
      I1 => Q(4),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(4),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(4),
      O => \data_out_s[4]_i_2_n_0\
    );
\data_out_s[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[5]\,
      I1 => Q(5),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(5),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(5),
      O => \data_out_s[5]_i_2_n_0\
    );
\data_out_s[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[6]\,
      I1 => Q(6),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(6),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(6),
      O => \data_out_s[6]_i_2_n_0\
    );
\data_out_s[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[7]\,
      I1 => Q(7),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(7),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(7),
      O => \data_out_s[7]_i_2_n_0\
    );
\data_out_s[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[8]\,
      I1 => Q(8),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(8),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(8),
      O => \data_out_s[8]_i_2_n_0\
    );
\data_out_s[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[9]\,
      I1 => Q(9),
      I2 => \data_out_s_reg[18]_1\,
      I3 => \data_out_s_reg[18]_2\(9),
      I4 => \data_out_s_reg[18]_3\,
      I5 => \data_out_s_reg[18]_4\(9),
      O => \data_out_s[9]_i_2_n_0\
    );
\data_out_s_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[0]_i_2_n_0\,
      I1 => \data_out_s_reg[0]\,
      O => D(0),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[10]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[10]_i_2_n_0\,
      I1 => \data_out_s_reg[10]\,
      O => D(10),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[11]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[11]_i_2_n_0\,
      I1 => \data_out_s_reg[11]\,
      O => D(11),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[12]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[12]_i_2_n_0\,
      I1 => \data_out_s_reg[12]\,
      O => D(12),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[13]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[13]_i_2_n_0\,
      I1 => \data_out_s_reg[13]\,
      O => D(13),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[14]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[14]_i_2_n_0\,
      I1 => \data_out_s_reg[14]\,
      O => D(14),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[15]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[15]_i_2_n_0\,
      I1 => \data_out_s_reg[15]\,
      O => D(15),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[16]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[16]_i_2_n_0\,
      I1 => \data_out_s_reg[16]\,
      O => D(16),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[17]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[17]_i_2_n_0\,
      I1 => \data_out_s_reg[17]\,
      O => D(17),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[18]_i_4_n_0\,
      I1 => \data_out_s_reg[18]_0\,
      O => D(18),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[1]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[1]_i_2_n_0\,
      I1 => \data_out_s_reg[1]\,
      O => D(1),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[2]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[2]_i_2_n_0\,
      I1 => \data_out_s_reg[2]\,
      O => D(2),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[3]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[3]_i_2_n_0\,
      I1 => \data_out_s_reg[3]\,
      O => D(3),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[4]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[4]_i_2_n_0\,
      I1 => \data_out_s_reg[4]\,
      O => D(4),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[5]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[5]_i_2_n_0\,
      I1 => \data_out_s_reg[5]\,
      O => D(5),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[6]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[6]_i_2_n_0\,
      I1 => \data_out_s_reg[6]\,
      O => D(6),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[7]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[7]_i_2_n_0\,
      I1 => \data_out_s_reg[7]\,
      O => D(7),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[8]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[8]_i_2_n_0\,
      I1 => \data_out_s_reg[8]\,
      O => D(8),
      S => \data_out_s_reg[18]\
    );
\data_out_s_reg[9]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \data_out_s[9]_i_2_n_0\,
      I1 => \data_out_s_reg[9]\,
      O => D(9),
      S => \data_out_s_reg[18]\
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => \final_res_s_reg_n_0_[0]\,
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => \final_res_s_reg_n_0_[10]\,
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => \final_res_s_reg_n_0_[11]\,
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => \final_res_s_reg_n_0_[12]\,
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => \final_res_s_reg_n_0_[13]\,
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => \final_res_s_reg_n_0_[14]\,
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => \final_res_s_reg_n_0_[15]\,
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => \final_res_s_reg_n_0_[16]\,
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => \final_res_s_reg_n_0_[17]\,
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => \final_res_s_reg_n_0_[18]\,
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => \final_res_s_reg_n_0_[1]\,
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => \final_res_s_reg_n_0_[2]\,
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => \final_res_s_reg_n_0_[3]\,
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => \final_res_s_reg_n_0_[4]\,
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => \final_res_s_reg_n_0_[5]\,
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => \final_res_s_reg_n_0_[6]\,
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => \final_res_s_reg_n_0_[7]\,
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => \final_res_s_reg_n_0_[8]\,
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => \final_res_s_reg_n_0_[9]\,
      R => data_rst_i
    );
\must_rst_s_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s_0,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => \must_rst_s_i_1__2_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__2_n_0\,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__2_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__2_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3 is
  port (
    end_s : out STD_LOGIC;
    ready_s : out STD_LOGIC;
    data_en_o_reg_0 : out STD_LOGIC;
    data_en_o_reg_1 : out STD_LOGIC;
    data_en_o_reg_2 : out STD_LOGIC;
    data_en_o_reg_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    \cpt_store_s_reg[2]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \cpt_store_s_reg[0]\ : in STD_LOGIC;
    \cpt_store_s_reg[0]_0\ : in STD_LOGIC;
    \cpt_store_s_reg[0]_1\ : in STD_LOGIC;
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3 is
  signal \data_en_s__0\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \data_out_s[18]_i_3_n_0\ : STD_LOGIC;
  signal \must_rst_s_i_1__1_n_0\ : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__3_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
\cpt_store_s[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101000E01010E0E"
    )
        port map (
      I0 => \data_out_s[18]_i_3_n_0\,
      I1 => \cpt_store_s_reg[2]\(4),
      I2 => data_rst_i,
      I3 => \cpt_store_s_reg[0]\,
      I4 => \cpt_store_s_reg[0]_0\,
      I5 => \cpt_store_s_reg[0]_1\,
      O => data_en_o_reg_0
    );
\cpt_store_s[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010E0100010E0F00"
    )
        port map (
      I0 => \data_out_s[18]_i_3_n_0\,
      I1 => \cpt_store_s_reg[2]\(4),
      I2 => data_rst_i,
      I3 => \cpt_store_s_reg[0]\,
      I4 => \cpt_store_s_reg[0]_0\,
      I5 => \cpt_store_s_reg[0]_1\,
      O => data_en_o_reg_1
    );
\cpt_store_s[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010F010F0E000000"
    )
        port map (
      I0 => \data_out_s[18]_i_3_n_0\,
      I1 => \cpt_store_s_reg[2]\(4),
      I2 => data_rst_i,
      I3 => \cpt_store_s_reg[0]\,
      I4 => \cpt_store_s_reg[0]_0\,
      I5 => \cpt_store_s_reg[0]_1\,
      O => data_en_o_reg_2
    );
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => \data_en_s__0\(4),
      R => data_rst_i
    );
\data_out_s[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \data_out_s[18]_i_3_n_0\,
      I1 => \cpt_store_s_reg[2]\(4),
      O => data_en_o_reg_3(0)
    );
\data_out_s[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \data_en_s__0\(4),
      I1 => \cpt_store_s_reg[2]\(2),
      I2 => \cpt_store_s_reg[2]\(0),
      I3 => \cpt_store_s_reg[2]\(1),
      I4 => \cpt_store_s_reg[2]\(3),
      I5 => \cpt_store_s_reg[2]\(5),
      O => \data_out_s[18]_i_3_n_0\
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => Q(0),
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => Q(10),
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => Q(11),
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => Q(12),
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => Q(13),
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => Q(14),
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => Q(15),
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => Q(16),
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => Q(17),
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => Q(18),
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => Q(1),
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => Q(2),
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => Q(3),
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => Q(4),
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => Q(5),
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => Q(6),
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => Q(7),
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => Q(8),
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => Q(9),
      R => data_rst_i
    );
\must_rst_s_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => \must_rst_s_i_1__1_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__1_n_0\,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__3_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__3_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4 is
  port (
    end_s : out STD_LOGIC;
    data_en_o_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    ready_s : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4 is
  signal \must_rst_s_i_1__0_n_0\ : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__4_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => data_en_o_reg_0(0),
      R => data_rst_i
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => Q(0),
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => Q(10),
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => Q(11),
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => Q(12),
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => Q(13),
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => Q(14),
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => Q(15),
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => Q(16),
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => Q(17),
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => Q(18),
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => Q(1),
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => Q(2),
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => Q(3),
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => Q(4),
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => Q(5),
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => Q(6),
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => Q(7),
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => Q(8),
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => Q(9),
      R => data_rst_i
    );
\must_rst_s_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => \must_rst_s_i_1__0_n_0\
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \must_rst_s_i_1__0_n_0\,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__4_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__4_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5 is
  port (
    end_s : out STD_LOGIC;
    data_en_o_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    ready_s : out STD_LOGIC;
    \final_res_s_reg[18]_0\ : out STD_LOGIC;
    \final_res_s_reg[17]_0\ : out STD_LOGIC;
    \final_res_s_reg[16]_0\ : out STD_LOGIC;
    \final_res_s_reg[15]_0\ : out STD_LOGIC;
    \final_res_s_reg[14]_0\ : out STD_LOGIC;
    \final_res_s_reg[13]_0\ : out STD_LOGIC;
    \final_res_s_reg[12]_0\ : out STD_LOGIC;
    \final_res_s_reg[11]_0\ : out STD_LOGIC;
    \final_res_s_reg[10]_0\ : out STD_LOGIC;
    \final_res_s_reg[9]_0\ : out STD_LOGIC;
    \final_res_s_reg[8]_0\ : out STD_LOGIC;
    \final_res_s_reg[7]_0\ : out STD_LOGIC;
    \final_res_s_reg[6]_0\ : out STD_LOGIC;
    \final_res_s_reg[5]_0\ : out STD_LOGIC;
    \final_res_s_reg[4]_0\ : out STD_LOGIC;
    \final_res_s_reg[3]_0\ : out STD_LOGIC;
    \final_res_s_reg[2]_0\ : out STD_LOGIC;
    \final_res_s_reg[1]_0\ : out STD_LOGIC;
    \final_res_s_reg[0]_0\ : out STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    end_next_s_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_clk_i : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_in_en_s : in STD_LOGIC;
    res_s0 : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready_next_s_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_s : in STD_LOGIC;
    \data_out_s_reg[18]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 18 downto 0 );
    \data_out_s_reg[18]_0\ : in STD_LOGIC;
    \data_out_s_reg[18]_1\ : in STD_LOGIC_VECTOR ( 18 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5 : entity is "firReal_proc";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5 is
  signal \final_res_s_reg_n_0_[0]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[10]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[11]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[12]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[13]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[14]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[15]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[16]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[17]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[18]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[1]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[2]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[3]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[4]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[5]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[6]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[7]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[8]\ : STD_LOGIC;
  signal \final_res_s_reg_n_0_[9]\ : STD_LOGIC;
  signal must_rst_s_i_1_n_0 : STD_LOGIC;
  signal must_rst_s_reg_n_0 : STD_LOGIC;
  signal \^ready_s\ : STD_LOGIC;
  signal \ready_s_i_1__5_n_0\ : STD_LOGIC;
  signal res_s_reg_n_100 : STD_LOGIC;
  signal res_s_reg_n_101 : STD_LOGIC;
  signal res_s_reg_n_102 : STD_LOGIC;
  signal res_s_reg_n_103 : STD_LOGIC;
  signal res_s_reg_n_104 : STD_LOGIC;
  signal res_s_reg_n_105 : STD_LOGIC;
  signal res_s_reg_n_71 : STD_LOGIC;
  signal res_s_reg_n_72 : STD_LOGIC;
  signal res_s_reg_n_73 : STD_LOGIC;
  signal res_s_reg_n_74 : STD_LOGIC;
  signal res_s_reg_n_75 : STD_LOGIC;
  signal res_s_reg_n_76 : STD_LOGIC;
  signal res_s_reg_n_77 : STD_LOGIC;
  signal res_s_reg_n_78 : STD_LOGIC;
  signal res_s_reg_n_79 : STD_LOGIC;
  signal res_s_reg_n_80 : STD_LOGIC;
  signal res_s_reg_n_81 : STD_LOGIC;
  signal res_s_reg_n_82 : STD_LOGIC;
  signal res_s_reg_n_83 : STD_LOGIC;
  signal res_s_reg_n_84 : STD_LOGIC;
  signal res_s_reg_n_85 : STD_LOGIC;
  signal res_s_reg_n_86 : STD_LOGIC;
  signal res_s_reg_n_87 : STD_LOGIC;
  signal res_s_reg_n_88 : STD_LOGIC;
  signal res_s_reg_n_89 : STD_LOGIC;
  signal res_s_reg_n_90 : STD_LOGIC;
  signal res_s_reg_n_91 : STD_LOGIC;
  signal res_s_reg_n_92 : STD_LOGIC;
  signal res_s_reg_n_93 : STD_LOGIC;
  signal res_s_reg_n_94 : STD_LOGIC;
  signal res_s_reg_n_95 : STD_LOGIC;
  signal res_s_reg_n_96 : STD_LOGIC;
  signal res_s_reg_n_97 : STD_LOGIC;
  signal res_s_reg_n_98 : STD_LOGIC;
  signal res_s_reg_n_99 : STD_LOGIC;
  signal NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_res_s_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_res_s_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_res_s_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_res_s_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 35 );
  signal NLW_res_s_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
  ready_s <= \^ready_s\;
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => E(0),
      Q => data_en_o_reg_0(0),
      R => data_rst_i
    );
\data_out_s[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[0]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(0),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(0),
      O => \final_res_s_reg[0]_0\
    );
\data_out_s[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[10]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(10),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(10),
      O => \final_res_s_reg[10]_0\
    );
\data_out_s[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[11]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(11),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(11),
      O => \final_res_s_reg[11]_0\
    );
\data_out_s[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[12]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(12),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(12),
      O => \final_res_s_reg[12]_0\
    );
\data_out_s[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[13]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(13),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(13),
      O => \final_res_s_reg[13]_0\
    );
\data_out_s[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[14]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(14),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(14),
      O => \final_res_s_reg[14]_0\
    );
\data_out_s[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[15]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(15),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(15),
      O => \final_res_s_reg[15]_0\
    );
\data_out_s[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[16]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(16),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(16),
      O => \final_res_s_reg[16]_0\
    );
\data_out_s[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[17]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(17),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(17),
      O => \final_res_s_reg[17]_0\
    );
\data_out_s[18]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[18]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(18),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(18),
      O => \final_res_s_reg[18]_0\
    );
\data_out_s[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[1]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(1),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(1),
      O => \final_res_s_reg[1]_0\
    );
\data_out_s[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[2]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(2),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(2),
      O => \final_res_s_reg[2]_0\
    );
\data_out_s[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[3]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(3),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(3),
      O => \final_res_s_reg[3]_0\
    );
\data_out_s[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[4]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(4),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(4),
      O => \final_res_s_reg[4]_0\
    );
\data_out_s[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[5]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(5),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(5),
      O => \final_res_s_reg[5]_0\
    );
\data_out_s[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[6]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(6),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(6),
      O => \final_res_s_reg[6]_0\
    );
\data_out_s[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[7]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(7),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(7),
      O => \final_res_s_reg[7]_0\
    );
\data_out_s[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[8]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(8),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(8),
      O => \final_res_s_reg[8]_0\
    );
\data_out_s[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \final_res_s_reg_n_0_[9]\,
      I1 => \data_out_s_reg[18]\,
      I2 => Q(9),
      I3 => \data_out_s_reg[18]_0\,
      I4 => \data_out_s_reg[18]_1\(9),
      O => \final_res_s_reg[9]_0\
    );
end_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => end_next_s_0(0),
      Q => end_s,
      R => data_rst_i
    );
\final_res_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_105,
      Q => \final_res_s_reg_n_0_[0]\,
      R => data_rst_i
    );
\final_res_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_95,
      Q => \final_res_s_reg_n_0_[10]\,
      R => data_rst_i
    );
\final_res_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_94,
      Q => \final_res_s_reg_n_0_[11]\,
      R => data_rst_i
    );
\final_res_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_93,
      Q => \final_res_s_reg_n_0_[12]\,
      R => data_rst_i
    );
\final_res_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_92,
      Q => \final_res_s_reg_n_0_[13]\,
      R => data_rst_i
    );
\final_res_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_91,
      Q => \final_res_s_reg_n_0_[14]\,
      R => data_rst_i
    );
\final_res_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_90,
      Q => \final_res_s_reg_n_0_[15]\,
      R => data_rst_i
    );
\final_res_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_89,
      Q => \final_res_s_reg_n_0_[16]\,
      R => data_rst_i
    );
\final_res_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_88,
      Q => \final_res_s_reg_n_0_[17]\,
      R => data_rst_i
    );
\final_res_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_87,
      Q => \final_res_s_reg_n_0_[18]\,
      R => data_rst_i
    );
\final_res_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_104,
      Q => \final_res_s_reg_n_0_[1]\,
      R => data_rst_i
    );
\final_res_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_103,
      Q => \final_res_s_reg_n_0_[2]\,
      R => data_rst_i
    );
\final_res_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_102,
      Q => \final_res_s_reg_n_0_[3]\,
      R => data_rst_i
    );
\final_res_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_101,
      Q => \final_res_s_reg_n_0_[4]\,
      R => data_rst_i
    );
\final_res_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_100,
      Q => \final_res_s_reg_n_0_[5]\,
      R => data_rst_i
    );
\final_res_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_99,
      Q => \final_res_s_reg_n_0_[6]\,
      R => data_rst_i
    );
\final_res_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_98,
      Q => \final_res_s_reg_n_0_[7]\,
      R => data_rst_i
    );
\final_res_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_97,
      Q => \final_res_s_reg_n_0_[8]\,
      R => data_rst_i
    );
\final_res_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => E(0),
      D => res_s_reg_n_96,
      Q => \final_res_s_reg_n_0_[9]\,
      R => data_rst_i
    );
must_rst_s_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => end_next_s_0(0),
      I1 => data_en_s,
      I2 => \^ready_s\,
      I3 => must_rst_s_reg_n_0,
      O => must_rst_s_i_1_n_0
    );
must_rst_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => must_rst_s_i_1_n_0,
      Q => must_rst_s_reg_n_0,
      R => data_rst_i
    );
\ready_s_i_1__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => ready_next_s_1(0),
      I1 => data_in_en_s,
      I2 => end_next_s_0(0),
      I3 => \^ready_s\,
      O => \ready_s_i_1__5_n_0\
    );
ready_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \ready_s_i_1__5_n_0\,
      Q => \^ready_s\,
      R => data_rst_i
    );
res_s_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(15),
      A(28) => A(15),
      A(27) => A(15),
      A(26) => A(15),
      A(25) => A(15),
      A(24) => A(15),
      A(23) => A(15),
      A(22) => A(15),
      A(21) => A(15),
      A(20) => A(15),
      A(19) => A(15),
      A(18) => A(15),
      A(17) => A(15),
      A(16) => A(15),
      A(15 downto 0) => A(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_res_s_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => data_i(13),
      B(16) => data_i(13),
      B(15) => data_i(13),
      B(14) => data_i(13),
      B(13 downto 0) => data_i(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_res_s_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_res_s_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_res_s_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => data_in_en_s,
      CEA2 => data_in_en_s,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => data_in_en_s,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => res_s0,
      CLK => data_clk_i,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_res_s_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 5) => B"01",
      OPMODE(4) => must_rst_s_reg_n_0,
      OPMODE(3 downto 0) => B"0101",
      OVERFLOW => NLW_res_s_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 35) => NLW_res_s_reg_P_UNCONNECTED(47 downto 35),
      P(34) => res_s_reg_n_71,
      P(33) => res_s_reg_n_72,
      P(32) => res_s_reg_n_73,
      P(31) => res_s_reg_n_74,
      P(30) => res_s_reg_n_75,
      P(29) => res_s_reg_n_76,
      P(28) => res_s_reg_n_77,
      P(27) => res_s_reg_n_78,
      P(26) => res_s_reg_n_79,
      P(25) => res_s_reg_n_80,
      P(24) => res_s_reg_n_81,
      P(23) => res_s_reg_n_82,
      P(22) => res_s_reg_n_83,
      P(21) => res_s_reg_n_84,
      P(20) => res_s_reg_n_85,
      P(19) => res_s_reg_n_86,
      P(18) => res_s_reg_n_87,
      P(17) => res_s_reg_n_88,
      P(16) => res_s_reg_n_89,
      P(15) => res_s_reg_n_90,
      P(14) => res_s_reg_n_91,
      P(13) => res_s_reg_n_92,
      P(12) => res_s_reg_n_93,
      P(11) => res_s_reg_n_94,
      P(10) => res_s_reg_n_95,
      P(9) => res_s_reg_n_96,
      P(8) => res_s_reg_n_97,
      P(7) => res_s_reg_n_98,
      P(6) => res_s_reg_n_99,
      P(5) => res_s_reg_n_100,
      P(4) => res_s_reg_n_101,
      P(3) => res_s_reg_n_102,
      P(2) => res_s_reg_n_103,
      P(1) => res_s_reg_n_104,
      P(0) => res_s_reg_n_105,
      PATTERNBDETECT => NLW_res_s_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_res_s_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_res_s_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => data_rst_i,
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => data_rst_i,
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => data_rst_i,
      UNDERFLOW => NLW_res_s_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram is
  port (
    DOBDO : out STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    coeff_en_s : in STD_LOGIC;
    mem_reg_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    mem_reg_1 : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram is
  signal NLW_mem_reg_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_mem_reg_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_mem_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of mem_reg : label is "p0_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of mem_reg : label is "p0_d16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of mem_reg : label is "{SYNTH-4 {cell *THIS*} {string 5}} {SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of mem_reg : label is 512;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of mem_reg : label is "U0/fir_top_inst/ram_coeff/mem_reg";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of mem_reg : label is "RAM_SDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of mem_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of mem_reg : label is 1023;
  attribute ram_offset : integer;
  attribute ram_offset of mem_reg : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of mem_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of mem_reg : label is 15;
begin
mem_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 9) => B"00000",
      ADDRARDADDR(8 downto 4) => mem_reg_0(4 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13 downto 9) => B"00000",
      ADDRBWRADDR(8 downto 4) => Q(4 downto 0),
      ADDRBWRADDR(3 downto 0) => B"0000",
      CLKARDCLK => s00_axi_aclk,
      CLKBWRCLK => data_clk_i,
      DIADI(15 downto 0) => mem_reg_1(15 downto 0),
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"00",
      DIPBDIP(1 downto 0) => B"00",
      DOADO(15 downto 0) => NLW_mem_reg_DOADO_UNCONNECTED(15 downto 0),
      DOBDO(15 downto 0) => DOBDO(15 downto 0),
      DOPADOP(1 downto 0) => NLW_mem_reg_DOPADOP_UNCONNECTED(1 downto 0),
      DOPBDOP(1 downto 0) => NLW_mem_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => coeff_en_s,
      ENBWREN => '1',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"11",
      WEBWE(3 downto 0) => B"0000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top is
  port (
    tick_o : out STD_LOGIC;
    data_o : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_rst_i : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    coeff_en_s : in STD_LOGIC;
    mem_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    mem_reg_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_en_i : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top is
  signal coeff_s : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[13]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[18]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[23]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[28]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[3]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg[8]\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \coeff_tab_s_reg_gate__0_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__10_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__11_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__12_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__13_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__14_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__15_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__16_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__17_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__18_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__19_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__1_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__20_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__21_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__22_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__23_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__24_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__25_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__26_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__27_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__28_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__29_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__2_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__30_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__31_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__32_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__33_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__34_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__35_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__36_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__37_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__38_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__39_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__3_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__40_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__41_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__42_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__43_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__44_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__45_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__46_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__47_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__48_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__49_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__4_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__50_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__51_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__52_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__53_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__54_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__55_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__56_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__57_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__58_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__59_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__5_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__60_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__61_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__62_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__63_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__64_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__65_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__66_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__67_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__68_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__69_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__6_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__70_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__71_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__72_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__73_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__74_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__75_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__76_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__77_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__78_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__79_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__7_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__80_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__81_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__82_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__83_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__84_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__85_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__86_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__87_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__88_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__89_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__8_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__90_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__91_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__92_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__93_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__94_n_0\ : STD_LOGIC;
  signal \coeff_tab_s_reg_gate__9_n_0\ : STD_LOGIC;
  signal coeff_tab_s_reg_gate_n_0 : STD_LOGIC;
  signal \cpt_s_reg_n_0_[0]\ : STD_LOGIC;
  signal \cpt_s_reg_n_0_[1]\ : STD_LOGIC;
  signal \cpt_s_reg_n_0_[2]\ : STD_LOGIC;
  signal \cpt_s_reg_n_0_[3]\ : STD_LOGIC;
  signal \cpt_s_reg_n_0_[4]\ : STD_LOGIC;
  signal \cpt_s_reg_n_0_[5]\ : STD_LOGIC;
  signal \cpt_store_s_reg_n_0_[0]\ : STD_LOGIC;
  signal \cpt_store_s_reg_n_0_[1]\ : STD_LOGIC;
  signal \cpt_store_s_reg_n_0_[2]\ : STD_LOGIC;
  signal data_en_next : STD_LOGIC;
  signal data_en_s : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal data_en_s_10 : STD_LOGIC;
  signal data_in_en_s : STD_LOGIC;
  signal data_out_en_s : STD_LOGIC;
  signal data_out_en_s_1 : STD_LOGIC;
  signal data_out_en_s_3 : STD_LOGIC;
  signal data_out_en_s_5 : STD_LOGIC;
  signal data_out_en_s_7 : STD_LOGIC;
  signal data_out_en_s_9 : STD_LOGIC;
  signal \data_s[6]_0\ : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \end_macc_s__0\ : STD_LOGIC;
  signal end_next_s : STD_LOGIC_VECTOR ( 0 to 0 );
  signal end_next_s_0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal end_s : STD_LOGIC;
  signal end_s_12 : STD_LOGIC;
  signal end_s_14 : STD_LOGIC;
  signal end_s_16 : STD_LOGIC;
  signal end_s_18 : STD_LOGIC;
  signal end_s_20 : STD_LOGIC;
  signal \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \end_s_reg_gate__0_n_0\ : STD_LOGIC;
  signal \end_s_reg_gate__1_n_0\ : STD_LOGIC;
  signal \end_s_reg_gate__2_n_0\ : STD_LOGIC;
  signal \end_s_reg_gate__3_n_0\ : STD_LOGIC;
  signal \end_s_reg_gate__4_n_0\ : STD_LOGIC;
  signal end_s_reg_gate_n_0 : STD_LOGIC;
  signal end_s_reg_r_0_n_0 : STD_LOGIC;
  signal end_s_reg_r_1_n_0 : STD_LOGIC;
  signal end_s_reg_r_2_n_0 : STD_LOGIC;
  signal end_s_reg_r_n_0 : STD_LOGIC;
  signal final_res_s : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \gen_macc[1].logic_inst_n_10\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_11\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_12\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_13\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_14\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_15\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_16\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_17\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_18\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_19\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_20\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_21\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_3\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_4\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_5\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_6\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_7\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_8\ : STD_LOGIC;
  signal \gen_macc[1].logic_inst_n_9\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_10\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_11\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_12\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_13\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_14\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_15\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_16\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_17\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_18\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_19\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_20\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_21\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_3\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_4\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_5\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_6\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_7\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_8\ : STD_LOGIC;
  signal \gen_macc[2].logic_inst_n_9\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_10\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_11\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_12\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_13\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_14\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_15\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_16\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_17\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_18\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_19\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_2\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_20\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_21\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_22\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_23\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_24\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_3\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_4\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_6\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_7\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_8\ : STD_LOGIC;
  signal \gen_macc[4].logic_inst_n_9\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_10\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_11\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_12\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_13\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_14\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_15\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_16\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_17\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_18\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_19\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_20\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_21\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_3\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_4\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_5\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_6\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_7\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_8\ : STD_LOGIC;
  signal \gen_macc[5].logic_inst_n_9\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_10\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_11\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_12\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_13\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_14\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_15\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_16\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_17\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_18\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_19\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_20\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_21\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_3\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_4\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_5\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_6\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_7\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_8\ : STD_LOGIC;
  signal \gen_macc[6].logic_inst_n_9\ : STD_LOGIC;
  signal mux_cpt_s : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ready_next_s : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ready_next_s_1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal ready_s : STD_LOGIC;
  signal ready_s_11 : STD_LOGIC;
  signal ready_s_13 : STD_LOGIC;
  signal ready_s_15 : STD_LOGIC;
  signal ready_s_17 : STD_LOGIC;
  signal ready_s_19 : STD_LOGIC;
  signal \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\ : STD_LOGIC;
  signal \ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\ : STD_LOGIC;
  signal \ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__0_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__1_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__2_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__3_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__4_n_0\ : STD_LOGIC;
  signal \ready_s_reg_gate__5_n_0\ : STD_LOGIC;
  signal ready_s_reg_gate_n_0 : STD_LOGIC;
  signal res_s0 : STD_LOGIC;
  signal res_s0_0 : STD_LOGIC;
  signal res_s0_2 : STD_LOGIC;
  signal res_s0_4 : STD_LOGIC;
  signal res_s0_6 : STD_LOGIC;
  signal res_s0_8 : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name : string;
  attribute srl_name of \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11] ";
  attribute srl_name of \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16] ";
  attribute srl_name of \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1] ";
  attribute srl_name of \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21] ";
  attribute srl_name of \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26] ";
  attribute srl_name of \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6] ";
  attribute srl_name of \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of coeff_tab_s_reg_gate : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__10\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__11\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__12\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__13\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__14\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__15\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__16\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__17\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__18\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__19\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__20\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__21\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__22\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__23\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__24\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__25\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__26\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__27\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__28\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__29\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__3\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__30\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__31\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__32\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__33\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__34\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__35\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__36\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__37\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__38\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__39\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__40\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__41\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__42\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__43\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__44\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__45\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__46\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__47\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__48\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__49\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__5\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__50\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__51\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__52\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__53\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__54\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__55\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__56\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__57\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__58\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__59\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__6\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__60\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__61\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__62\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__63\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__64\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__65\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__66\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__67\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__68\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__69\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__7\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__70\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__71\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__72\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__73\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__74\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__75\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__76\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__77\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__78\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__79\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__8\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__80\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__81\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__82\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__83\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__84\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__85\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__86\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__87\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__88\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__89\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__9\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__90\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__91\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__92\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__93\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \coeff_tab_s_reg_gate__94\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \cpt_s[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cpt_s[4]_i_1\ : label is "soft_lutpair4";
  attribute srl_bus_name of \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg ";
  attribute srl_name of \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute SOFT_HLUTNM of end_s_reg_gate : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \end_s_reg_gate__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \end_s_reg_gate__1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \end_s_reg_gate__2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \end_s_reg_gate__3\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \end_s_reg_gate__4\ : label is "soft_lutpair7";
  attribute srl_bus_name of \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0\ : label is "\U0/fir_top_inst/ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0 ";
  attribute srl_bus_name of \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute srl_bus_name of \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg ";
  attribute srl_name of \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\ : label is "\U0/fir_top_inst/ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1 ";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__3\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__4\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \ready_s_reg_gate__5\ : label is "soft_lutpair58";
begin
\coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(0),
      Q => \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(10),
      Q => \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(11),
      Q => \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(12),
      Q => \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(13),
      Q => \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(14),
      Q => \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(15),
      Q => \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(1),
      Q => \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(2),
      Q => \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(3),
      Q => \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(4),
      Q => \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(5),
      Q => \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(6),
      Q => \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(7),
      Q => \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(8),
      Q => \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[8]\(9),
      Q => \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[11][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[13][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__46_n_0\,
      Q => \coeff_tab_s_reg[13]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__36_n_0\,
      Q => \coeff_tab_s_reg[13]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__35_n_0\,
      Q => \coeff_tab_s_reg[13]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__34_n_0\,
      Q => \coeff_tab_s_reg[13]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__33_n_0\,
      Q => \coeff_tab_s_reg[13]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__32_n_0\,
      Q => \coeff_tab_s_reg[13]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__31_n_0\,
      Q => \coeff_tab_s_reg[13]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__45_n_0\,
      Q => \coeff_tab_s_reg[13]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__44_n_0\,
      Q => \coeff_tab_s_reg[13]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__43_n_0\,
      Q => \coeff_tab_s_reg[13]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__42_n_0\,
      Q => \coeff_tab_s_reg[13]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__41_n_0\,
      Q => \coeff_tab_s_reg[13]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__40_n_0\,
      Q => \coeff_tab_s_reg[13]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__39_n_0\,
      Q => \coeff_tab_s_reg[13]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__38_n_0\,
      Q => \coeff_tab_s_reg[13]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[13][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__37_n_0\,
      Q => \coeff_tab_s_reg[13]\(9),
      R => data_rst_i
    );
\coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(0),
      Q => \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(10),
      Q => \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(11),
      Q => \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(12),
      Q => \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(13),
      Q => \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(14),
      Q => \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(15),
      Q => \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(1),
      Q => \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(2),
      Q => \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(3),
      Q => \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(4),
      Q => \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(5),
      Q => \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(6),
      Q => \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(7),
      Q => \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(8),
      Q => \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[13]\(9),
      Q => \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[16][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[18][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__62_n_0\,
      Q => \coeff_tab_s_reg[18]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__52_n_0\,
      Q => \coeff_tab_s_reg[18]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__51_n_0\,
      Q => \coeff_tab_s_reg[18]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__50_n_0\,
      Q => \coeff_tab_s_reg[18]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__49_n_0\,
      Q => \coeff_tab_s_reg[18]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__48_n_0\,
      Q => \coeff_tab_s_reg[18]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__47_n_0\,
      Q => \coeff_tab_s_reg[18]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__61_n_0\,
      Q => \coeff_tab_s_reg[18]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__60_n_0\,
      Q => \coeff_tab_s_reg[18]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__59_n_0\,
      Q => \coeff_tab_s_reg[18]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__58_n_0\,
      Q => \coeff_tab_s_reg[18]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__57_n_0\,
      Q => \coeff_tab_s_reg[18]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__56_n_0\,
      Q => \coeff_tab_s_reg[18]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__55_n_0\,
      Q => \coeff_tab_s_reg[18]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__54_n_0\,
      Q => \coeff_tab_s_reg[18]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[18][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__53_n_0\,
      Q => \coeff_tab_s_reg[18]\(9),
      R => data_rst_i
    );
\coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(0),
      Q => \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(10),
      Q => \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(11),
      Q => \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(12),
      Q => \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(13),
      Q => \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(14),
      Q => \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(15),
      Q => \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(1),
      Q => \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(2),
      Q => \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(3),
      Q => \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(4),
      Q => \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(5),
      Q => \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(6),
      Q => \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(7),
      Q => \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(8),
      Q => \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => coeff_s(9),
      Q => \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(0),
      Q => \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(10),
      Q => \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(11),
      Q => \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(12),
      Q => \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(13),
      Q => \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(14),
      Q => \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(15),
      Q => \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(1),
      Q => \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(2),
      Q => \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(3),
      Q => \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(4),
      Q => \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(5),
      Q => \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(6),
      Q => \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(7),
      Q => \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(8),
      Q => \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[18]\(9),
      Q => \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[21][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[23][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__78_n_0\,
      Q => \coeff_tab_s_reg[23]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__68_n_0\,
      Q => \coeff_tab_s_reg[23]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__67_n_0\,
      Q => \coeff_tab_s_reg[23]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__66_n_0\,
      Q => \coeff_tab_s_reg[23]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__65_n_0\,
      Q => \coeff_tab_s_reg[23]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__64_n_0\,
      Q => \coeff_tab_s_reg[23]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__63_n_0\,
      Q => \coeff_tab_s_reg[23]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__77_n_0\,
      Q => \coeff_tab_s_reg[23]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__76_n_0\,
      Q => \coeff_tab_s_reg[23]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__75_n_0\,
      Q => \coeff_tab_s_reg[23]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__74_n_0\,
      Q => \coeff_tab_s_reg[23]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__73_n_0\,
      Q => \coeff_tab_s_reg[23]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__72_n_0\,
      Q => \coeff_tab_s_reg[23]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__71_n_0\,
      Q => \coeff_tab_s_reg[23]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__70_n_0\,
      Q => \coeff_tab_s_reg[23]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[23][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__69_n_0\,
      Q => \coeff_tab_s_reg[23]\(9),
      R => data_rst_i
    );
\coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(0),
      Q => \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(10),
      Q => \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(11),
      Q => \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(12),
      Q => \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(13),
      Q => \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(14),
      Q => \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(15),
      Q => \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(1),
      Q => \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(2),
      Q => \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(3),
      Q => \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(4),
      Q => \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(5),
      Q => \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(6),
      Q => \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(7),
      Q => \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(8),
      Q => \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[23]\(9),
      Q => \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[26][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[28][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__94_n_0\,
      Q => \coeff_tab_s_reg[28]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__84_n_0\,
      Q => \coeff_tab_s_reg[28]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__83_n_0\,
      Q => \coeff_tab_s_reg[28]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__82_n_0\,
      Q => \coeff_tab_s_reg[28]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__81_n_0\,
      Q => \coeff_tab_s_reg[28]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__80_n_0\,
      Q => \coeff_tab_s_reg[28]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__79_n_0\,
      Q => \coeff_tab_s_reg[28]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__93_n_0\,
      Q => \coeff_tab_s_reg[28]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__92_n_0\,
      Q => \coeff_tab_s_reg[28]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__91_n_0\,
      Q => \coeff_tab_s_reg[28]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__90_n_0\,
      Q => \coeff_tab_s_reg[28]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__89_n_0\,
      Q => \coeff_tab_s_reg[28]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__88_n_0\,
      Q => \coeff_tab_s_reg[28]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__87_n_0\,
      Q => \coeff_tab_s_reg[28]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__86_n_0\,
      Q => \coeff_tab_s_reg[28]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[28][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__85_n_0\,
      Q => \coeff_tab_s_reg[28]\(9),
      R => data_rst_i
    );
\coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][0]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][10]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][11]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][12]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][13]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][14]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][15]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][1]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][2]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][3]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][4]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][5]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][6]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][7]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][8]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[1][9]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[3][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__14_n_0\,
      Q => \coeff_tab_s_reg[3]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__4_n_0\,
      Q => \coeff_tab_s_reg[3]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__3_n_0\,
      Q => \coeff_tab_s_reg[3]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__2_n_0\,
      Q => \coeff_tab_s_reg[3]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__1_n_0\,
      Q => \coeff_tab_s_reg[3]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__0_n_0\,
      Q => \coeff_tab_s_reg[3]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => coeff_tab_s_reg_gate_n_0,
      Q => \coeff_tab_s_reg[3]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__13_n_0\,
      Q => \coeff_tab_s_reg[3]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__12_n_0\,
      Q => \coeff_tab_s_reg[3]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__11_n_0\,
      Q => \coeff_tab_s_reg[3]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__10_n_0\,
      Q => \coeff_tab_s_reg[3]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__9_n_0\,
      Q => \coeff_tab_s_reg[3]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__8_n_0\,
      Q => \coeff_tab_s_reg[3]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__7_n_0\,
      Q => \coeff_tab_s_reg[3]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__6_n_0\,
      Q => \coeff_tab_s_reg[3]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[3][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__5_n_0\,
      Q => \coeff_tab_s_reg[3]\(9),
      R => data_rst_i
    );
\coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(0),
      Q => \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(10),
      Q => \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(11),
      Q => \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(12),
      Q => \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(13),
      Q => \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(14),
      Q => \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(15),
      Q => \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(1),
      Q => \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(2),
      Q => \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(3),
      Q => \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(4),
      Q => \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(5),
      Q => \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(6),
      Q => \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(7),
      Q => \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(8),
      Q => \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => \coeff_tab_s_reg[3]\(9),
      Q => \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][0]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][10]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][11]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][12]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][14]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][15]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][1]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][2]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][4]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][5]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][6]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][7]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg[6][9]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\coeff_tab_s_reg[8][0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__30_n_0\,
      Q => \coeff_tab_s_reg[8]\(0),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__20_n_0\,
      Q => \coeff_tab_s_reg[8]\(10),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__19_n_0\,
      Q => \coeff_tab_s_reg[8]\(11),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__18_n_0\,
      Q => \coeff_tab_s_reg[8]\(12),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__17_n_0\,
      Q => \coeff_tab_s_reg[8]\(13),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__16_n_0\,
      Q => \coeff_tab_s_reg[8]\(14),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__15_n_0\,
      Q => \coeff_tab_s_reg[8]\(15),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__29_n_0\,
      Q => \coeff_tab_s_reg[8]\(1),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__28_n_0\,
      Q => \coeff_tab_s_reg[8]\(2),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__27_n_0\,
      Q => \coeff_tab_s_reg[8]\(3),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__26_n_0\,
      Q => \coeff_tab_s_reg[8]\(4),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__25_n_0\,
      Q => \coeff_tab_s_reg[8]\(5),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__24_n_0\,
      Q => \coeff_tab_s_reg[8]\(6),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__23_n_0\,
      Q => \coeff_tab_s_reg[8]\(7),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__22_n_0\,
      Q => \coeff_tab_s_reg[8]\(8),
      R => data_rst_i
    );
\coeff_tab_s_reg[8][9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \coeff_tab_s_reg_gate__21_n_0\,
      Q => \coeff_tab_s_reg[8]\(9),
      R => data_rst_i
    );
coeff_tab_s_reg_gate: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][15]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => coeff_tab_s_reg_gate_n_0
    );
\coeff_tab_s_reg_gate__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][14]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__0_n_0\
    );
\coeff_tab_s_reg_gate__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][13]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__1_n_0\
    );
\coeff_tab_s_reg_gate__10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][4]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__10_n_0\
    );
\coeff_tab_s_reg_gate__11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][3]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__11_n_0\
    );
\coeff_tab_s_reg_gate__12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][2]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__12_n_0\
    );
\coeff_tab_s_reg_gate__13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][1]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__13_n_0\
    );
\coeff_tab_s_reg_gate__14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][0]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__14_n_0\
    );
\coeff_tab_s_reg_gate__15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__15_n_0\
    );
\coeff_tab_s_reg_gate__16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__16_n_0\
    );
\coeff_tab_s_reg_gate__17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__17_n_0\
    );
\coeff_tab_s_reg_gate__18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__18_n_0\
    );
\coeff_tab_s_reg_gate__19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__19_n_0\
    );
\coeff_tab_s_reg_gate__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][12]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__2_n_0\
    );
\coeff_tab_s_reg_gate__20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__20_n_0\
    );
\coeff_tab_s_reg_gate__21\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__21_n_0\
    );
\coeff_tab_s_reg_gate__22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__22_n_0\
    );
\coeff_tab_s_reg_gate__23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__23_n_0\
    );
\coeff_tab_s_reg_gate__24\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__24_n_0\
    );
\coeff_tab_s_reg_gate__25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__25_n_0\
    );
\coeff_tab_s_reg_gate__26\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__26_n_0\
    );
\coeff_tab_s_reg_gate__27\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__27_n_0\
    );
\coeff_tab_s_reg_gate__28\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__28_n_0\
    );
\coeff_tab_s_reg_gate__29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__29_n_0\
    );
\coeff_tab_s_reg_gate__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][11]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__3_n_0\
    );
\coeff_tab_s_reg_gate__30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[7][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__30_n_0\
    );
\coeff_tab_s_reg_gate__31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__31_n_0\
    );
\coeff_tab_s_reg_gate__32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__32_n_0\
    );
\coeff_tab_s_reg_gate__33\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__33_n_0\
    );
\coeff_tab_s_reg_gate__34\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__34_n_0\
    );
\coeff_tab_s_reg_gate__35\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__35_n_0\
    );
\coeff_tab_s_reg_gate__36\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__36_n_0\
    );
\coeff_tab_s_reg_gate__37\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__37_n_0\
    );
\coeff_tab_s_reg_gate__38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__38_n_0\
    );
\coeff_tab_s_reg_gate__39\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__39_n_0\
    );
\coeff_tab_s_reg_gate__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][10]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__4_n_0\
    );
\coeff_tab_s_reg_gate__40\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__40_n_0\
    );
\coeff_tab_s_reg_gate__41\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__41_n_0\
    );
\coeff_tab_s_reg_gate__42\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__42_n_0\
    );
\coeff_tab_s_reg_gate__43\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__43_n_0\
    );
\coeff_tab_s_reg_gate__44\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__44_n_0\
    );
\coeff_tab_s_reg_gate__45\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__45_n_0\
    );
\coeff_tab_s_reg_gate__46\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[12][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__46_n_0\
    );
\coeff_tab_s_reg_gate__47\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__47_n_0\
    );
\coeff_tab_s_reg_gate__48\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__48_n_0\
    );
\coeff_tab_s_reg_gate__49\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__49_n_0\
    );
\coeff_tab_s_reg_gate__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][9]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__5_n_0\
    );
\coeff_tab_s_reg_gate__50\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__50_n_0\
    );
\coeff_tab_s_reg_gate__51\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__51_n_0\
    );
\coeff_tab_s_reg_gate__52\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__52_n_0\
    );
\coeff_tab_s_reg_gate__53\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__53_n_0\
    );
\coeff_tab_s_reg_gate__54\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__54_n_0\
    );
\coeff_tab_s_reg_gate__55\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__55_n_0\
    );
\coeff_tab_s_reg_gate__56\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__56_n_0\
    );
\coeff_tab_s_reg_gate__57\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__57_n_0\
    );
\coeff_tab_s_reg_gate__58\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__58_n_0\
    );
\coeff_tab_s_reg_gate__59\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__59_n_0\
    );
\coeff_tab_s_reg_gate__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][8]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__6_n_0\
    );
\coeff_tab_s_reg_gate__60\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__60_n_0\
    );
\coeff_tab_s_reg_gate__61\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__61_n_0\
    );
\coeff_tab_s_reg_gate__62\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[17][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__62_n_0\
    );
\coeff_tab_s_reg_gate__63\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__63_n_0\
    );
\coeff_tab_s_reg_gate__64\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__64_n_0\
    );
\coeff_tab_s_reg_gate__65\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__65_n_0\
    );
\coeff_tab_s_reg_gate__66\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__66_n_0\
    );
\coeff_tab_s_reg_gate__67\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__67_n_0\
    );
\coeff_tab_s_reg_gate__68\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__68_n_0\
    );
\coeff_tab_s_reg_gate__69\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__69_n_0\
    );
\coeff_tab_s_reg_gate__7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][7]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__7_n_0\
    );
\coeff_tab_s_reg_gate__70\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__70_n_0\
    );
\coeff_tab_s_reg_gate__71\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__71_n_0\
    );
\coeff_tab_s_reg_gate__72\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__72_n_0\
    );
\coeff_tab_s_reg_gate__73\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__73_n_0\
    );
\coeff_tab_s_reg_gate__74\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__74_n_0\
    );
\coeff_tab_s_reg_gate__75\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__75_n_0\
    );
\coeff_tab_s_reg_gate__76\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__76_n_0\
    );
\coeff_tab_s_reg_gate__77\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__77_n_0\
    );
\coeff_tab_s_reg_gate__78\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[22][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__78_n_0\
    );
\coeff_tab_s_reg_gate__79\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][15]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__79_n_0\
    );
\coeff_tab_s_reg_gate__8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][6]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__8_n_0\
    );
\coeff_tab_s_reg_gate__80\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__80_n_0\
    );
\coeff_tab_s_reg_gate__81\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][13]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__81_n_0\
    );
\coeff_tab_s_reg_gate__82\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][12]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__82_n_0\
    );
\coeff_tab_s_reg_gate__83\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][11]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__83_n_0\
    );
\coeff_tab_s_reg_gate__84\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][10]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__84_n_0\
    );
\coeff_tab_s_reg_gate__85\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__85_n_0\
    );
\coeff_tab_s_reg_gate__86\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][8]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__86_n_0\
    );
\coeff_tab_s_reg_gate__87\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][7]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__87_n_0\
    );
\coeff_tab_s_reg_gate__88\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][6]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__88_n_0\
    );
\coeff_tab_s_reg_gate__89\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][5]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__89_n_0\
    );
\coeff_tab_s_reg_gate__9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[2][5]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => \coeff_tab_s_reg_gate__9_n_0\
    );
\coeff_tab_s_reg_gate__90\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__90_n_0\
    );
\coeff_tab_s_reg_gate__91\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][3]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__91_n_0\
    );
\coeff_tab_s_reg_gate__92\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][2]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__92_n_0\
    );
\coeff_tab_s_reg_gate__93\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][1]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__93_n_0\
    );
\coeff_tab_s_reg_gate__94\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \coeff_tab_s_reg[27][0]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \coeff_tab_s_reg_gate__94_n_0\
    );
\cpt_s[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0E0F0F0F0F0F0F"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[4]\,
      I2 => \cpt_s_reg_n_0_[0]\,
      I3 => \cpt_s_reg_n_0_[3]\,
      I4 => \cpt_s_reg_n_0_[5]\,
      I5 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(0)
    );
\cpt_s[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0E0F0FF0F0F0F0"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[4]\,
      I2 => \cpt_s_reg_n_0_[0]\,
      I3 => \cpt_s_reg_n_0_[3]\,
      I4 => \cpt_s_reg_n_0_[5]\,
      I5 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(1)
    );
\cpt_s[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[0]\,
      I2 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(2)
    );
\cpt_s[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"78F0"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[0]\,
      I2 => \cpt_s_reg_n_0_[3]\,
      I3 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(3)
    );
\cpt_s[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6CCCCCCC"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[4]\,
      I2 => \cpt_s_reg_n_0_[0]\,
      I3 => \cpt_s_reg_n_0_[3]\,
      I4 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(4)
    );
\cpt_s[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFE8000FFFF0000"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[2]\,
      I1 => \cpt_s_reg_n_0_[4]\,
      I2 => \cpt_s_reg_n_0_[0]\,
      I3 => \cpt_s_reg_n_0_[3]\,
      I4 => \cpt_s_reg_n_0_[5]\,
      I5 => \cpt_s_reg_n_0_[1]\,
      O => mux_cpt_s(5)
    );
\cpt_s_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(0),
      Q => \cpt_s_reg_n_0_[0]\,
      R => data_rst_i
    );
\cpt_s_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(1),
      Q => \cpt_s_reg_n_0_[1]\,
      R => data_rst_i
    );
\cpt_s_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(2),
      Q => \cpt_s_reg_n_0_[2]\,
      R => data_rst_i
    );
\cpt_s_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(3),
      Q => \cpt_s_reg_n_0_[3]\,
      R => data_rst_i
    );
\cpt_s_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(4),
      Q => \cpt_s_reg_n_0_[4]\,
      R => data_rst_i
    );
\cpt_s_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => data_en_i,
      D => mux_cpt_s(5),
      Q => \cpt_s_reg_n_0_[5]\,
      R => data_rst_i
    );
\cpt_store_s_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => '1',
      D => \gen_macc[4].logic_inst_n_2\,
      Q => \cpt_store_s_reg_n_0_[0]\,
      R => '0'
    );
\cpt_store_s_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => '1',
      D => \gen_macc[4].logic_inst_n_3\,
      Q => \cpt_store_s_reg_n_0_[1]\,
      R => '0'
    );
\cpt_store_s_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_clk_i,
      CE => '1',
      D => \gen_macc[4].logic_inst_n_4\,
      Q => \cpt_store_s_reg_n_0_[2]\,
      R => '0'
    );
data_en_o_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => data_en_next,
      Q => tick_o,
      R => data_rst_i
    );
data_in_en_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => data_en_i,
      Q => data_in_en_s,
      R => data_rst_i
    );
\data_out_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(0),
      Q => data_o(0),
      R => data_rst_i
    );
\data_out_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(10),
      Q => data_o(10),
      R => data_rst_i
    );
\data_out_s_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(11),
      Q => data_o(11),
      R => data_rst_i
    );
\data_out_s_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(12),
      Q => data_o(12),
      R => data_rst_i
    );
\data_out_s_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(13),
      Q => data_o(13),
      R => data_rst_i
    );
\data_out_s_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(14),
      Q => data_o(14),
      R => data_rst_i
    );
\data_out_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(15),
      Q => data_o(15),
      R => data_rst_i
    );
\data_out_s_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(16),
      Q => data_o(16),
      R => data_rst_i
    );
\data_out_s_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(17),
      Q => data_o(17),
      R => data_rst_i
    );
\data_out_s_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(18),
      Q => data_o(18),
      R => data_rst_i
    );
\data_out_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(1),
      Q => data_o(1),
      R => data_rst_i
    );
\data_out_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(2),
      Q => data_o(2),
      R => data_rst_i
    );
\data_out_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(3),
      Q => data_o(3),
      R => data_rst_i
    );
\data_out_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(4),
      Q => data_o(4),
      R => data_rst_i
    );
\data_out_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(5),
      Q => data_o(5),
      R => data_rst_i
    );
\data_out_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(6),
      Q => data_o(6),
      R => data_rst_i
    );
\data_out_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(7),
      Q => data_o(7),
      R => data_rst_i
    );
\data_out_s_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(8),
      Q => data_o(8),
      R => data_rst_i
    );
\data_out_s_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_en_next,
      D => \data_s[6]_0\(9),
      Q => data_o(9),
      R => data_rst_i
    );
end_delay_macc_s_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => '1',
      D => \end_macc_s__0\,
      Q => end_next_s(0),
      R => data_rst_i
    );
end_macc_s: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => \cpt_s_reg_n_0_[5]\,
      I1 => \cpt_s_reg_n_0_[3]\,
      I2 => \cpt_s_reg_n_0_[4]\,
      I3 => \cpt_s_reg_n_0_[2]\,
      I4 => \cpt_s_reg_n_0_[0]\,
      I5 => \cpt_s_reg_n_0_[1]\,
      O => \end_macc_s__0\
    );
\end_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => end_next_s(0),
      Q => end_next_s_0(1),
      R => data_rst_i
    );
\end_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg_gate__3_n_0\,
      Q => end_next_s_0(11),
      R => data_rst_i
    );
\end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(11),
      Q => \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\end_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg_gate__2_n_0\,
      Q => end_next_s_0(16),
      R => data_rst_i
    );
\end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(16),
      Q => \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\end_s_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg_gate__1_n_0\,
      Q => end_next_s_0(21),
      R => data_rst_i
    );
\end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(21),
      Q => \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\end_s_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg_gate__0_n_0\,
      Q => end_next_s_0(26),
      R => data_rst_i
    );
\end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(26),
      Q => \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\end_s_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => end_s_reg_gate_n_0,
      Q => end_next_s_0(31),
      R => data_rst_i
    );
\end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(1),
      Q => \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\end_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg_gate__4_n_0\,
      Q => end_next_s_0(6),
      R => data_rst_i
    );
\end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => end_next_s_0(6),
      Q => \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \end_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
end_s_reg_gate: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => end_s_reg_gate_n_0
    );
\end_s_reg_gate__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \end_s_reg_gate__0_n_0\
    );
\end_s_reg_gate__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \end_s_reg_gate__1_n_0\
    );
\end_s_reg_gate__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \end_s_reg_gate__2_n_0\
    );
\end_s_reg_gate__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \end_s_reg_gate__3_n_0\
    );
\end_s_reg_gate__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \end_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \end_s_reg_gate__4_n_0\
    );
end_s_reg_r: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => '1',
      Q => end_s_reg_r_n_0,
      R => data_rst_i
    );
end_s_reg_r_0: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => end_s_reg_r_n_0,
      Q => end_s_reg_r_0_n_0,
      R => data_rst_i
    );
end_s_reg_r_1: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => end_s_reg_r_0_n_0,
      Q => end_s_reg_r_1_n_0,
      R => data_rst_i
    );
end_s_reg_r_2: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => end_s_reg_r_1_n_0,
      Q => end_s_reg_r_2_n_0,
      R => data_rst_i
    );
\gen_macc[0].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc
     port map (
      DOBDO(15 downto 0) => coeff_s(15 downto 0),
      E(0) => data_out_en_s_9,
      Q(18 downto 0) => final_res_s(18 downto 0),
      data_clk_i => data_clk_i,
      data_en_s(0) => data_en_s(0),
      data_en_s_0 => data_en_s_10,
      data_en_s_reg_0(0) => data_out_en_s_7,
      data_en_s_reg_1(0) => data_out_en_s_5,
      data_en_s_reg_2(0) => data_out_en_s_3,
      data_en_s_reg_3(0) => data_out_en_s_1,
      data_en_s_reg_4(0) => data_out_en_s,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(1),
      end_s => end_s,
      end_s_10 => end_s_16,
      end_s_12 => end_s_18,
      end_s_14 => end_s_20,
      end_s_6 => end_s_12,
      end_s_8 => end_s_14,
      ready_next_s_1(0) => ready_next_s_1(1),
      ready_s => ready_s,
      ready_s_11 => ready_s_15,
      ready_s_13 => ready_s_17,
      ready_s_15 => ready_s_19,
      ready_s_7 => ready_s_11,
      ready_s_9 => ready_s_13,
      res_s0 => res_s0_8,
      res_s0_1 => res_s0_6,
      res_s0_2 => res_s0_4,
      res_s0_3 => res_s0_2,
      res_s0_4 => res_s0_0,
      res_s0_5 => res_s0
    );
\gen_macc[1].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_0
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[3]\(15 downto 0),
      E(0) => data_out_en_s_9,
      Q(18) => \gen_macc[1].logic_inst_n_3\,
      Q(17) => \gen_macc[1].logic_inst_n_4\,
      Q(16) => \gen_macc[1].logic_inst_n_5\,
      Q(15) => \gen_macc[1].logic_inst_n_6\,
      Q(14) => \gen_macc[1].logic_inst_n_7\,
      Q(13) => \gen_macc[1].logic_inst_n_8\,
      Q(12) => \gen_macc[1].logic_inst_n_9\,
      Q(11) => \gen_macc[1].logic_inst_n_10\,
      Q(10) => \gen_macc[1].logic_inst_n_11\,
      Q(9) => \gen_macc[1].logic_inst_n_12\,
      Q(8) => \gen_macc[1].logic_inst_n_13\,
      Q(7) => \gen_macc[1].logic_inst_n_14\,
      Q(6) => \gen_macc[1].logic_inst_n_15\,
      Q(5) => \gen_macc[1].logic_inst_n_16\,
      Q(4) => \gen_macc[1].logic_inst_n_17\,
      Q(3) => \gen_macc[1].logic_inst_n_18\,
      Q(2) => \gen_macc[1].logic_inst_n_19\,
      Q(1) => \gen_macc[1].logic_inst_n_20\,
      Q(0) => \gen_macc[1].logic_inst_n_21\,
      data_clk_i => data_clk_i,
      data_en_s(0) => data_en_s(1),
      data_en_s_0 => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(6),
      end_s => end_s,
      ready_next_s_1(0) => ready_next_s_1(6),
      ready_s => ready_s,
      res_s0 => res_s0_8
    );
\gen_macc[2].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_1
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[8]\(15 downto 0),
      E(0) => data_out_en_s_7,
      Q(18) => \gen_macc[2].logic_inst_n_3\,
      Q(17) => \gen_macc[2].logic_inst_n_4\,
      Q(16) => \gen_macc[2].logic_inst_n_5\,
      Q(15) => \gen_macc[2].logic_inst_n_6\,
      Q(14) => \gen_macc[2].logic_inst_n_7\,
      Q(13) => \gen_macc[2].logic_inst_n_8\,
      Q(12) => \gen_macc[2].logic_inst_n_9\,
      Q(11) => \gen_macc[2].logic_inst_n_10\,
      Q(10) => \gen_macc[2].logic_inst_n_11\,
      Q(9) => \gen_macc[2].logic_inst_n_12\,
      Q(8) => \gen_macc[2].logic_inst_n_13\,
      Q(7) => \gen_macc[2].logic_inst_n_14\,
      Q(6) => \gen_macc[2].logic_inst_n_15\,
      Q(5) => \gen_macc[2].logic_inst_n_16\,
      Q(4) => \gen_macc[2].logic_inst_n_17\,
      Q(3) => \gen_macc[2].logic_inst_n_18\,
      Q(2) => \gen_macc[2].logic_inst_n_19\,
      Q(1) => \gen_macc[2].logic_inst_n_20\,
      Q(0) => \gen_macc[2].logic_inst_n_21\,
      data_clk_i => data_clk_i,
      data_en_s(0) => data_en_s(2),
      data_en_s_0 => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(11),
      end_s => end_s_12,
      ready_next_s_1(0) => ready_next_s_1(11),
      ready_s => ready_s_11,
      res_s0 => res_s0_6
    );
\gen_macc[3].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_2
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[13]\(15 downto 0),
      D(18 downto 0) => \data_s[6]_0\(18 downto 0),
      E(0) => data_out_en_s_5,
      Q(18) => \gen_macc[2].logic_inst_n_3\,
      Q(17) => \gen_macc[2].logic_inst_n_4\,
      Q(16) => \gen_macc[2].logic_inst_n_5\,
      Q(15) => \gen_macc[2].logic_inst_n_6\,
      Q(14) => \gen_macc[2].logic_inst_n_7\,
      Q(13) => \gen_macc[2].logic_inst_n_8\,
      Q(12) => \gen_macc[2].logic_inst_n_9\,
      Q(11) => \gen_macc[2].logic_inst_n_10\,
      Q(10) => \gen_macc[2].logic_inst_n_11\,
      Q(9) => \gen_macc[2].logic_inst_n_12\,
      Q(8) => \gen_macc[2].logic_inst_n_13\,
      Q(7) => \gen_macc[2].logic_inst_n_14\,
      Q(6) => \gen_macc[2].logic_inst_n_15\,
      Q(5) => \gen_macc[2].logic_inst_n_16\,
      Q(4) => \gen_macc[2].logic_inst_n_17\,
      Q(3) => \gen_macc[2].logic_inst_n_18\,
      Q(2) => \gen_macc[2].logic_inst_n_19\,
      Q(1) => \gen_macc[2].logic_inst_n_20\,
      Q(0) => \gen_macc[2].logic_inst_n_21\,
      data_clk_i => data_clk_i,
      data_en_s(0) => data_en_s(3),
      data_en_s_0 => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      \data_out_s_reg[0]\ => \gen_macc[6].logic_inst_n_21\,
      \data_out_s_reg[10]\ => \gen_macc[6].logic_inst_n_11\,
      \data_out_s_reg[11]\ => \gen_macc[6].logic_inst_n_10\,
      \data_out_s_reg[12]\ => \gen_macc[6].logic_inst_n_9\,
      \data_out_s_reg[13]\ => \gen_macc[6].logic_inst_n_8\,
      \data_out_s_reg[14]\ => \gen_macc[6].logic_inst_n_7\,
      \data_out_s_reg[15]\ => \gen_macc[6].logic_inst_n_6\,
      \data_out_s_reg[16]\ => \gen_macc[6].logic_inst_n_5\,
      \data_out_s_reg[17]\ => \gen_macc[6].logic_inst_n_4\,
      \data_out_s_reg[18]\ => \cpt_store_s_reg_n_0_[2]\,
      \data_out_s_reg[18]_0\ => \gen_macc[6].logic_inst_n_3\,
      \data_out_s_reg[18]_1\ => \cpt_store_s_reg_n_0_[1]\,
      \data_out_s_reg[18]_2\(18) => \gen_macc[1].logic_inst_n_3\,
      \data_out_s_reg[18]_2\(17) => \gen_macc[1].logic_inst_n_4\,
      \data_out_s_reg[18]_2\(16) => \gen_macc[1].logic_inst_n_5\,
      \data_out_s_reg[18]_2\(15) => \gen_macc[1].logic_inst_n_6\,
      \data_out_s_reg[18]_2\(14) => \gen_macc[1].logic_inst_n_7\,
      \data_out_s_reg[18]_2\(13) => \gen_macc[1].logic_inst_n_8\,
      \data_out_s_reg[18]_2\(12) => \gen_macc[1].logic_inst_n_9\,
      \data_out_s_reg[18]_2\(11) => \gen_macc[1].logic_inst_n_10\,
      \data_out_s_reg[18]_2\(10) => \gen_macc[1].logic_inst_n_11\,
      \data_out_s_reg[18]_2\(9) => \gen_macc[1].logic_inst_n_12\,
      \data_out_s_reg[18]_2\(8) => \gen_macc[1].logic_inst_n_13\,
      \data_out_s_reg[18]_2\(7) => \gen_macc[1].logic_inst_n_14\,
      \data_out_s_reg[18]_2\(6) => \gen_macc[1].logic_inst_n_15\,
      \data_out_s_reg[18]_2\(5) => \gen_macc[1].logic_inst_n_16\,
      \data_out_s_reg[18]_2\(4) => \gen_macc[1].logic_inst_n_17\,
      \data_out_s_reg[18]_2\(3) => \gen_macc[1].logic_inst_n_18\,
      \data_out_s_reg[18]_2\(2) => \gen_macc[1].logic_inst_n_19\,
      \data_out_s_reg[18]_2\(1) => \gen_macc[1].logic_inst_n_20\,
      \data_out_s_reg[18]_2\(0) => \gen_macc[1].logic_inst_n_21\,
      \data_out_s_reg[18]_3\ => \cpt_store_s_reg_n_0_[0]\,
      \data_out_s_reg[18]_4\(18 downto 0) => final_res_s(18 downto 0),
      \data_out_s_reg[1]\ => \gen_macc[6].logic_inst_n_20\,
      \data_out_s_reg[2]\ => \gen_macc[6].logic_inst_n_19\,
      \data_out_s_reg[3]\ => \gen_macc[6].logic_inst_n_18\,
      \data_out_s_reg[4]\ => \gen_macc[6].logic_inst_n_17\,
      \data_out_s_reg[5]\ => \gen_macc[6].logic_inst_n_16\,
      \data_out_s_reg[6]\ => \gen_macc[6].logic_inst_n_15\,
      \data_out_s_reg[7]\ => \gen_macc[6].logic_inst_n_14\,
      \data_out_s_reg[8]\ => \gen_macc[6].logic_inst_n_13\,
      \data_out_s_reg[9]\ => \gen_macc[6].logic_inst_n_12\,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(16),
      end_s => end_s_14,
      ready_next_s_1(0) => ready_next_s_1(16),
      ready_s => ready_s_13,
      res_s0 => res_s0_4
    );
\gen_macc[4].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_3
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[18]\(15 downto 0),
      E(0) => data_out_en_s_3,
      Q(18) => \gen_macc[4].logic_inst_n_6\,
      Q(17) => \gen_macc[4].logic_inst_n_7\,
      Q(16) => \gen_macc[4].logic_inst_n_8\,
      Q(15) => \gen_macc[4].logic_inst_n_9\,
      Q(14) => \gen_macc[4].logic_inst_n_10\,
      Q(13) => \gen_macc[4].logic_inst_n_11\,
      Q(12) => \gen_macc[4].logic_inst_n_12\,
      Q(11) => \gen_macc[4].logic_inst_n_13\,
      Q(10) => \gen_macc[4].logic_inst_n_14\,
      Q(9) => \gen_macc[4].logic_inst_n_15\,
      Q(8) => \gen_macc[4].logic_inst_n_16\,
      Q(7) => \gen_macc[4].logic_inst_n_17\,
      Q(6) => \gen_macc[4].logic_inst_n_18\,
      Q(5) => \gen_macc[4].logic_inst_n_19\,
      Q(4) => \gen_macc[4].logic_inst_n_20\,
      Q(3) => \gen_macc[4].logic_inst_n_21\,
      Q(2) => \gen_macc[4].logic_inst_n_22\,
      Q(1) => \gen_macc[4].logic_inst_n_23\,
      Q(0) => \gen_macc[4].logic_inst_n_24\,
      \cpt_store_s_reg[0]\ => \cpt_store_s_reg_n_0_[1]\,
      \cpt_store_s_reg[0]_0\ => \cpt_store_s_reg_n_0_[0]\,
      \cpt_store_s_reg[0]_1\ => \cpt_store_s_reg_n_0_[2]\,
      \cpt_store_s_reg[2]\(5 downto 4) => data_en_s(6 downto 5),
      \cpt_store_s_reg[2]\(3 downto 0) => data_en_s(3 downto 0),
      data_clk_i => data_clk_i,
      data_en_o_reg_0 => \gen_macc[4].logic_inst_n_2\,
      data_en_o_reg_1 => \gen_macc[4].logic_inst_n_3\,
      data_en_o_reg_2 => \gen_macc[4].logic_inst_n_4\,
      data_en_o_reg_3(0) => data_en_next,
      data_en_s => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(21),
      end_s => end_s_16,
      ready_next_s_1(0) => ready_next_s_1(21),
      ready_s => ready_s_15,
      res_s0 => res_s0_2
    );
\gen_macc[5].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_4
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[23]\(15 downto 0),
      E(0) => data_out_en_s_1,
      Q(18) => \gen_macc[5].logic_inst_n_3\,
      Q(17) => \gen_macc[5].logic_inst_n_4\,
      Q(16) => \gen_macc[5].logic_inst_n_5\,
      Q(15) => \gen_macc[5].logic_inst_n_6\,
      Q(14) => \gen_macc[5].logic_inst_n_7\,
      Q(13) => \gen_macc[5].logic_inst_n_8\,
      Q(12) => \gen_macc[5].logic_inst_n_9\,
      Q(11) => \gen_macc[5].logic_inst_n_10\,
      Q(10) => \gen_macc[5].logic_inst_n_11\,
      Q(9) => \gen_macc[5].logic_inst_n_12\,
      Q(8) => \gen_macc[5].logic_inst_n_13\,
      Q(7) => \gen_macc[5].logic_inst_n_14\,
      Q(6) => \gen_macc[5].logic_inst_n_15\,
      Q(5) => \gen_macc[5].logic_inst_n_16\,
      Q(4) => \gen_macc[5].logic_inst_n_17\,
      Q(3) => \gen_macc[5].logic_inst_n_18\,
      Q(2) => \gen_macc[5].logic_inst_n_19\,
      Q(1) => \gen_macc[5].logic_inst_n_20\,
      Q(0) => \gen_macc[5].logic_inst_n_21\,
      data_clk_i => data_clk_i,
      data_en_o_reg_0(0) => data_en_s(5),
      data_en_s => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(26),
      end_s => end_s_18,
      ready_next_s_1(0) => ready_next_s_1(26),
      ready_s => ready_s_17,
      res_s0 => res_s0_0
    );
\gen_macc[6].logic_inst\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_proc_5
     port map (
      A(15 downto 0) => \coeff_tab_s_reg[28]\(15 downto 0),
      E(0) => data_out_en_s,
      Q(18) => \gen_macc[5].logic_inst_n_3\,
      Q(17) => \gen_macc[5].logic_inst_n_4\,
      Q(16) => \gen_macc[5].logic_inst_n_5\,
      Q(15) => \gen_macc[5].logic_inst_n_6\,
      Q(14) => \gen_macc[5].logic_inst_n_7\,
      Q(13) => \gen_macc[5].logic_inst_n_8\,
      Q(12) => \gen_macc[5].logic_inst_n_9\,
      Q(11) => \gen_macc[5].logic_inst_n_10\,
      Q(10) => \gen_macc[5].logic_inst_n_11\,
      Q(9) => \gen_macc[5].logic_inst_n_12\,
      Q(8) => \gen_macc[5].logic_inst_n_13\,
      Q(7) => \gen_macc[5].logic_inst_n_14\,
      Q(6) => \gen_macc[5].logic_inst_n_15\,
      Q(5) => \gen_macc[5].logic_inst_n_16\,
      Q(4) => \gen_macc[5].logic_inst_n_17\,
      Q(3) => \gen_macc[5].logic_inst_n_18\,
      Q(2) => \gen_macc[5].logic_inst_n_19\,
      Q(1) => \gen_macc[5].logic_inst_n_20\,
      Q(0) => \gen_macc[5].logic_inst_n_21\,
      data_clk_i => data_clk_i,
      data_en_o_reg_0(0) => data_en_s(6),
      data_en_s => data_en_s_10,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_in_en_s => data_in_en_s,
      \data_out_s_reg[18]\ => \cpt_store_s_reg_n_0_[1]\,
      \data_out_s_reg[18]_0\ => \cpt_store_s_reg_n_0_[0]\,
      \data_out_s_reg[18]_1\(18) => \gen_macc[4].logic_inst_n_6\,
      \data_out_s_reg[18]_1\(17) => \gen_macc[4].logic_inst_n_7\,
      \data_out_s_reg[18]_1\(16) => \gen_macc[4].logic_inst_n_8\,
      \data_out_s_reg[18]_1\(15) => \gen_macc[4].logic_inst_n_9\,
      \data_out_s_reg[18]_1\(14) => \gen_macc[4].logic_inst_n_10\,
      \data_out_s_reg[18]_1\(13) => \gen_macc[4].logic_inst_n_11\,
      \data_out_s_reg[18]_1\(12) => \gen_macc[4].logic_inst_n_12\,
      \data_out_s_reg[18]_1\(11) => \gen_macc[4].logic_inst_n_13\,
      \data_out_s_reg[18]_1\(10) => \gen_macc[4].logic_inst_n_14\,
      \data_out_s_reg[18]_1\(9) => \gen_macc[4].logic_inst_n_15\,
      \data_out_s_reg[18]_1\(8) => \gen_macc[4].logic_inst_n_16\,
      \data_out_s_reg[18]_1\(7) => \gen_macc[4].logic_inst_n_17\,
      \data_out_s_reg[18]_1\(6) => \gen_macc[4].logic_inst_n_18\,
      \data_out_s_reg[18]_1\(5) => \gen_macc[4].logic_inst_n_19\,
      \data_out_s_reg[18]_1\(4) => \gen_macc[4].logic_inst_n_20\,
      \data_out_s_reg[18]_1\(3) => \gen_macc[4].logic_inst_n_21\,
      \data_out_s_reg[18]_1\(2) => \gen_macc[4].logic_inst_n_22\,
      \data_out_s_reg[18]_1\(1) => \gen_macc[4].logic_inst_n_23\,
      \data_out_s_reg[18]_1\(0) => \gen_macc[4].logic_inst_n_24\,
      data_rst_i => data_rst_i,
      end_next_s_0(0) => end_next_s_0(31),
      end_s => end_s_20,
      \final_res_s_reg[0]_0\ => \gen_macc[6].logic_inst_n_21\,
      \final_res_s_reg[10]_0\ => \gen_macc[6].logic_inst_n_11\,
      \final_res_s_reg[11]_0\ => \gen_macc[6].logic_inst_n_10\,
      \final_res_s_reg[12]_0\ => \gen_macc[6].logic_inst_n_9\,
      \final_res_s_reg[13]_0\ => \gen_macc[6].logic_inst_n_8\,
      \final_res_s_reg[14]_0\ => \gen_macc[6].logic_inst_n_7\,
      \final_res_s_reg[15]_0\ => \gen_macc[6].logic_inst_n_6\,
      \final_res_s_reg[16]_0\ => \gen_macc[6].logic_inst_n_5\,
      \final_res_s_reg[17]_0\ => \gen_macc[6].logic_inst_n_4\,
      \final_res_s_reg[18]_0\ => \gen_macc[6].logic_inst_n_3\,
      \final_res_s_reg[1]_0\ => \gen_macc[6].logic_inst_n_20\,
      \final_res_s_reg[2]_0\ => \gen_macc[6].logic_inst_n_19\,
      \final_res_s_reg[3]_0\ => \gen_macc[6].logic_inst_n_18\,
      \final_res_s_reg[4]_0\ => \gen_macc[6].logic_inst_n_17\,
      \final_res_s_reg[5]_0\ => \gen_macc[6].logic_inst_n_16\,
      \final_res_s_reg[6]_0\ => \gen_macc[6].logic_inst_n_15\,
      \final_res_s_reg[7]_0\ => \gen_macc[6].logic_inst_n_14\,
      \final_res_s_reg[8]_0\ => \gen_macc[6].logic_inst_n_13\,
      \final_res_s_reg[9]_0\ => \gen_macc[6].logic_inst_n_12\,
      ready_next_s_1(0) => ready_next_s_1(31),
      ready_s => ready_s_19,
      res_s0 => res_s0
    );
ram_coeff: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_ram
     port map (
      DOBDO(15 downto 0) => coeff_s(15 downto 0),
      Q(4) => \cpt_s_reg_n_0_[4]\,
      Q(3) => \cpt_s_reg_n_0_[3]\,
      Q(2) => \cpt_s_reg_n_0_[2]\,
      Q(1) => \cpt_s_reg_n_0_[1]\,
      Q(0) => \cpt_s_reg_n_0_[0]\,
      coeff_en_s => coeff_en_s,
      data_clk_i => data_clk_i,
      mem_reg_0(4 downto 0) => mem_reg(4 downto 0),
      mem_reg_1(15 downto 0) => mem_reg_0(15 downto 0),
      s00_axi_aclk => s00_axi_aclk
    );
\ready_s_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => ready_next_s(0),
      Q => ready_next_s_1(1),
      S => data_rst_i
    );
\ready_s_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__4_n_0\,
      Q => ready_next_s_1(11),
      R => data_rst_i
    );
\ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(11),
      Q => \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[13]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\ready_s_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__3_n_0\,
      Q => ready_next_s_1(16),
      R => data_rst_i
    );
\ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(16),
      Q => \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[18]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\ready_s_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__2_n_0\,
      Q => ready_next_s_1(21),
      R => data_rst_i
    );
\ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(21),
      Q => \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[23]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\ready_s_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__1_n_0\,
      Q => ready_next_s_1(26),
      R => data_rst_i
    );
\ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(26),
      Q => \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[28]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\ready_s_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__0_n_0\,
      Q => ready_next_s_1(31),
      R => data_rst_i
    );
\ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(31),
      Q => \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\
    );
\ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[32]_srl2___U0_fir_top_inst_end_s_reg_r_0_n_0\,
      Q => \ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      R => '0'
    );
\ready_s_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => ready_s_reg_gate_n_0,
      Q => ready_next_s(0),
      R => data_rst_i
    );
\ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(1),
      Q => \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[3]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
\ready_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg_gate__5_n_0\,
      Q => ready_next_s_1(6),
      R => data_rst_i
    );
\ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1\: unisim.vcomponents.SRL16E
     port map (
      A0 => '0',
      A1 => '1',
      A2 => '0',
      A3 => '0',
      CE => data_in_en_s,
      CLK => data_clk_i,
      D => ready_next_s_1(6),
      Q => \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\
    );
\ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2\: unisim.vcomponents.FDRE
     port map (
      C => data_clk_i,
      CE => data_in_en_s,
      D => \ready_s_reg[8]_srl3___U0_fir_top_inst_end_s_reg_r_1_n_0\,
      Q => \ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      R => '0'
    );
ready_s_reg_gate: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[33]_U0_fir_top_inst_end_s_reg_r_1_n_0\,
      I1 => end_s_reg_r_1_n_0,
      O => ready_s_reg_gate_n_0
    );
\ready_s_reg_gate__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[29]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__0_n_0\
    );
\ready_s_reg_gate__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[24]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__1_n_0\
    );
\ready_s_reg_gate__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[19]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__2_n_0\
    );
\ready_s_reg_gate__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[14]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__3_n_0\
    );
\ready_s_reg_gate__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[9]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__4_n_0\
    );
\ready_s_reg_gate__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \ready_s_reg[4]_U0_fir_top_inst_end_s_reg_r_2_n_0\,
      I1 => end_s_reg_r_2_n_0,
      O => \ready_s_reg_gate__5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal is
  port (
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 1 downto 0 );
    data_o : out STD_LOGIC_VECTOR ( 18 downto 0 );
    tick_o : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_reset : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_en_i : in STD_LOGIC;
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal is
  signal addr_s : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal coeff_addr_s : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal coeff_addr_uns_s : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal coeff_en_s : STD_LOGIC;
  signal coeff_val_s : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal coeff_val_s_0 : STD_LOGIC;
  signal handle_comm_n_10 : STD_LOGIC;
  signal handle_comm_n_11 : STD_LOGIC;
  signal handle_comm_n_12 : STD_LOGIC;
  signal handle_comm_n_13 : STD_LOGIC;
  signal handle_comm_n_14 : STD_LOGIC;
  signal handle_comm_n_4 : STD_LOGIC;
  signal handle_comm_n_7 : STD_LOGIC;
  signal handle_comm_n_9 : STD_LOGIC;
  signal read_en_s : STD_LOGIC;
begin
firReal_axi_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_axi
     port map (
      D(0) => handle_comm_n_7,
      E(0) => coeff_val_s_0,
      Q(4 downto 0) => coeff_addr_uns_s(4 downto 0),
      addr_s(1 downto 0) => addr_s(1 downto 0),
      \coeff_addr_s_reg[0]_0\(0) => handle_comm_n_4,
      \coeff_addr_s_reg[4]_0\(4 downto 0) => coeff_addr_s(4 downto 0),
      \coeff_addr_s_reg[4]_1\(4) => handle_comm_n_10,
      \coeff_addr_s_reg[4]_1\(3) => handle_comm_n_11,
      \coeff_addr_s_reg[4]_1\(2) => handle_comm_n_12,
      \coeff_addr_s_reg[4]_1\(1) => handle_comm_n_13,
      \coeff_addr_s_reg[4]_1\(0) => handle_comm_n_14,
      coeff_en_s => coeff_en_s,
      coeff_en_s_reg_0 => handle_comm_n_9,
      \coeff_val_s_reg[15]_0\(15 downto 0) => coeff_val_s(15 downto 0),
      read_en_s => read_en_s,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_rdata(1 downto 0) => s00_axi_rdata(1 downto 0),
      s00_axi_reset => s00_axi_reset,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(15 downto 0) => s00_axi_wdata(15 downto 0)
    );
fir_top_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_top
     port map (
      coeff_en_s => coeff_en_s,
      data_clk_i => data_clk_i,
      data_en_i => data_en_i,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_o(18 downto 0) => data_o(18 downto 0),
      data_rst_i => data_rst_i,
      mem_reg(4 downto 0) => coeff_addr_s(4 downto 0),
      mem_reg_0(15 downto 0) => coeff_val_s(15 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      tick_o => tick_o
    );
handle_comm: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal_handCom
     port map (
      D(0) => handle_comm_n_7,
      E(0) => coeff_val_s_0,
      Q(4 downto 0) => coeff_addr_uns_s(4 downto 0),
      addr_s(1 downto 0) => addr_s(1 downto 0),
      axi_arready_reg_0 => s00_axi_arready,
      axi_awready_reg_0 => s00_axi_awready,
      axi_wready_reg_0 => s00_axi_wready,
      \coeff_addr_uns_s_reg[4]\(4) => handle_comm_n_10,
      \coeff_addr_uns_s_reg[4]\(3) => handle_comm_n_11,
      \coeff_addr_uns_s_reg[4]\(2) => handle_comm_n_12,
      \coeff_addr_uns_s_reg[4]\(1) => handle_comm_n_13,
      \coeff_addr_uns_s_reg[4]\(0) => handle_comm_n_14,
      read_en_s => read_en_s,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(1 downto 0),
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(1 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_awvalid_0(0) => handle_comm_n_4,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_reset => s00_axi_reset,
      s00_axi_reset_0 => handle_comm_n_9,
      s00_axi_rready => s00_axi_rready,
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    data_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    data_en_i : in STD_LOGIC;
    data_clk_i : in STD_LOGIC;
    data_rst_i : in STD_LOGIC;
    data_o : out STD_LOGIC_VECTOR ( 18 downto 0 );
    data_en_o : out STD_LOGIC;
    data_eof_o : out STD_LOGIC;
    data_clk_o : out STD_LOGIC;
    data_rst_o : out STD_LOGIC;
    tick_o : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_reset : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "acq_firReal_0_0,firReal,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "firReal,Vivado 2022.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^data_clk_i\ : STD_LOGIC;
  signal \^data_rst_i\ : STD_LOGIC;
  signal \^s00_axi_rdata\ : STD_LOGIC_VECTOR ( 28 downto 0 );
  signal \^tick_o\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of data_clk_i : signal is "xilinx.com:interface:real:1.0 data_in DATA_CLK";
  attribute x_interface_info of data_clk_o : signal is "xilinx.com:interface:real:1.0 data_out DATA_CLK";
  attribute x_interface_info of data_en_i : signal is "xilinx.com:interface:real:1.0 data_in DATA_EN";
  attribute x_interface_info of data_en_o : signal is "xilinx.com:interface:real:1.0 data_out DATA_EN";
  attribute x_interface_info of data_eof_o : signal is "xilinx.com:interface:real:1.0 data_out DATA_EOF";
  attribute x_interface_info of data_rst_i : signal is "xilinx.com:interface:real:1.0 data_in DATA_RST";
  attribute x_interface_info of data_rst_o : signal is "xilinx.com:interface:real:1.0 data_out DATA_RST";
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 s00_axi_signal_clock CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME s00_axi_signal_clock, ASSOCIATED_BUSIF s00_axi, ASSOCIATED_RESET s00_axi_reset, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN acq_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 s00_axi ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 s00_axi ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 s00_axi AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 s00_axi AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 s00_axi BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 s00_axi BVALID";
  attribute x_interface_info of s00_axi_reset : signal is "xilinx.com:signal:reset:1.0 s00_axi_signal_reset RST";
  attribute x_interface_parameter of s00_axi_reset : signal is "XIL_INTERFACENAME s00_axi_signal_reset, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 s00_axi RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 s00_axi RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 s00_axi WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 s00_axi WVALID";
  attribute x_interface_info of data_i : signal is "xilinx.com:interface:real:1.0 data_in DATA";
  attribute x_interface_info of data_o : signal is "xilinx.com:interface:real:1.0 data_out DATA";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 s00_axi ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 s00_axi ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 s00_axi AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME s00_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 125000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN acq_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 s00_axi AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 s00_axi BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 s00_axi RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 s00_axi RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 s00_axi WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 s00_axi WSTRB";
begin
  \^data_clk_i\ <= data_clk_i;
  \^data_rst_i\ <= data_rst_i;
  data_clk_o <= \^data_clk_i\;
  data_en_o <= \^tick_o\;
  data_eof_o <= \<const0>\;
  data_rst_o <= \^data_rst_i\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rdata(31) <= \<const0>\;
  s00_axi_rdata(30) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(29) <= \<const0>\;
  s00_axi_rdata(28) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(27) <= \<const0>\;
  s00_axi_rdata(26) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(25) <= \<const0>\;
  s00_axi_rdata(24) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(23) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(22) <= \<const0>\;
  s00_axi_rdata(21) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(20) <= \<const0>\;
  s00_axi_rdata(19) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(18) <= \<const0>\;
  s00_axi_rdata(17) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(16) <= \<const0>\;
  s00_axi_rdata(15) <= \<const0>\;
  s00_axi_rdata(14) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(13) <= \<const0>\;
  s00_axi_rdata(12) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(11) <= \<const0>\;
  s00_axi_rdata(10) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(9) <= \<const0>\;
  s00_axi_rdata(8) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(7) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(6) <= \<const0>\;
  s00_axi_rdata(5) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(4) <= \<const0>\;
  s00_axi_rdata(3) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(2) <= \<const0>\;
  s00_axi_rdata(1) <= \^s00_axi_rdata\(28);
  s00_axi_rdata(0) <= \^s00_axi_rdata\(0);
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
  tick_o <= \^tick_o\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_firReal
     port map (
      data_clk_i => \^data_clk_i\,
      data_en_i => data_en_i,
      data_i(13 downto 0) => data_i(13 downto 0),
      data_o(18 downto 0) => data_o(18 downto 0),
      data_rst_i => \^data_rst_i\,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(3 downto 2),
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(3 downto 2),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(1) => \^s00_axi_rdata\(28),
      s00_axi_rdata(0) => \^s00_axi_rdata\(0),
      s00_axi_reset => s00_axi_reset,
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(15 downto 0) => s00_axi_wdata(15 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wvalid => s00_axi_wvalid,
      tick_o => \^tick_o\
    );
end STRUCTURE;
