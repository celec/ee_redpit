-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Nov 13 10:01:45 2023
-- Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_convertRealToComplex_0_0_stub.vhdl
-- Design      : acq_convertRealToComplex_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    dataI_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dataI_en_i : in STD_LOGIC;
    dataI_eof_i : in STD_LOGIC;
    dataI_rst_i : in STD_LOGIC;
    dataI_clk_i : in STD_LOGIC;
    dataQ_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dataQ_en_i : in STD_LOGIC;
    dataQ_rst_i : in STD_LOGIC;
    dataQ_eof_i : in STD_LOGIC;
    dataQ_clk_i : in STD_LOGIC;
    data_i_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    data_q_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    data_eof_o : out STD_LOGIC;
    data_rst_o : out STD_LOGIC;
    data_en_o : out STD_LOGIC;
    data_clk_o : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "dataI_i[13:0],dataI_en_i,dataI_eof_i,dataI_rst_i,dataI_clk_i,dataQ_i[13:0],dataQ_en_i,dataQ_rst_i,dataQ_eof_i,dataQ_clk_i,data_i_o[13:0],data_q_o[13:0],data_eof_o,data_rst_o,data_en_o,data_clk_o";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "convertRealToComplex,Vivado 2022.2";
begin
end;
