// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Nov 13 10:01:45 2023
// Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_convertRealToComplex_0_0_sim_netlist.v
// Design      : acq_convertRealToComplex_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "acq_convertRealToComplex_0_0,convertRealToComplex,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "convertRealToComplex,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (dataI_i,
    dataI_en_i,
    dataI_eof_i,
    dataI_rst_i,
    dataI_clk_i,
    dataQ_i,
    dataQ_en_i,
    dataQ_rst_i,
    dataQ_eof_i,
    dataQ_clk_i,
    data_i_o,
    data_q_o,
    data_eof_o,
    data_rst_o,
    data_en_o,
    data_clk_o);
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataI_in DATA" *) input [13:0]dataI_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataI_in DATA_EN" *) input dataI_en_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataI_in DATA_EOF" *) input dataI_eof_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataI_in DATA_RST" *) input dataI_rst_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataI_in DATA_CLK" *) input dataI_clk_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataQ_in DATA" *) input [13:0]dataQ_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataQ_in DATA_EN" *) input dataQ_en_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataQ_in DATA_RST" *) input dataQ_rst_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataQ_in DATA_EOF" *) input dataQ_eof_i;
  (* x_interface_info = "xilinx.com:interface:real:1.0 dataQ_in DATA_CLK" *) input dataQ_clk_i;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_I" *) output [13:0]data_i_o;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_Q" *) output [13:0]data_q_o;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_EOF" *) output data_eof_o;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_RST" *) output data_rst_o;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_EN" *) output data_en_o;
  (* x_interface_info = "xilinx.com:interface:complex:1.0 data_out DATA_CLK" *) output data_clk_o;

  wire dataI_clk_i;
  wire dataI_en_i;
  wire dataI_eof_i;
  wire [13:0]dataI_i;
  wire dataI_rst_i;
  wire dataQ_clk_i;
  wire dataQ_en_i;
  wire dataQ_eof_i;
  wire [13:0]dataQ_i;
  wire dataQ_rst_i;
  wire data_clk_o;
  wire data_en_o;
  wire data_eof_o;
  wire data_rst_o;

  assign data_i_o[13:0] = dataI_i;
  assign data_q_o[13:0] = dataQ_i;
  LUT2 #(
    .INIT(4'h8)) 
    data_clk_o_INST_0
       (.I0(dataI_clk_i),
        .I1(dataQ_clk_i),
        .O(data_clk_o));
  LUT2 #(
    .INIT(4'h8)) 
    data_en_o_INST_0
       (.I0(dataI_en_i),
        .I1(dataQ_en_i),
        .O(data_en_o));
  LUT2 #(
    .INIT(4'h8)) 
    data_eof_o_INST_0
       (.I0(dataI_eof_i),
        .I1(dataQ_eof_i),
        .O(data_eof_o));
  LUT2 #(
    .INIT(4'h8)) 
    data_rst_o_INST_0
       (.I0(dataI_rst_i),
        .I1(dataQ_rst_i),
        .O(data_rst_o));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
