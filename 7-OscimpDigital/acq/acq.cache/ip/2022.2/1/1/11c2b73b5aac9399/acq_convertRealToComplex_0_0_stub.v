// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon Nov 13 10:01:45 2023
// Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_convertRealToComplex_0_0_stub.v
// Design      : acq_convertRealToComplex_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "convertRealToComplex,Vivado 2022.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(dataI_i, dataI_en_i, dataI_eof_i, dataI_rst_i, 
  dataI_clk_i, dataQ_i, dataQ_en_i, dataQ_rst_i, dataQ_eof_i, dataQ_clk_i, data_i_o, data_q_o, 
  data_eof_o, data_rst_o, data_en_o, data_clk_o)
/* synthesis syn_black_box black_box_pad_pin="dataI_i[13:0],dataI_en_i,dataI_eof_i,dataI_rst_i,dataI_clk_i,dataQ_i[13:0],dataQ_en_i,dataQ_rst_i,dataQ_eof_i,dataQ_clk_i,data_i_o[13:0],data_q_o[13:0],data_eof_o,data_rst_o,data_en_o,data_clk_o" */;
  input [13:0]dataI_i;
  input dataI_en_i;
  input dataI_eof_i;
  input dataI_rst_i;
  input dataI_clk_i;
  input [13:0]dataQ_i;
  input dataQ_en_i;
  input dataQ_rst_i;
  input dataQ_eof_i;
  input dataQ_clk_i;
  output [13:0]data_i_o;
  output [13:0]data_q_o;
  output data_eof_o;
  output data_rst_o;
  output data_en_o;
  output data_clk_o;
endmodule
