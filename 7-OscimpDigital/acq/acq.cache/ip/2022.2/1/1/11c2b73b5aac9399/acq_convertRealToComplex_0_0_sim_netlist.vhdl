-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Mon Nov 13 10:01:46 2023
-- Host        : artemis running 64-bit Debian GNU/Linux trixie/sid
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ acq_convertRealToComplex_0_0_sim_netlist.vhdl
-- Design      : acq_convertRealToComplex_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    dataI_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dataI_en_i : in STD_LOGIC;
    dataI_eof_i : in STD_LOGIC;
    dataI_rst_i : in STD_LOGIC;
    dataI_clk_i : in STD_LOGIC;
    dataQ_i : in STD_LOGIC_VECTOR ( 13 downto 0 );
    dataQ_en_i : in STD_LOGIC;
    dataQ_rst_i : in STD_LOGIC;
    dataQ_eof_i : in STD_LOGIC;
    dataQ_clk_i : in STD_LOGIC;
    data_i_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    data_q_o : out STD_LOGIC_VECTOR ( 13 downto 0 );
    data_eof_o : out STD_LOGIC;
    data_rst_o : out STD_LOGIC;
    data_en_o : out STD_LOGIC;
    data_clk_o : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "acq_convertRealToComplex_0_0,convertRealToComplex,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "convertRealToComplex,Vivado 2022.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^datai_i\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^dataq_i\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of dataI_clk_i : signal is "xilinx.com:interface:real:1.0 dataI_in DATA_CLK";
  attribute x_interface_info of dataI_en_i : signal is "xilinx.com:interface:real:1.0 dataI_in DATA_EN";
  attribute x_interface_info of dataI_eof_i : signal is "xilinx.com:interface:real:1.0 dataI_in DATA_EOF";
  attribute x_interface_info of dataI_rst_i : signal is "xilinx.com:interface:real:1.0 dataI_in DATA_RST";
  attribute x_interface_info of dataQ_clk_i : signal is "xilinx.com:interface:real:1.0 dataQ_in DATA_CLK";
  attribute x_interface_info of dataQ_en_i : signal is "xilinx.com:interface:real:1.0 dataQ_in DATA_EN";
  attribute x_interface_info of dataQ_eof_i : signal is "xilinx.com:interface:real:1.0 dataQ_in DATA_EOF";
  attribute x_interface_info of dataQ_rst_i : signal is "xilinx.com:interface:real:1.0 dataQ_in DATA_RST";
  attribute x_interface_info of data_clk_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_CLK";
  attribute x_interface_info of data_en_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_EN";
  attribute x_interface_info of data_eof_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_EOF";
  attribute x_interface_info of data_rst_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_RST";
  attribute x_interface_info of dataI_i : signal is "xilinx.com:interface:real:1.0 dataI_in DATA";
  attribute x_interface_info of dataQ_i : signal is "xilinx.com:interface:real:1.0 dataQ_in DATA";
  attribute x_interface_info of data_i_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_I";
  attribute x_interface_info of data_q_o : signal is "xilinx.com:interface:complex:1.0 data_out DATA_Q";
begin
  \^datai_i\(13 downto 0) <= dataI_i(13 downto 0);
  \^dataq_i\(13 downto 0) <= dataQ_i(13 downto 0);
  data_i_o(13 downto 0) <= \^datai_i\(13 downto 0);
  data_q_o(13 downto 0) <= \^dataq_i\(13 downto 0);
data_clk_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => dataI_clk_i,
      I1 => dataQ_clk_i,
      O => data_clk_o
    );
data_en_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => dataI_en_i,
      I1 => dataQ_en_i,
      O => data_en_o
    );
data_eof_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => dataI_eof_i,
      I1 => dataQ_eof_i,
      O => data_eof_o
    );
data_rst_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => dataI_rst_i,
      I1 => dataQ_rst_i,
      O => data_rst_o
    );
end STRUCTURE;
