#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
	int k, fi, fo;
	char c[16384];
	fi = open("/dev/data1600", O_RDWR);
	fo = open("/tmp/data.bin", O_WRONLY | O_CREAT);
	for (k = 1; k < 5; k++) {
		read(fi, c, 16384);
		write(fo, c, 16384);
	}
	close(fi);
	close(fo);
}
