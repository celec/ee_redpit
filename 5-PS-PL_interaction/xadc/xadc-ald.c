#include "linux/export.h"
#include "linux/kern_levels.h"
#include "linux/printk.h"
#include "linux/types.h"
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/of.h> // Open Firmware, convention for device-tree
#include <linux/of_device.h>
#include <linux/slab.h>
#include <linux/mutex.h>

#define DDR0 0x40
#define CHAN_SEL 0x10

struct timer_list exp_timer;
struct mutex mymutex;

struct ald_struct {
	void *mem_base;
	void *chan;
	void *period;
};
struct ald_struct *mdev;

static void print_timer(struct timer_list *t)
{
	unsigned *period;
        unsigned long value;
	period = mdev->period;
	mutex_lock(&mymutex);
	value = readl(mdev->mem_base + DDR0);
        printk(KERN_INFO "Read from XADC: %lu\n", value);
	mod_timer(&exp_timer, jiffies + *period * HZ);
	mutex_unlock(&mymutex);
}

static int xadx_ald_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	static void __iomem *ald_gpio;
	unsigned offset_init = 0x43c00000;
	unsigned channel = 0;
	unsigned period = 1;
	struct ald_struct *dev_cpy;
	dev_cpy = (struct ald_struct *)kzalloc(sizeof(struct ald_struct),
					       GFP_KERNEL);
	of_property_read_u32(np, "offset_init", &offset_init);
	of_property_read_u32(np, "channel", &channel);
	of_property_read_u32(np, "period", &period);
	// base
	ald_gpio = (void *)ioremap(offset_init, 0x04);
	dev_cpy->mem_base = ald_gpio;
	// channel
	ald_gpio = (void *)ioremap(channel, 0x04);
	dev_cpy->chan = ald_gpio;
	// timer
	ald_gpio = (void *)ioremap(period, 0x04);
	dev_cpy->period = ald_gpio;
	timer_setup(&exp_timer, print_timer, 0); // since 4.14
	exp_timer.expires = jiffies + period * HZ;
	mutex_init(&mymutex);

	writel(CHAN_SEL | channel, dev_cpy->mem_base + DDR0);

	mdev = dev_cpy;
	platform_set_drvdata(pdev, dev_cpy);
	printk(KERN_INFO "Hello from driver\n");
	return 0;
}

static int xadx_ald_remove(struct platform_device *pdev)
{
	struct ald_struct *dev_cpy = platform_get_drvdata(pdev);
	kfree(dev_cpy);
	del_timer(&exp_timer);
	platform_device_unregister(pdev);
	printk(KERN_INFO "Goodbye from driver\n");
	return 0;
}

static const struct of_device_id xadx_ald_id_of[] = { // for dev ice tree
	{
		.compatible = "xadx-ald",
	},
	{}
};
MODULE_DEVICE_TABLE(of, xadx_ald_id_of);

static struct platform_driver xadx_ald_driver = {
        .probe = xadx_ald_probe,
        .remove = xadx_ald_remove,
        .driver = {
                .name = "xadx-ald",
                .owner = THIS_MODULE,
                .of_match_table = of_match_ptr(xadx_ald_id_of),
        },
};
module_platform_driver(xadx_ald_driver);

MODULE_DESCRIPTION("Simple driver for FPGA XADC GPIO access");
MODULE_LICENSE("GPL");
