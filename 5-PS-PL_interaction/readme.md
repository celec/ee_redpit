# PL/PS interaction

## Create from Vivado
```Bash
LANG="en_US.UTF-8"
source /opt/Xilinx/Vivado/*/settings64.sh
vivado&
```
RTL project wihtout any sources for now, `xc7z010clg400-1` chip. Via a block-design, add `ZYNQ7 Processing System` and from [https://github.com/oscimp/fpga_ip/tree/master/preset](repo TCL) its board description (`redpitaya.tcl` in `Presets > Apply preset`). Then add the `AXI GPIO` block. Don't forget automation at each steps. Customize GPIO block to have only a GPIO width of 8 bit.
Then add the constraints `ports.xdc` and `clocks.xdc`.
Finally, let Vivado creates the top description wrapper, and generate the Bitstream.
Find it in `project_1.runs/impl_1/design_1_wrapper.bit`

## Bootgen:
Configuration in a `*.bif` file:
```
all: {
        design_1_wrapper.bit 
}
```
Then:
```Bash 
bootgen -image file.bif -arch zynq -process_bitstream bin
```
Creates `design_1_wrapper.bit.bin`

## Flash 
On RedPitaya
```Bash
mv design_1_wrapper.bit.bin /lib/firmware/
echo "design_1_wrapper.bit.bin" > /sys/class/fpga_manager/fpga0/firmware
```

## Play with LEDs
On RedPitaya
```Bash 
devmem 0x41200004 8 0x00 # tristate (offset 4) as outputs
devmem 0x41200000 8 0xff # state (offset 0) as ON (all LEDs ignated)
devmem 0x41200000 8 0x0f # state (offset 0) as ON for LSBs only
```
