#include "linux/export.h"
#include "linux/kern_levels.h"
#include "linux/printk.h"
#include "linux/types.h"
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/of.h> // Open Firmware, convention for device-tree
#include <linux/of_device.h>
#include <linux/slab.h>

struct ald_struct {
	void *mem_base;
};

static ssize_t led_store(struct device *pdev, struct device_attribute *attr,
			 const char *buf, size_t count)
{
	long bargraph;
	struct ald_struct *dev_cpy = dev_get_drvdata(pdev);
	if (!kstrtol(buf, 10, &bargraph))
		writel(bargraph, dev_cpy->mem_base);
	return count;
}
static DEVICE_ATTR(bargraph, 0220, NULL, led_store); // store

static int led_ald_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	static void __iomem *ald_gpio;
	unsigned offset_init = 0x41200000;
	struct ald_struct *dev_cpy;
	dev_cpy = (struct ald_struct *)kzalloc(sizeof(struct ald_struct),
					       GFP_KERNEL);
	device_create_file(&pdev->dev, &dev_attr_bargraph);
	of_property_read_u32(np, "offset_init", &offset_init);
	ald_gpio = (void *)ioremap(offset_init, 0x04);
	dev_cpy->mem_base = ald_gpio;
	platform_set_drvdata(pdev, dev_cpy);
	writel(0x0, ald_gpio + 0x04);
	printk(KERN_INFO "Hello from driver\n");
	return 0;
}

static int led_ald_remove(struct platform_device *pdev)
{
	struct ald_struct *dev_cpy = platform_get_drvdata(pdev);
	kfree(dev_cpy);
	device_remove_file(&pdev->dev, &dev_attr_bargraph);
	printk(KERN_INFO "Goodbye from driver\n");
	return 0;
}

static const struct of_device_id led_ald_id_of[] = { // for dev ice tree
	{
		.compatible = "led-ald",
	},
	{}
};
MODULE_DEVICE_TABLE(of, led_ald_id_of);

static struct platform_driver led_ald_driver = {
        .probe = led_ald_probe,
        .remove = led_ald_remove,
        .driver = {
                .name = "led-ald",
                .owner = THIS_MODULE,
                .of_match_table = of_match_ptr(led_ald_id_of),
        },
};
module_platform_driver(led_ald_driver);

MODULE_DESCRIPTION("Simple driver for FPGA LED GPIO access");
MODULE_LICENSE("GPL");
